if [ "$1" = "" ] || [ "$1" = "debug" ]
then
	OutputDir="debug"
	CompilerFlags="-DDEBUG -DPLAT_LINUX -g -I../../gbgengine"
fi

if [ "$1" = "release" ]
then
	OutputDir="release"
	CompilerFlags="-O2 -DPLAT_LINUX -I../../gbgengine"
fi

echo "param $OutputDir"

if [ "$OutputDir" != "debug" ] && [ "$OutputDir" != "release" ]
then
	echo "Invalid parameter $1"
	echo "Valid params 'debug' or 'release'"
	exit 1
fi

mkdir $OutputDir
cd $OutputDir

GTK_FLAGS=$(pkg-config --cflags gtk+-3.0)
GTK_LIBS=$(pkg-config --libs gtk+-3.0)

gcc $CompilerFlags ../../gbgengine/linux/linux.c -o puyo $GTK_FLAGS -lX11 -lGL -ldl -lm -lpulse -lpulse-simple -lpthread $GTK_LIBS

touch lock
gcc $CompilerFlags ../../source/game.c -fPIC -shared -Wl,-soname,libgame.so -o libgame.so -lGL -lm 
rm lock

if [ "$1" = "release" ]
then
	cp -r ../../assets .
fi

cd -

