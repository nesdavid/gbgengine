**Common Steps**

1. Create a new project and add gbg engine as a git submodule

**Windows**

2. run `setup.bat` in _scripts_ folder
3. It will create a new folder on the root project called _win64_
4. Run `compile.bat` from a x64 native tools command prompt (Installed with visual studio)
5. You should have an exe file in _win64/debug_ folder

**Linux**

2. run `setup.sh` in _scripts_ folder
3. It will create a new folder on the root project called _linux_
4. Run `compile.sh` from a terminal. 
5. You should have an exe file in _linux/debug_ folder

**Android (only under windows for now)**

2. run `setup_android.bat` in _scripts_ folder
3. It will create a new folder on the root project called _android_
4. Open Android Studio
5. At the welcome screen click on `Open` and select _android\GbGEngine_ folder, if everything worked out fine you should see an android icon on that folder
6. Wait until it finishes setting up the project and click on little hammer icon to compile the project
