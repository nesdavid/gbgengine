package com.gbgengine.wallbreaker;

import android.app.NativeActivity;
import android.content.Intent;
import android.net.Uri;
import android.view.KeyEvent;
import android.view.View;

public class GbGActivity extends NativeActivity {
    private native void onUnicode(int unicode);

    @Override
    protected void onResume() {
        super.onResume();

        View decorView = getWindow().getDecorView();
// Hide both the navigation bar and the status bar.
// SYSTEM_UI_FLAG_FULLSCREEN is only available on Android 4.1 and higher, but as
// a general rule, you should design your app to hide the status bar whenever you
// hide the navigation bar.
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        System.out.println("key event " + event.getAction());
        //TODO: Manage KeyEvent.MULTIPLE
        if (event.getAction() == KeyEvent.ACTION_UP) {
            int metaState = event.getMetaState();
            int unichar = event.getUnicodeChar(metaState);

            System.out.println("unichar " + unichar);
            onUnicode(unichar);
        }
        return super.dispatchKeyEvent(event);
    }

    public void openURL(String url) {
        Intent urlIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(urlIntent);
    }

    static {
        System.loadLibrary("gbg");
    }
}
