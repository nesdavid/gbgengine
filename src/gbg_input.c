#include "gbg_input.h"
#include "gbg_engine.h"

player_controller PlayerController(engine *Engine)
{
	player_controller P = {0};
	P.Keys = dynamic_array_create(Engine, key);
	P.Allowed = dynamic_array_create(Engine, enum key_code);

	return P;
}

//TODO: Make input actions that mimic button behavior and can be configured for multiple inputs
void input_init(engine *Engine)
{
	input *Input = Engine->Input;
	Input->Keys = dynamic_array_create(Engine, key);
	Input->MotionEvents = dynamic_array_create(Engine, motion_event);

	Input->PlayerControllers = dynamic_array_create(Engine, player_controller);

	//Create default player
	player_controller DefaultPlayer = PlayerController(Engine);
	dynamic_array_add(&Input->PlayerControllers, DefaultPlayer);
}

void input_update(engine *Engine)
{
	input *Input = Engine->Input;

	for (u32 PC = 0; PC<dynamic_array_len(Input->PlayerControllers); ++PC)
	{
		player_controller *Controller = Input->PlayerControllers + PC;
		Controller->Axis = Vec2(0, 0);

		for (u32 i=0; i<dynamic_array_len(Input->Keys); ++i)
		{
			key *K = Controller->Keys + i;
			if (K->State == ButtonState_Down)
			{
				if (K->KeyCode == KeyCode_Right)
				{
					Controller->KeyboardAxis.x = clamp(Controller->KeyboardAxis.x + 1, 0, 1);
				}
				if (K->KeyCode == KeyCode_Left)
				{
					Controller->KeyboardAxis.x = clamp(Controller->KeyboardAxis.x - 1, -1, 0);
				}
				if (K->KeyCode == KeyCode_Up)
				{
					Controller->KeyboardAxis.y = clamp(Controller->KeyboardAxis.y + 1, 0, 1);
				}
				if (K->KeyCode == KeyCode_Down)
				{
					Controller->KeyboardAxis.y = clamp(Controller->KeyboardAxis.y - 1, -1, 0);
				}
			}
			else if (K->State == ButtonState_Up)
			{
				if (K->KeyCode == KeyCode_Right)
				{
					Controller->KeyboardAxis.x = clamp(Controller->KeyboardAxis.x - 1, -1, 0);
				}
				if (K->KeyCode == KeyCode_Left)
				{
					Controller->KeyboardAxis.x = clamp(Controller->KeyboardAxis.x + 1, 0, 1);
				}
				if (K->KeyCode == KeyCode_Up)
				{
					Controller->KeyboardAxis.y = clamp(Controller->KeyboardAxis.y - 1, -1, 0);
				}
				if (K->KeyCode == KeyCode_Down)
				{
					Controller->KeyboardAxis.y = clamp(Controller->KeyboardAxis.y + 1, 0, 1);
				}
			}
		}

		gamepad *Gamepad = Input->Gamepads + PC;
		if (Gamepad->IsConnected)
		{
			if (input_was_button_down(&Gamepad->Buttons[Gamepad_DpadLeft]))
			{
				Controller->KeyboardAxis.x = clamp(Controller->KeyboardAxis.x - 1, -1, 0);
			}
			else if(input_was_button_clicked(&Gamepad->Buttons[Gamepad_DpadLeft]))
			{
				Controller->KeyboardAxis.x = clamp(Controller->KeyboardAxis.x + 1, 0, 1);
			}

			if (input_was_button_down(&Gamepad->Buttons[Gamepad_DpadRight]))
			{
				Controller->KeyboardAxis.x = clamp(Controller->KeyboardAxis.x + 1, 0, 1);
			}
			else if(input_was_button_clicked(&Gamepad->Buttons[Gamepad_DpadRight]))
			{
				Controller->KeyboardAxis.x = clamp(Controller->KeyboardAxis.x - 1, -1, 0);
			}

			Controller->Axis = Gamepad->LeftThumb;
			if (Gamepad->LeftThumb.x != 0)
				Engine->Platform->Log("Thumb %f, %f\n", Gamepad->LeftThumb.x, Gamepad->LeftThumb.y);
		}

		Controller->Axis.x = clamp(Controller->KeyboardAxis.x + Controller->Axis.x, -1, 1);
		Controller->Axis.y = clamp(Controller->KeyboardAxis.y + Controller->Axis.y, -1, 1);
	}
}

void process_key(engine *Engine, key Key)
{
	input *Input = Engine->Input;
	dynamic_array_add(&Input->Keys, Key);

	for (u32 pc=0; pc<dynamic_array_len(Input->PlayerControllers); ++pc)
	{
		player_controller *PC = Input->PlayerControllers + pc;

		if (PC->Allowed == 0 || dynamic_array_len(PC->Allowed) == 0)
		{
			dynamic_array_add(&PC->Keys, Key);
		}
		else
		{
			for (u32 i=0; i<dynamic_array_len(PC->Allowed); ++i)
			{
				if (PC->Allowed[i] == Key.KeyCode)
				{
					dynamic_array_add(&PC->Keys, Key);
					break;
				}
			}
		}
	}
}

key KeyFromCode(enum key_code KeyCode, button_state State)
{
	key Ret = {.KeyCode = KeyCode, .State = State};
	return Ret;
}

void input_at_end_step(engine *Engine)
{
	input *Input = Engine->Input;
	Input->MouseState.LeftButton.PrevState = Engine->Input->MouseState.LeftButton.CurState;
	Input->MouseState.RightButton.PrevState = Engine->Input->MouseState.RightButton.CurState;
	Input->MouseState.WheelDelta = 0;
	dynamic_array_clear(Input->Keys);

	for (u32 p = 0; p<dynamic_array_len(Input->PlayerControllers); ++p)
	{
		dynamic_array_clear(Input->PlayerControllers[p].Keys);
	}

	dynamic_array_clear(Input->MotionEvents);

	for (u32 g=0; g<static_array_len(Input->Gamepads); ++g)
	{
		gamepad *Gamepad = Input->Gamepads + g;
		if (Gamepad->IsConnected)
		{
			for (u32 i = 0; i<Gamepad_Max; ++i)
			{
				Gamepad->Buttons[i].PrevState = Gamepad->Buttons[i].CurState;
			}
		}
	}
}

b32 input_was_button_clicked(button *Button)
{
	return Button->CurState == ButtonState_Up && Button->PrevState == ButtonState_Down;
}

b32 input_was_button_down(button *Button)
{
	return Button->CurState == ButtonState_Down && Button->PrevState == ButtonState_Up;
}

player_controller *input_get_or_create_player_controller(struct engine *Engine, u32 Index)
{
	if (dynamic_array_len(Engine->Input->PlayerControllers) <= Index)
	{
		player_controller P = PlayerController(Engine);
		dynamic_array_add(&Engine->Input->PlayerControllers, P);
	}

	return Engine->Input->PlayerControllers + Index;
}
