#include "gbg_editor.h"
#include "gbg_rtti.h"
#include "gbg_imgui.h"
#include "gbg_world.h"

static u32 editor_read_gea_header(engine *Engine, file_handle File, u8 *Version)
{
	u32 Read = Engine->Platform->FileRead(File, Version, sizeof(*Version), 0);
	u8 Code[3];
	Read += Engine->Platform->FileRead(File, Code, sizeof(Code), Read);
	if (Code[0] == 'G' && Code[1] == 'E' && Code[2] == 'A')
	{
		return Read;
	}

	assert(0);
	return 0;
}

void editor_init(engine *Engine)
{
#ifdef NO_EDITOR
	//TODO: Fix this nonsense
	char *TexturesDir = string_new(Engine, "%stextures/", Engine->AssetDatabase.RootPath);
	Engine->Platform->ListFileStart(TexturesDir, "dds");
	for (;;)
	{
		char *FileName = Engine->Platform->ListFileNext();
		if (FileName == 0)
			break;

		char *FullPath = string_new(Engine, "textures/%s", FileName);
		s32 ExtStart = string_reverse_index_of(FullPath, '.');
		FullPath[ExtStart] = 0;
		asset_load_texture(Engine, FullPath);
		//TODO: This should be selectable from the editor, the blend mode and order
		if (string_compare(FileName, "gui_font.dds"))
		{
			render_sprite_batch_add(Engine, FullPath, 1, BlendMode_Normal);
		}
		else
		{
			render_sprite_batch_add(Engine, FullPath, 0, BlendMode_Normal);
		}
	}

	string_delete(Engine, TexturesDir);

	Engine->Rtti->CachedEntityRecord = rtti_record_get_by_name(Engine, "entity");
	editor_set_default_theme(Engine);

	return;
#else
	Engine->Editor = memory_alloc_type(&Engine->Memory, editor);
	memset(Engine->Editor, 0, sizeof(editor));

	editor *Editor = Engine->Editor;

	Editor->Config.HideImprints = dynamic_array_create(Engine, char*);

	Editor->TextureNames = dynamic_array_create(Engine, char*);
	char *Empty = 0;
	dynamic_array_add(&Editor->TextureNames, Empty);

	Editor->AvailableAssets = dynamic_array_create(Engine, char *);
	dynamic_array_add(&Editor->AvailableAssets, Empty);

	Engine->Platform->ListFileStart(Engine->AssetDatabase.RootPath, "gea");
	for (;;)
	{
		char *FileName = Engine->Platform->ListFileNext();
		if (FileName == 0)
			break;

		char *FullPath = string_new(Engine, "%s/%s", Engine->AssetDatabase.RootPath, FileName);
		file_handle File = Engine->Platform->FileOpen(FullPath);
		string_delete(Engine, FullPath);

		u8 Version;
		u32 Read = editor_read_gea_header(Engine, File, &Version);
		assert(Read != 0 && Version == 1);

		asset_config AssetConfig = {0};
		rtti_record *Record;
		Read += read_record_header(Engine, File, Read, &Record);
		if (string_compare(Record->Name, "asset_config"))
		{
			read_record(Engine, File, Read, rtti_record_get_by_type(Engine, asset_config), (u8*)&AssetConfig);
			if (AssetConfig.Type == AssetType_SpriteSheet)
			{
				sprite_sheet_config *SpriteSheetConf = (sprite_sheet_config*)AssetConfig.Config.Ptr;

				//Creation of raw asset, if it doesn't exists yet
				asset *TexAsset = asset_get(Engine, SKey(AssetConfig.AssetRef.FilePath));
				if (!TexAsset)
				{
					asset_load_texture(Engine, AssetConfig.AssetRef.FilePath);
					TexAsset = asset_get(Engine, SKey(AssetConfig.AssetRef.FilePath));

					TexAsset->TextureAsset.RenderBatchId = render_sprite_batch_add(Engine, TexAsset->TextureAsset.TextureId, SpriteSheetConf->OrderId, BlendMode_Normal);
				}

				//Creation of configured asset as a normal one
				skey KeyName = SKey(string_dup(Engine, FileName));
				asset *SSA = asset_get_or_create(Engine, KeyName);
				SSA->Type = AssetType_SpriteSheet;
				SSA->SpriteSheetAsset.Texture = TexAsset->TextureAsset;
				SSA->SpriteSheetAsset.SpriteW = SpriteSheetConf->SpriteW;
				SSA->SpriteSheetAsset.SpriteH = SpriteSheetConf->SpriteH;
			}
			else if (AssetConfig.Type == AssetType_Texture)
			{
				assert(0);
			}
		}
		Engine->Platform->FileClose(File);
	}

	//TODO: This seems like a good idea but if there's no way to change the render order then it's not that good.
	//TODO: Some assets should be for the engine only, like the font
	char *TexturesDir = string_new(Engine, "%stextures/", Engine->AssetDatabase.RootPath);
	Engine->Platform->ListFileStart(TexturesDir, "dds");
	for (;;)
	{
		char *FileName = Engine->Platform->ListFileNext();
		if (FileName == 0)
			break;

		char *FullPath = string_new(Engine, "textures/%s", FileName);
		s32 ExtStart = string_reverse_index_of(FullPath, '.');
		FullPath[ExtStart] = 0;
		asset_load_texture(Engine, FullPath);
		//TODO: This should be selectable from the editor, the blend mode and order
		if (string_compare(FileName, "gui_font.dds"))
		{
			asset *TexAsset = asset_get(Engine, SKey(FullPath));
			TexAsset->TextureAsset.RenderBatchId = render_sprite_batch_add(Engine, TexAsset->TextureAsset.TextureId, 1, BlendMode_Normal);
			dynamic_array_add(&Editor->TextureNames, FullPath);
		}
		else
		{
//			render_sprite_batch_add(Engine, FullPath, 0, BlendMode_Normal);
		}
	}

	string_delete(Engine, TexturesDir);

	Editor->LevelsDir = string_new(Engine,"%s/levels", Engine->AssetDatabase.RootPath);
	Engine->Platform->DirectoryCreate(Editor->LevelsDir);

	Engine->Editor->RecordNames = dynamic_array_create(Engine, char*);
	dynamic_array_add(&Engine->Editor->RecordNames, Empty);

	for (u32 r=0; r<dynamic_array_len(Engine->Rtti->Records); ++r)
	{
		rtti_record *R = Engine->Rtti->Records + r;
		dynamic_array_add(&Engine->Editor->RecordNames, R->Name);
	}

	Engine->Rtti->CachedEntityRecord = rtti_record_get_by_name(Engine, "entity");

	Engine->Editor->ListBoxIndices = dynamic_array_create(Engine, u32);

	editor_set_default_theme(Engine);

	Editor->CollapsedStatus = dynamic_array_create(Engine, b32);

	bmp_font Font = {string_dup(Engine, "textures/gui_font"), 7, 15, 34, 68};
	Engine->ImGui->Font = Font;
	Engine->ImGui->FontSize = 14;
#endif
}

void editor_set_default_theme(engine *Engine)
{
	//Green
	//Engine->ImGui->Theme.DefaultColor = Vec4(0.188f, 0.796f, 0.388f, 1.0f);
	//Blue
	//	Engine->ImGui->Theme.DefaultColor = Vec4(0.51f, 0.565f, 0.824f, 1.0f);
	Engine->ImGui->FontColor = Vec4(1, 1, 1, 1);
	//Violet
	//	Engine->ImGui->Theme.DefaultColor = Vec4(0.171f, 0.132f, 0.281f, 1.0f);
	//	Black
	Engine->ImGui->Theme.DefaultColor = Vec4(0.131f, 0.131f, 0.131f, 1.0f);
	Engine->ImGui->Theme.MouseOverColor = Vec4(0.333f, 0.333f, 0.333f, 1.0f);
	Engine->ImGui->Theme.PressedColor = Vec4(0.222f, 0.222f, 0.222f, 1.0f);
	Engine->ImGui->Theme.BorderColor = Vec4(0.833f, 0.833f, 0.833f, 1.0f);
	Engine->ImGui->Theme.SelectedColor = Vec4(0.333f, 0.333f, 0.333f, 1.0f);
	Engine->ImGui->Theme.SelectedBorderColor = Vec4(0.648f, 0.699f, 0.968f, 1.0f);
	Engine->ImGui->Theme.Button.TextureAsset = 0;
	Engine->ImGui->Theme.OnOverSound = 0;
}

#ifndef NO_EDITOR
static u32 write_record_header(engine *Engine, file_handle File, rtti_record *Record, u32 From)
{
	u32 StringLen = string_len(Record->Name);
	u32 Written = write_string(Engine, File, Record->Name, StringLen, From);

	return Written;
}

static u32 write_record(engine *Engine, file_handle File, rtti_record *Record, u8 *Object, u32 From);

static u32 editor_write_gea_header(engine *Engine, file_handle FileHandle)
{
	u8 Version = 1;
	u32 Written = Engine->Platform->FileWrite(FileHandle, &Version, 1, 0);
	u8 Code[] = "GEA";
	Written += Engine->Platform->FileWrite(FileHandle, Code, 3, Written);
	//TODO: Is useful to write the size of the file?
	
	return Written;
}

static void editor_write_asset(engine *Engine, char *AssetName, asset_config *Config)
{
	file_handle File = Engine->Platform->FileCreate(AssetName);

	u32 Written = editor_write_gea_header(Engine, File);
	rtti_record *Record = rtti_record_get_by_name(Engine, "asset_config");
	Written += write_record_header(Engine, File, Record, Written);

	write_record(Engine, File, Record, (u8*)Config, Written);

	Engine->Platform->FileClose(File);
}

static u32 write_float_json(engine *Engine, file_handle File, f32 Val, u32 From)
{
	char *Value = string_from_float(Engine, Val);
	u32 Written = Engine->Platform->FileWrite(File, Value, string_len(Value), From);
	string_delete(Engine, Value);

	return Written;
}

static u32 write_value_json(engine *Engine, file_handle File, u8 *Obj, rtti_field *Field, u32 From)
{
	u32 Written = 0;
	switch (Field->Type)
	{
		case RFT_Integer:
		case RFT_Enum:
		{
			char *Value = string_from_int(Engine, *((s32*)Obj));
			Written += Engine->Platform->FileWrite(File, Value, string_len(Value), From);
			string_delete(Engine, Value);
		}	break;
		case RFT_Float:
			Written += write_float_json(Engine, File, *((f32*)Obj), From);
			break;
		case RFT_Char:
			Written += write_string_json(Engine, File, Obj, 1, From);
			break;
		case RFT_Vec:
			{
				Written += Engine->Platform->FileWrite(File, "[", 1, From);
				s32 Elements = Field->Size/sizeof(f32);
				for (s32 i=0; i<Elements; i++)
				{
					f32 *Element = (f32*)Obj + i;
					Written += write_float_json(Engine, File, *Element, From + Written);
					if (i < Elements-1)
						Written += Engine->Platform->FileWrite(File, ", ", 2, From + Written);
				}
				Written += Engine->Platform->FileWrite(File, "]", 1, From + Written);
			} break;
		default:
			break;
	}
	Written += Engine->Platform->FileWrite(File, ",\n", 2, From + Written);
	return Written;
}

static u32 write_record_header_json(engine *Engine, file_handle File, rtti_record *Record, u32 From)
{
	u32 StringLen = string_len(Record->Name);
	u32 Written = write_string_json(Engine, File, Record->Name, StringLen, From);

	return Written;
}

static u32 write_record_json(engine *Engine, file_handle File, rtti_record *Record, u8 *Object, u32 From)
{
	u32 Written = 0;
	Written += Engine->Platform->FileWrite(File, "{\n", 2, From + Written);

	if (string_compare(Record->Name, "named_pointer"))
	{
		named_pointer *NP = (named_pointer*)Object;
		if (NP->RecordName && string_len(NP->RecordName) > 0)
		{
			rtti_record* NPRec = rtti_record_get_by_name(Engine, NP->RecordName);
			Written += write_record_header_json(Engine, File, NPRec, From);
			Written += write_record_json(Engine, File, NPRec, NP->Ptr, From + Written);
		}
		return Written;
	}
	else if (string_compare(Record->Name, "asset_ref"))
	{
		asset_ref *AssetRef = (asset_ref*)Object;
		Written += write_string_json(Engine, File, AssetRef->FilePath, string_len(AssetRef->FilePath), From);

		return Written;
	}

	for (u32 i=0; i<dynamic_array_len(Record->Fields); ++i)
	{
		rtti_field *Field = Record->Fields + i;

		//TODO: Save field name to check if the record changed between serialization and loading
		Written += write_string_json(Engine, File, Field->Name, string_len(Field->Name), From + Written);
		Written += Engine->Platform->FileWrite(File, ":", 1, From + Written);

		u8 *Obj = (u8*)Object + Field->Offset;

		//NOTE: What's the best way to save this? is that an array?
		if (Field->IsPointer)
		{
			if (Field->Type == RFT_String)
			{
				char **S = (char**)Obj;
				u32 Len = string_len(*S);
				Written += write_string_json(Engine, File, *S, Len, From + Written);
			}
		}
		else
		{
			assert(Field->Type != RFT_Unknown);
			if (Field->Type == RFT_Struct) 
			{
				//recurse
				rtti_record *SubRecord = rtti_record_get_by_name(Engine, Field->TypeAsString);
				Written += write_record_header_json(Engine, File, SubRecord, From + Written);
				Written += write_record_json(Engine, File, SubRecord, Obj, From + Written);
			}
			else if (Field->Type == RFT_Union)
			{
				rtti_record *SubRecord = rtti_record_get_by_name(Engine, Field->TypeAsString);

				//Find the biggest field, only serialize that one
				rtti_field *BiggestField = rtti_union_get_biggest_field(SubRecord);

				if (BiggestField->Type == RFT_Struct || BiggestField->Type == RFT_Union)
				{
					//recurse
					rtti_record *BiggestRecord = rtti_record_get_by_name(Engine, BiggestField->TypeAsString);
					Written += write_record_header_json(Engine, File, BiggestRecord, From + Written);
					Written += write_record_json(Engine, File, BiggestRecord, Obj, From + Written);
				}
				else
				{
					Written += Engine->Platform->FileWrite(File, Obj, BiggestField->Size, From + Written);
				}
			}
			else
			{
				Written += write_value_json(Engine, File, Obj, Field, From + Written);
			}
		}
	}

	Written += Engine->Platform->FileWrite(File, "\n}\n", 3, From + Written);
	return Written;
}

static u32 write_record_field(engine *Engine, file_handle File, rtti_field *Field, u8 *FieldObj, u32 From)
{
	u32 Written = write_string(Engine, File, Field->Name, string_len(Field->Name), From);
	Written += write_string(Engine, File, Field->TypeAsString, string_len(Field->TypeAsString), From + Written);
	Written += Engine->Platform->FileWrite(File, (u8*)&Field->Size, sizeof(Field->Size), From + Written);


	//NOTE: What's the best way to save this? is that an array?
	//Only saving strings
	if (Field->IsPointer)
	{
		if (Field->Type == RFT_String)
		{
			char **S = (char**)FieldObj;
			u32 Len = string_len(*S);
			Written += write_string(Engine, File, *S, Len, From + Written);
		}
	}
	else
	{
		assert(Field->Type != RFT_Unknown);
		if (Field->Type == RFT_Struct) 
		{
			rtti_record *SubRecord = rtti_record_get_by_name(Engine, Field->TypeAsString);
			Written += write_record(Engine, File, SubRecord, FieldObj, From + Written);
		}
		else if (Field->Type == RFT_Union)
		{
			rtti_record *SubRecord = rtti_record_get_by_name(Engine, Field->TypeAsString);

			//Find the biggest field, only serialize that one
			rtti_field *BiggestField = rtti_union_get_biggest_field(SubRecord);

			if (BiggestField->Type == RFT_Struct || BiggestField->Type == RFT_Union)
			{
				rtti_record *BiggestRecord = rtti_record_get_by_name(Engine, BiggestField->TypeAsString);
				Written += write_record(Engine, File, BiggestRecord, FieldObj, From + Written);
			}
			else
			{
				Written += Engine->Platform->FileWrite(File, FieldObj, BiggestField->Size, From + Written);
			}
		}
		else
		{
			//TODO: Use hton or something similar for byte order
			//TODO: Also copy vector in parts
			Written += Engine->Platform->FileWrite(File, FieldObj, Field->Size, From + Written);
		}
	}

	return Written;
}

static u32 write_record_fields(engine *Engine, file_handle File, rtti_record *Record, skey *Changes, u8 *Object, u32 From)
{
	u32 FieldCount = dynamic_array_len(Changes);
	u32 Written = Engine->Platform->FileWrite(File, (u8*)&FieldCount, sizeof(FieldCount), From);
	for (s32 i=0; i<FieldCount; ++i)
	{
		skey *FieldName = Changes + i;
		rtti_field *Field = rtti_record_get_field_by_name(Record, FieldName->S);
		assert(Field);

		u8 *FieldObj = (u8*)Object + Field->Offset;

		Written += write_record_field(Engine, File, Field, FieldObj, From + Written);
	}

	return Written;
}

static u32 write_record(engine *Engine, file_handle File, rtti_record *Record, u8 *Object, u32 From)
{
	u32 Written = 0;

	if (string_compare(Record->Name, "named_pointer"))
	{
		named_pointer *NP = (named_pointer*)Object;
		if (NP->Ptr == 0)
		{
			//Nothing to write. Write empty string so the reader can parse it
			Written += write_string(Engine, File, "", string_len(""), From);
		}
		else
		{
			rtti_record* NPRec = rtti_record_get_by_name(Engine, NP->RecordName);
			Written += write_record_header(Engine, File, NPRec, From);
			Written += write_record(Engine, File, NPRec, NP->Ptr, From + Written);
		}
		return Written;
	}
	else if (string_compare(Record->Name, "asset_ref"))
	{
		asset_ref *AssetRef = (asset_ref*)Object;
		Written += write_string(Engine, File, AssetRef->FilePath, string_len(AssetRef->FilePath), From);

		return Written;
	}

	u32 FieldCount = dynamic_array_len(Record->Fields);
	Written += Engine->Platform->FileWrite(File, (u8*)&FieldCount, sizeof(FieldCount), From + Written);
	for (u32 i=0; i<FieldCount; ++i)
	{
		rtti_field *Field = Record->Fields + i;
		u8 *Obj = (u8*)Object + Field->Offset;

		Written += write_record_field(Engine, File, Field, Obj, From + Written);
	}

	return Written;
}

static void editor_write_record_json(struct engine *Engine, rtti_record *Record, void *Object, const char *ImprintName)
{
	char *TemplateDir = string_new(Engine, "%s/imprints", Engine->AssetDatabase.RootPath);
	Engine->Platform->DirectoryCreate(TemplateDir);
	char *TemplateName = string_new(Engine, "%s/%s.geat", TemplateDir, ImprintName);

	//TODO: Check for file validity
	file_handle File = Engine->Platform->FileCreate(TemplateName);

	u32 Written = Engine->Platform->FileWrite(File, "{\n", 2, 0);

	Written += write_record_header_json(Engine, File, Record, Written);
	Written += Engine->Platform->FileWrite(File, ":", 1, Written);

	//Write full record, recursive
	write_record_json(Engine, File, Record, Object, Written);

	Written += Engine->Platform->FileWrite(File, "\n}\n", 2, Written);
	Engine->Platform->FileClose(File);

	string_delete(Engine, TemplateName);
	string_delete(Engine, TemplateDir);
	Engine->Editor->NewSelected = 0;
	Engine->Editor->TempObject = 0;
}

static void editor_write_record(struct engine *Engine, rtti_record *Record, void *Object, const char *ImprintName)
{
	char *TemplateDir = string_new(Engine, "%s/imprints", Engine->AssetDatabase.RootPath);
	Engine->Platform->DirectoryCreate(TemplateDir);
	char *TemplateName = string_new(Engine, "%s/%s.gea", TemplateDir, ImprintName);

	//TODO: Check for file validity
	file_handle File = Engine->Platform->FileCreate(TemplateName);
	u32 Written = editor_write_gea_header(Engine, File);

	Written += write_record_header(Engine, File, Record, Written);

	//Write full record, recursive
	write_record(Engine, File, Record, Object, Written);

	Engine->Platform->FileClose(File);

	string_delete(Engine, TemplateName);
	string_delete(Engine, TemplateDir);
	Engine->Editor->NewSelected = 0;
	Engine->Editor->TempObject = 0;
}

static b32 is_mouse_on_editor_entity(engine *Engine)
{
	entity_cluster *Cluster = &Engine->World->EntitiesFromEditor;
	while (Cluster)
	{
		for (u32 i=0; i<static_array_len(Cluster->E); ++i)
		{
			entity *Ent = Cluster->E + i;
			if (Ent->IsValid)
			{
				rect R = {vec3_to2(Ent->Pos), Ent->RigidBody.Rect.Rad};
				if (collision_test_rect_point(R, Engine->Input->MouseState.MousePos))
				{
					return 1;
				}
			}
		}

		Cluster = Cluster->Next;
	}
	return 0;
}

static void select_entity(editor *Editor, entity *Ent)
{
	Editor->LastEntityClicked.Mode = SM_EntityLevel;
	imprint *Selected = &Editor->LastEntityClicked.Imprint;
	Selected->Entity = Ent;
}

static void try_create_entity_in_free_space(engine *Engine)
{
	entity *Ent = Engine->Editor->LastEntityClicked.Imprint.Entity;
	rect TestRect;
	TestRect.Pos = vec3_to2(Ent->Pos);
	TestRect.Rad = vec2_div(Ent->RenderParams.Size, 2);
	TestRect.Rad.x--;
	TestRect.Rad.y--;
	if (!world_get_entity_under_rect(Engine, TestRect))
	{
		world_entity_create_from_imprint_in_editor(Engine, &Engine->Editor->LastEntityClicked.Imprint);
	}
}

#endif

void editor_update(engine *Engine)
{
#ifdef NO_EDITOR
	return;
#else
	editor *Editor = Engine->Editor;
	if (Editor->Show)
	{
		rect R = {Vec2(710, 500), Vec2(35, 8)};
		render_set_default_color(Engine, Vec4(1, 1, 1, 1));
		render_draw_line(Engine, Vec3(650, 0, 0), Vec3(650, 540, 0));
		{
			if (Editor->CurrentWorldName)
			{
				//TODO: Normalize path separator?
				s32 SepIndex = string_reverse_index_of(Editor->CurrentWorldName, '\\') + 1;
				char *LevelLabel = string_new(Engine, "Current Level: %s", Editor->CurrentWorldName + SepIndex);
				imgui_label(Engine, LevelLabel, R.Pos, TextAlign_Left);
				string_delete(Engine, LevelLabel);
			}
			R.Pos.y -= 20;

			if (imgui_button(Engine, "New Map", R))
			{
				if (Editor->CurrentWorldName)
				{
					string_delete(Engine, Editor->CurrentWorldName);
					Editor->CurrentWorldName = 0;
				}

				world_clear_level_editor(Engine);
				Editor->NewSelected = 0;
				Editor->LastEntityClicked.Imprint.Entity = 0;
			}
			R.Pos.x += 70;
			if (imgui_button(Engine, "Open Map", R))
			{
				const char *LevelName = Engine->Platform->FilePicker(Editor->LevelsDir, FP_Open, "Game Level", "*.lvl");
				if (LevelName)
				{
					world_load_level_in_editor(Engine, LevelName);
					Engine->Editor->CurrentWorldName = string_dup(Engine, LevelName);
				}
			}
			R.Pos.x += 70;

			if (imgui_button(Engine, "Save Map", R))
			{
				if (!Editor->CurrentWorldName)
					Editor->CurrentWorldName = Engine->Platform->FilePicker(Editor->LevelsDir, FP_Save, "Game Level", "*.lvl");

				if (Editor->CurrentWorldName)
				{
					file_handle LvlHandle = Engine->Platform->FileCreate(Editor->CurrentWorldName);
					u32 Written = Engine->Platform->FileWrite(LvlHandle, (u8*)"LVL", 3, 0);
					Written += write_record(Engine, LvlHandle, rtti_record_get_by_name(Engine, "gbg_level_config"), (u8*)&Engine->World->UserConfig, Written);
					u32 EntCount = 0;
					u32 EntCountFilePos = Written;
					Written += Engine->Platform->FileWrite(LvlHandle, (u8*)&EntCount, sizeof(EntCount), Written);

					entity_cluster *Cluster = &Engine->World->EntitiesFromEditor;
					skey *Changes = dynamic_array_create(Engine, skey);
					while (Cluster)
					{
						for (u32 i=0; i<static_array_len(Cluster->E); ++i)
						{
							entity *EdEnt = Cluster->E + i;
							if (EdEnt->IsValid)
							{
								entity *OriginEnt = 0;
								imprint *OriginImprint = imprint_get(Engine, EdEnt->ImprintName);
								if (OriginImprint->Entity)
									OriginEnt = OriginImprint->Entity;

								Written += write_string(Engine, LvlHandle, EdEnt->ImprintName.S, string_len(EdEnt->ImprintName.S), Written);
								entity_diff(Engine, &Changes, Engine->Rtti->CachedEntityRecord, OriginEnt, EdEnt);
								Written += write_record_fields(Engine, LvlHandle, Engine->Rtti->CachedEntityRecord, Changes, (u8*)EdEnt, Written);
								
								dynamic_array_clear(Changes);

								++EntCount;
							}
						}
						Cluster = Cluster->Next;
					}

					dynamic_array_destroy(Changes);

					Engine->Platform->FileWrite(LvlHandle, (u8*)&EntCount, sizeof(EntCount), EntCountFilePos);
					Engine->Platform->FileClose(LvlHandle);
				}
			}

			R.Pos.x = 700;
			R.Pos.y -= 20;
			R.Rad.x = 25;
			if (Engine->World->CurrentState == WorldState_Stop)
			{
				if (imgui_button(Engine, "Play", R))
				{
					Engine->World->SetTransition = WorldTransition_Play;
					Engine->World->IsPaused = 0;
				}

				//draw lines
				render_set_default_color(Engine, Vec4(1, 1, 1, 0.5));

				f32 StartX = Editor->WorldStart.x;
				f32 StartY = Editor->WorldStart.y;
				for (u32 i=0; i<=Editor->WorldDimInTiles.x; ++i)
				{
					f32 X = StartX + (i*Editor->TileDim.x);
					render_draw_line(Engine, Vec3(X, StartY, 0), Vec3(X, StartY + (Editor->TileDim.y * Editor->WorldDimInTiles.y), 0));
				}

				for (u32 i=0; i<=Editor->WorldDimInTiles.y; ++i)
				{
					f32 Y = StartY + (i*Editor->TileDim.y);
					render_draw_line(Engine, Vec3(StartX, Y, 0), Vec3(StartX + (Editor->TileDim.x * Editor->WorldDimInTiles.x), Y, 0));
				}
			}
			else
			{
				imgui_label(Engine, "Playing", R.Pos, TextAlign_Center);
			}


			R.Pos.x += 50;
			if (Engine->World->CurrentState == WorldState_Play)
			{
				if (imgui_button(Engine, "Stop", R))
				{
					Engine->World->SetTransition = WorldTransition_Stop;
				}
			}
			else
			{
				imgui_label(Engine, "Stopped", R.Pos, TextAlign_Center);
			}
		}

		R.Pos.x = 718;
		R.Pos.y -= 24;
		R.Rad = Vec2(44, 10);

		if (!Engine->Editor->NewSelected)
		{
			if (Engine->Editor->Config.HideEntityCreation == 0)
			{
				if (imgui_button(Engine, "New Entity...", R))
				{
					Engine->Editor->TempObject = memory_alloc(&Engine->Memory, sizeof(entity));
					entity Ent = {.Scale = {1, 1}, 
						.RenderParams = {.Color = {1, 1, 1, 1}
						},
					};
					*(entity*)Engine->Editor->TempObject = Ent;
					Engine->Editor->NewSelected = Engine->Rtti->CachedEntityRecord;
					Engine->Editor->LastEntityClicked.Imprint.Entity = 0;

					dynamic_array_clear(Engine->Editor->ListBoxIndices);
					Engine->Editor->ListBoxCurIndex = 0;
				}

				R.Pos.x += 100;
				if (imgui_button(Engine, "New Asset...", R))
				{
					Engine->Editor->TempObject = memory_alloc(&Engine->Memory, sizeof(asset));
					asset Asset = {0};
					*(asset*)Engine->Editor->TempObject = Asset;
					Engine->Editor->NewSelected = rtti_record_get_by_name(Engine, "asset_config");
				}
			}

			//Show already saved imprints
			f32 BtnX = 694.0f;
			for (u32 i = 0; i<dynamic_array_len(Engine->AssetDatabase.SavedImprints); ++i)
			{
				imprint *Obj = Engine->AssetDatabase.SavedImprints + i;

				b32 ShowImprint = 1;
				for (u32 HideIx=0; HideIx<dynamic_array_len(Engine->Editor->Config.HideImprints); ++HideIx)
				{
					char *HideName = Engine->Editor->Config.HideImprints[HideIx];
					skey HideKey = SKey(HideName);
					if (skey_compare_fast(&HideKey, &Obj->Name))
					{
						ShowImprint = 0;
						break;
					}
				}	

				if (ShowImprint == 0)
					continue;

				if (BtnX > 940)
				{
					BtnX = 694.0f;
					R.Pos.y -= 42.0f;
				}

				entity *Ent = (entity*)Obj->Entity;
				vec3 EntRealPos = Ent->Pos;
				Ent->Pos.x = BtnX;
				Ent->Pos.y = R.Pos.y - 34;
				Ent->Pos.z = -0.4f;

				BtnX += 42.0f;
				vec2 OldScale = Ent->Scale;
				
				float Scale = (Ent->RigidBody.Rect.Rad.x*2)/Ent->RenderParams.Size.x ;
				Ent->Scale = Vec2(Scale, Scale);

				entity_render(Engine, Ent);	
				Ent->Scale = OldScale;

				rect BtnR;
				BtnR.Pos = vec3_to2(Ent->Pos);
				BtnR.Rad = Vec2(20, 20);

				vec2 MousePos = Engine->Input->MouseState.MousePos;
				if (collision_test_rect_point(BtnR, MousePos))
				{
					f32 OldZLayer = Engine->ImGui->ZLayer;
					vec4 OldFontColor = Engine->ImGui->FontColor;
					u32 OldFontSize = Engine->ImGui->FontSize;
					Engine->ImGui->FontColor = Vec4(0.91f, 0.94f, 0.26f, 1.0f);
					Engine->ImGui->FontSize = OldFontSize + 2;
				   	Engine->ImGui->ZLayer = Ent->Pos.z - 0.04f;

					vec2 Extents = imgui_get_text_metrics(Engine, &Engine->ImGui->Font, Engine->ImGui->FontSize, Obj->Name.S, string_len(Obj->Name.S));
					render_set_default_color(Engine, Engine->ImGui->Theme.DefaultColor);
					vec2 TooltipPos = Vec2(MousePos.x, MousePos.y + 5); 
					render_draw_rect(Engine, vec2_to3(TooltipPos, Engine->ImGui->ZLayer), Extents);
					imgui_label(Engine, Obj->Name.S, TooltipPos, TextAlign_Center);

					Engine->ImGui->ZLayer = OldZLayer;
					Engine->ImGui->FontSize = OldFontSize;
					Engine->ImGui->FontColor = OldFontColor;
				}

				Ent->Pos = EntRealPos;

				if (imgui_button(Engine, "", BtnR))
				{
					if (Editor->LastEntityClicked.Imprint.Entity && Editor->LastEntityClicked.Mode == SM_Imprint)
					{
						entity_destroy(Engine, Editor->LastEntityClicked.Imprint.Entity);
						Editor->LastEntityClicked.Imprint.Entity = 0;
						Editor->LastEntityClicked.Imprint.Name = SKey(0);
					}
					imprint Copy;
					Copy.Name = Obj->Name;
					Copy.Entity = rtti_duplicate(Engine, Engine->Rtti->CachedEntityRecord, Ent);

					Editor->LastEntityClicked.Imprint = Copy;
					Editor->LastEntityClicked.Mode = SM_Imprint;
				}
			}

			if (!Editor->LastEntityClicked.Imprint.Entity)
			{
				Engine->Editor->ListBoxCurIndex = 0;
				rtti_record *NamedPtrRec = rtti_record_get_by_name(Engine, "gbg_level_config");
				vec2 P = {680, R.Pos.y - 80};
				editor_show_record(Engine, NamedPtrRec, &Engine->World->UserConfig, &P);
			}
		}
		else
		{
			rtti_record *S = Engine->Editor->NewSelected;
			char *Title = string_new(Engine, "Create new %s", S->Name);
			imgui_label(Engine, Title, Vec2(680, 442), TextAlign_Left);
			string_delete(Engine, Title);

			if (string_compare(S->Name, "entity"))
			{
				imgui_label(Engine, "Name", Vec2(680, 424), TextAlign_Left);
				rect R = {Vec2(850, 424), Vec2(90, 9)};
				imgui_edit_text(Engine, Engine->Editor->NameOut, static_array_len(Engine->Editor->NameOut), R);
			}

			vec2 P = {680, 400};

			Engine->Editor->CollapseCount = 0;
			Engine->Editor->ListBoxCurIndex = 0;
			editor_show_record(Engine, S, Engine->Editor->TempObject, &P);

			P.y -= 20;
			P.x = 800;
			R.Pos = P;
			R.Rad = Vec2(40, 10);

			if (imgui_button(Engine, "Save", R))
			{
				void *TmpObj = Engine->Editor->TempObject;
				if (string_compare(Engine->Editor->NewSelected->Name, "asset_config"))
				{
					char *AssetName = Engine->Platform->FilePicker(Engine->AssetDatabase.RootPath, FP_Save, "gbg asset", "*.gea");
					if (AssetName)
					{
						asset_config *Config = (asset_config*)TmpObj;
						editor_write_asset(Engine, AssetName, Config);
						string_delete(Engine, AssetName);
						//TODO: Create new asset in memory. Parse file to extract right name
					}
				}
				else
				{
					imprint Obj = {SKey(string_dup(Engine, Engine->Editor->NameOut)), TmpObj};
					dynamic_array_add(&Engine->AssetDatabase.SavedImprints, Obj);
					editor_write_record(Engine, Engine->Rtti->CachedEntityRecord, TmpObj, Engine->Editor->NameOut);
				}

				Engine->Editor->NewSelected = 0;
			}

			R.Pos.x += 80;
			if (imgui_button(Engine, "Cancel", R))
			{
				Engine->Editor->NewSelected = 0;
				memory_free(&Engine->Memory, Engine->Editor->TempObject);
			}
		}

		//update clicked entity
		if (Editor->LastEntityClicked.Imprint.Entity && Engine->World->CurrentState == WorldState_Stop)
		{
			vec2 P = {680, R.Pos.y - 70};
			b32 CurReadMode = Engine->ImGui->ReadOnlyMode;
			if (Editor->Config.HideEntityCreation && Editor->LastEntityClicked.Mode == SM_Imprint)
			{
				Engine->ImGui->ReadOnlyMode = 1;
			}

			Engine->Editor->CollapseCount = 0;
			Engine->Editor->ListBoxCurIndex = 0;
			//TODO: Calculate how height can the panel be
			imgui_begin_scroll_panel(Engine, Rect(Vec2(810, P.y - 150), Vec2(140, 130)), SKey("Editor Entity"));
			vec2 InsidePanelPos = Vec2(-114, 120);
			editor_show_record(Engine, Engine->Rtti->CachedEntityRecord, Editor->LastEntityClicked.Imprint.Entity, &InsidePanelPos);
			imgui_end_scroll_panel(Engine);

			Engine->ImGui->ReadOnlyMode = CurReadMode;

			P.x += 40;
			rect R = {P, {40, 10}};

			if (Editor->LastEntityClicked.Mode == SM_Imprint)
			{
				if (!Editor->Config.HideEntityCreation)
				{
					imgui_label(Engine, Editor->LastEntityClicked.Imprint.Name.S, Vec2(P.x - 40, P.y), TextAlign_Left);
					vec2 Extents = imgui_get_text_metrics(Engine, &Engine->ImGui->Font, Engine->ImGui->FontSize, Editor->LastEntityClicked.Imprint.Name.S, string_len(Editor->LastEntityClicked.Imprint.Name.S));
					R.Pos.x += Extents.x;
					if (imgui_button(Engine, "Save", R))
					{
						imprint *ModifiedImprint = &Engine->Editor->LastEntityClicked.Imprint;
						editor_write_record(Engine, Engine->Rtti->CachedEntityRecord, ModifiedImprint->Entity, ModifiedImprint->Name.S);
						//editor_write_record_json(Engine, Engine->Rtti->CachedEntityRecord, ModifiedImprint->Entity, ModifiedImprint->Name.S);

						for (u32 i=0; i<dynamic_array_len(Engine->AssetDatabase.SavedImprints); ++i)
						{
							imprint *Imprint = Engine->AssetDatabase.SavedImprints + i;
							if (skey_compare_fast(&Imprint->Name, &ModifiedImprint->Name))
							{
								rtti_record_copy(Engine, Engine->Rtti->CachedEntityRecord, ModifiedImprint->Entity, Imprint->Entity);
							}
						}
					}
					R.Pos.x += 90;
					if (imgui_button(Engine, "Cancel", R))
					{
						Editor->LastEntityClicked.Imprint.Entity = 0;
					}
				}

				vec2 MousePos = Engine->Input->MouseState.MousePos;
				vec2 WorldEnd = {Editor->WorldStart.x + (Editor->TileDim.x*Editor->WorldDimInTiles.x),
						Editor->WorldStart.y + (Editor->TileDim.y*Editor->WorldDimInTiles.y)};
				if (MousePos.x > Editor->WorldStart.x && MousePos.x < WorldEnd.x && MousePos.y > Editor->WorldStart.y && MousePos.y < WorldEnd.y)
				{
					entity *Ent = Editor->LastEntityClicked.Imprint.Entity;

					vec2 *MousePos = &Engine->Input->MouseState.MousePos;
					u32 TileX = (u32)((MousePos->x - Editor->WorldStart.x) / Editor->TileDim.x);
					u32 TileY = (u32)((MousePos->y - Editor->WorldStart.y) / Editor->TileDim.y);
					char *MouseLabel = string_new(Engine, "Cursor Position: %d, %d", TileX, TileY);

					imgui_label(Engine, MouseLabel, Vec2(Editor->WorldStart.x, WorldEnd.y + 20), TextAlign_Left);
					string_delete(Engine, MouseLabel);

					f32 X = (TileX*Editor->TileDim.x) + Editor->WorldStart.x;
					f32 Y = (TileY*Editor->TileDim.y) + Editor->WorldStart.y;
					Ent->Pos = Vec3((f32)X, (f32)Y, Ent->Pos.z);
					entity_render(Engine, Ent);

					if (Editor->Config.PaintMode)
					{
						button *MouseLeft = &Engine->Input->MouseState.LeftButton;
						if (MouseLeft->CurState == ButtonState_Down)
						{
							try_create_entity_in_free_space(Engine);
						}
						else if (input_was_button_clicked(MouseLeft))
						{
						//	Editor->LastEntityClicked.Imprint.Entity = 0;
						}
					}
					else // TODO: Maybe the no PaintMode is not useful for tile editing
					{
						if (input_was_button_clicked(&Engine->Input->MouseState.LeftButton))
						{
							if (is_mouse_on_editor_entity(Engine))
							{
								select_entity(Editor, Ent);
							}
							else
							{
								try_create_entity_in_free_space(Engine);
								Editor->LastEntityClicked.Imprint.Entity = 0;
							}
						}
					}
				}
			}
			else 
			{
				b32 DeletePressed = 0;
				for (u32 i=0; i<dynamic_array_len(Engine->Input->PlayerControllers->Keys); ++i)
				{
					key *K = Engine->Input->PlayerControllers->Keys + i;
					if (K->State == ButtonState_Down)
					{
						if (K->KeyCode == KeyCode_Delete)
						{
							DeletePressed = 1;
							break;
						}
					}
				}
				if (imgui_button(Engine, "Delete", R) || DeletePressed)
				{
					entity_destroy(Engine, Editor->LastEntityClicked.Imprint.Entity);
					Editor->LastEntityClicked.Imprint.Entity = 0;
				}
			}
		}

		if (input_was_button_clicked(&Engine->Input->MouseState.RightButton))
		{
			Editor->LastEntityClicked.Imprint.Entity = 0;
		}

		//Highlight mouse over entities
		if (Engine->World->CurrentState == WorldState_Stop)
		{
			if (Editor->LastEntityClicked.Imprint.Entity == 0 || Editor->LastEntityClicked.Mode == SM_EntityLevel)
			{
				entity_cluster *Cluster = &Engine->World->EntitiesFromEditor;
				while (Cluster)
				{
					for (u32 i=0; i<static_array_len(Cluster->E); ++i)
					{
						entity *Ent = Cluster->E + i;
						if (Ent->IsValid)
						{
							b32 IsEntSelected = Ent == Editor->LastEntityClicked.Imprint.Entity;
							rect R = {vec3_to2(Ent->Pos), Ent->RigidBody.Rect.Rad};
							if (IsEntSelected || collision_test_rect_point(R, Engine->Input->MouseState.MousePos))
							{
								render_set_default_color(Engine, Vec4(1, 1, 1, 1));
								if (Ent->RigidBody.Type == RB_Circle)
								{
									render_draw_circle(Engine, Ent->Pos, Ent->RigidBody.Circle.Rad * 1.1f);
								}
								else
								{
									vec3 Pos = Ent->Pos;
									Pos.z += 0.02f;
									render_draw_rect(Engine, Pos, vec2_mul(Ent->RigidBody.Rect.Rad, 2.1f));
								}

								if (input_was_button_clicked(&Engine->Input->MouseState.LeftButton))
								{
									select_entity(Editor, Ent);
								}
							}
						}
					}
					Cluster = Cluster->Next;
				}
			}
		}
	}
#endif
}

static void editor_field_post_edit(engine *Engine, rtti_record *Record, void *Obj, rtti_field *EditedField)
{
	if (Engine->Editor->NewSelected)
	{
		if (string_compare(Engine->Editor->NewSelected->Name, "asset_config"))
		{
			asset_config *AssetConfig = (asset_config*)Obj;
			if (AssetConfig->Type == AssetType_SpriteSheet)
			{
				if (AssetConfig->Config.RecordName)
				{
					string_delete(Engine, AssetConfig->Config.RecordName);
					memory_free(&Engine->Memory, AssetConfig->Config.Ptr);
				}

				AssetConfig->Config.RecordName = string_dup(Engine, "sprite_sheet_config");
				AssetConfig->Config.Ptr = memory_alloc_type(&Engine->Memory, sprite_sheet_config);
				memset(AssetConfig->Config.Ptr, 0, sizeof(sprite_sheet_config));
			}
		}
	}
}

static char **editor_generate_record_names(engine *Engine, rtti_record *Record, void *Obj, rtti_field *Field)
{
	char **CharArray = dynamic_array_create(Engine, char *);
	char *None = 0;
	dynamic_array_add(&CharArray, None);

	if (string_compare("asset_config", Record->Name))
	{
		asset_config *AssetConfig = (asset_config*)Obj;
		if (AssetConfig->Type == AssetType_SpriteSheet)
		{
			char *Name = "sprite_sheet_config";
			dynamic_array_add(&CharArray, Name);
		}
	}

	return CharArray;
}

static void texture_asset_preview(engine *Engine, asset *Asset, rect UVRect, vec2 DrawPos, vec2 *GlobalPos)
{
	sprite Sprite = {Asset, UVRect, Vec4(1, 1, 1, 1)};
	vec2 ThumbSize = Vec2(50, 50);
	if (UVRect.Rad.x > UVRect.Rad.y)
	{
		f32 Ratio = UVRect.Rad.y / UVRect.Rad.x;
		ThumbSize.y *= Ratio;
	}
	else if (UVRect.Rad.x < UVRect.Rad.y)
	{
		f32 Ratio = UVRect.Rad.x / UVRect.Rad.y;
		ThumbSize.x *= Ratio;
	}

	imgui_sprite(Engine, Sprite, DrawPos, ThumbSize);
	GlobalPos->y -= 70;
}

static void query_assets_of_type_cb(engine *Engine, asset *Asset)
{
	dynamic_array_add(&Engine->Editor->AvailableAssets, Asset->PathName.S);
}

static void editor_show_asset_ref(engine *Engine, rtti_record *Record, u8 *ObjPtr, rtti_field *Field, vec2 *Pos, rect FieldR)
{
	asset_ref *AssetRef = (asset_ref*)(ObjPtr + Field->Offset);
	b32 RenderPreview = 0;

	if (string_compare(Record->Name, "asset_config"))
	{
		char *Name = "Open file picker...";
		if (AssetRef->FilePath)
			Name = AssetRef->FilePath;

		if (imgui_button(Engine, Name, FieldR))
		{
			const char *Extension = "*.*";
			//Assuming we are in a asset config record
			asset_config* Conf = (asset_config*)ObjPtr;
			if (Conf->Type == AssetType_SpriteSheet || Conf->Type == AssetType_Texture)
			{
				Extension = "*.dds";
			}
			const char *AssetName = Engine->Platform->FilePicker(Engine->AssetDatabase.RootPath, FP_Open, "GbG Asset", Extension);
			if (AssetName)
			{
				string_delete(Engine, AssetRef->FilePath);
				s32 Index = string_find_str(AssetName, "assets");
				s32 ExtIndex = string_reverse_index_of(AssetName, '.');
				AssetRef->FilePath = string_substr(Engine, AssetName, Index+string_len("assets")+1, ExtIndex);
				string_replace(AssetRef->FilePath, '\\', '/');
				editor_field_post_edit(Engine, Record, (void*)ObjPtr, Field);
			}
		}

		//Preview
		{
			asset_config *AssetConfig = (asset_config*)ObjPtr;
			if (AssetConfig->AssetRef.FilePath)
			{
				if (AssetConfig->Type == AssetType_SpriteSheet || AssetConfig->Type == AssetType_Texture)
				{
					skey KeyName = SKey(AssetConfig->AssetRef.FilePath);
					asset *Asset = asset_get(Engine, KeyName);
					if (Asset)
					{
						rect UVR = {Vec2(0, 0), Vec2((f32)Asset->TextureAsset.Width, (f32)Asset->TextureAsset.Height)};
						texture_asset_preview(Engine, Asset, UVR, FieldR.Pos, Pos);
					}
					else
					{
						asset_load_texture(Engine, AssetConfig->AssetRef.FilePath);
					}

				}
			}
		}
	}
	else if (string_compare(Record->Name, "render_params"))
	{
		render_params *RenderParams = (render_params*)ObjPtr;
		//show asset selector
		//Textures
		{
			asset_type AssetType = AssetType_Texture;
			if (RenderParams->Type == RenderType_SpriteSheet)
				AssetType = AssetType_SpriteSheet;

			dynamic_array_clear(Engine->Editor->AvailableAssets);
			asset_get_all_of_type(Engine, AssetType, query_assets_of_type_cb);
			if (Engine->Editor->ListBoxCurIndex == dynamic_array_len(Engine->Editor->ListBoxIndices))
			{
				u32 Val = 0;
				for (u32 i=1; i<dynamic_array_len(Engine->Editor->AvailableAssets); ++i)
				{
					if (string_compare(AssetRef->FilePath, Engine->Editor->AvailableAssets[i]))
					{
						Val = i;
						break;
					}
				}

				dynamic_array_add(&Engine->Editor->ListBoxIndices, Val);
			}

			u32 *SelectedVal = &Engine->Editor->ListBoxIndices[Engine->Editor->ListBoxCurIndex];
			rect R = FieldR;
			R.Rad.x += 20;
			if (imgui_list_box(Engine, (const char **)Engine->Editor->AvailableAssets, R, SelectedVal))
			{
				AssetRef->FilePath = Engine->Editor->AvailableAssets[*SelectedVal];
			}
			++Engine->Editor->ListBoxCurIndex;

			//Preview
			{
				render_params *RenderParams = (render_params*)ObjPtr;
				if (RenderParams->AssetRef.FilePath)
				{
					skey KeyName = SKey(RenderParams->AssetRef.FilePath);
					asset *Asset = asset_get(Engine, KeyName);
					rect UVRect = {Vec2(0, 0), Vec2((f32)Asset->TextureAsset.Width, (f32)Asset->TextureAsset.Height)};
					if (RenderParams->Type == RenderType_Normal)
						UVRect = RenderParams->UVRect;
					else if (RenderParams->Type == RenderType_SpriteSheet)
					{
						UVRect.Pos.x = RenderParams->ColAndRow.x * (f32)Asset->SpriteSheetAsset.SpriteW;
						UVRect.Pos.y = RenderParams->ColAndRow.y * (f32)Asset->SpriteSheetAsset.SpriteH;
						UVRect.Rad.x = (f32)Asset->SpriteSheetAsset.SpriteW;
						UVRect.Rad.y = (f32)Asset->SpriteSheetAsset.SpriteH;
					}
					texture_asset_preview(Engine, Asset, UVRect, FieldR.Pos, Pos);
				}
			}
		}
	}
}

void editor_show_record(engine *Engine, rtti_record *Record, void *Obj, vec2 *StartPos)
{
	if (Engine->Editor->Config.HideEntityEdit)
		return;

	f32 YInc = 18.0f;
	f32 XInc = 20.0f;
	f32 FieldW = 8.0f;
	rtti *Rtti = Engine->Rtti;
	char TempField[256];
	memset(TempField, 0, static_array_len(TempField));
	u8 *ObjPtr = (u8*)Obj;
	for (u32 f=0; f<dynamic_array_len(Record->Fields); ++f)
	{
		rtti_field *Field = Record->Fields + f;

		if (Engine->Editor->Config.AllowOnlyGameDataEdit)
		{
			if (Field->Type != RFT_Struct)
				continue;

			if (string_compare(Field->TypeAsString, "named_pointer") == 0)
				continue;
		}

		if (Field->Name)
		{
			imgui_label(Engine, Field->Name, *StartPos, TextAlign_Left);
		}
		rect FieldR = {*StartPos};
		FieldR.Pos.x += 140;
		FieldR.Rad = Vec2(60, FieldW);

		switch (Field->Type)
		{
			case RFT_Boolean:
			{
				FieldR.Rad = Vec2(8, FieldW);
				b32 *BoolVal = (b32*)(ObjPtr + Field->Offset);
				if (imgui_tick_box(Engine, *BoolVal, FieldR))
				{
					*BoolVal = !*BoolVal;
				}
			}	break;
			case RFT_Integer:
			{
				s32 *IntVal = (s32*)(ObjPtr + Field->Offset);
				imgui_edit_int(Engine, IntVal, FieldR);
			}	break;
			case RFT_Float:
			{
				f32 *FloatVal = (f32*)(ObjPtr + Field->Offset);
				imgui_edit_float(Engine, FloatVal, FieldR);
			}	break;
			case RFT_Char:
				break;
			case RFT_String:
			{
				char **S = (char**)(ObjPtr + Field->Offset);
				// Fixing all editor text to be 128 size
				u32 NewTextSize = 128;
				if (*S == 0)
				{
					*S = (char*)memory_alloc(&Engine->Memory, NewTextSize);
					(*S)[0] = 0;
				}
				rect FTR = FieldR;
				FTR.Rad.x += 20;
				FTR.Pos.x += 10;
				imgui_edit_text(Engine, *S, NewTextSize, FTR);
			}	break;
			case RFT_Enum:
				for (u32 i=0; i<dynamic_array_len(Rtti->Enums); ++i)
				{
					rtti_enum *Enum = Rtti->Enums + i;
					if (string_compare(Enum->Name, Field->TypeAsString))
					{
						char **ListNames = dynamic_array_create(Engine, char*);
						for (u32 f=0; f<dynamic_array_len(Enum->Fields); ++f)
						{
							char *Name = (Enum->Fields + f)->Name;
							dynamic_array_add(&ListNames, Name);
						}
						u32 *EnumValue = (u32*)(ObjPtr + Field->Offset);

						imgui_list_box(Engine, (const char **)ListNames, FieldR, EnumValue);
						dynamic_array_destroy(ListNames);
					}
				}
				break;
			case RFT_Vec:
				if (Field->Size/sizeof(f32) == 2)
				{
					vec2 *VecVal = (vec2*)(ObjPtr + Field->Offset);
					FieldR.Pos.x -= 150;

					FieldR.Pos.x += 100;

					char *Header[] = {"X", "Y"};
					FieldR.Rad = Vec2(30, FieldW);
					for (u32 i=0; i<2; ++i)
					{
						imgui_label(Engine, Header[i], FieldR.Pos, TextAlign_Left);
						FieldR.Pos.x += 40;
						imgui_edit_float(Engine, &VecVal->E[i], FieldR);
						FieldR.Pos.x += 40;
					}
				}
				else if (Field->Size/sizeof(f32) == 3)
				{
					vec3 *VecVal = (vec3*)(ObjPtr + Field->Offset);
					FieldR.Pos.x -= 150;

					FieldR.Pos.x += 100;

					FieldR.Rad = Vec2(20, FieldW);
					char *Header[] = {"X", "Y", "Z"};
					for (u32 i=0; i<3; ++i)
					{
						imgui_label(Engine, Header[i], FieldR.Pos, TextAlign_Left);
						FieldR.Pos.x += 30;
						imgui_edit_float(Engine, &VecVal->E[i], FieldR);
						FieldR.Pos.x += 30;
					}
				}
				else
				{
					vec4 *VecVal = (vec4*)(ObjPtr + Field->Offset);
					FieldR.Pos.x -= 150;

					FieldR.Pos.x += 100;

					FieldR.Rad = Vec2(16, FieldW);
					char *Header[] = {"R", "G", "B", "A"};
					for (u32 i=0; i<4; ++i)
					{
						imgui_label(Engine, Header[i], FieldR.Pos, TextAlign_Left);
						FieldR.Pos.x += 22;
						imgui_edit_float(Engine, &VecVal->E[i], FieldR);
						FieldR.Pos.x += 22;
					}
				}
				break;
			case RFT_Struct:
				if (string_compare(Field->TypeAsString, "named_pointer"))
				{
					//show struct selector
					named_pointer *NP = (named_pointer*)(ObjPtr + Field->Offset);

					//TODO: Create decorator so only named pointer show up on this list box
					//char **RecordNames = editor_generate_record_names(Engine, Record, Obj, Field);
					char **RecordNames = Engine->Editor->RecordNames;

					//New list in editor
					if (Engine->Editor->ListBoxCurIndex == dynamic_array_len(Engine->Editor->ListBoxIndices))
					{
						u32 Val = 0;
						//If record has something, then show it. If not, add a new empty entry
						if (NP->RecordName)
						{
							for (u32 i=1; i<dynamic_array_len(RecordNames); ++i)
							{
								if (string_compare(NP->RecordName, RecordNames[i]))
								{
									Val = i;
									break;
								}
							}
						}
						dynamic_array_add(&Engine->Editor->ListBoxIndices, Val);
					}
					
					rtti_record *SelectedRec = 0;

					u32 *SelectedVal = &Engine->Editor->ListBoxIndices[Engine->Editor->ListBoxCurIndex];
					//Not a selected val, but already a Ptr, the value was setted from before, so we show it here, instead of waiting for user selection
					if (*SelectedVal == 0 && NP->Ptr)
					{
						for (u32 i=1; i<dynamic_array_len(RecordNames); ++i)
						{
							if (string_compare(NP->RecordName, RecordNames[i]))
							{
								*SelectedVal = i;
								break;
							}
						}
					}
					b32 OldMode = Engine->ImGui->ReadOnlyMode;
					if (Engine->Editor->Config.AllowOnlyGameDataEdit)
					{
						Engine->ImGui->ReadOnlyMode = 1;
					}
					if (imgui_list_box(Engine, (const char **)RecordNames, FieldR, SelectedVal))
					{
						if (NP->Ptr)
						{
							//TODO: change it for a rtti_delete();
							memory_free(&Engine->Memory, NP->Ptr);
						}
						NP->RecordName = RecordNames[*SelectedVal];
						if (NP->RecordName == 0)
						{
							break;
						}

						SelectedRec = rtti_record_get_by_name(Engine, NP->RecordName);
						u32 MemSize = rtti_record_get_size(Engine, SelectedRec);
						//TODO: Don't leak this on Cancel click
						NP->Ptr = memory_alloc(&Engine->Memory, MemSize);
						memset(NP->Ptr, 0, MemSize);
					}

					Engine->ImGui->ReadOnlyMode = OldMode;

					if (NP->Ptr)
					{
						SelectedRec = rtti_record_get_by_name(Engine, NP->RecordName);
						StartPos->y -= YInc;
						StartPos->x += XInc;

						b32 OldConfig = Engine->Editor->Config.AllowOnlyGameDataEdit;
						Engine->Editor->Config.AllowOnlyGameDataEdit = 0;
						editor_show_record(Engine, SelectedRec, NP->Ptr, StartPos);
						Engine->Editor->Config.AllowOnlyGameDataEdit = OldConfig;
						StartPos->y += YInc;
						StartPos->x -= XInc;
					}

					++Engine->Editor->ListBoxCurIndex;
					//dynamic_array_destroy(RecordNames);
					break;
				}
				else if (string_compare(Field->TypeAsString, "asset_ref"))
				{
					editor_show_asset_ref(Engine, Record, ObjPtr, Field, StartPos, FieldR);
					break;
				}
				else
				{
					if (Engine->Editor->CollapseCount == dynamic_array_len(Engine->Editor->CollapsedStatus))
					{
						b32 Val = 0;
						dynamic_array_add(&Engine->Editor->CollapsedStatus, Val);
					}
					rect CollapseButtonRect = FieldR;
					CollapseButtonRect.Pos = *StartPos;
					CollapseButtonRect.Pos.x -= 16;
					CollapseButtonRect.Rad.x = FieldW;
					b32 *ColVal = Engine->Editor->CollapsedStatus + Engine->Editor->CollapseCount;
					char *ColLbl = *ColVal? "+" : "-";
					b32 CreateUnique = Engine->ImGui->CreateUniqueSid;
					Engine->ImGui->CreateUniqueSid = 1;
					if (imgui_button(Engine, ColLbl, CollapseButtonRect))
					{
						*ColVal = !*ColVal;
						//Regenerate list box indices, the collapse could have change the order of the list boxes
						dynamic_array_clear(Engine->Editor->ListBoxIndices);
						Engine->Editor->ListBoxCurIndex = 0;
					}
					Engine->ImGui->CreateUniqueSid = CreateUnique;
					++Engine->Editor->CollapseCount;

					if (*ColVal)
					{
						break;
					}
				}
				//falling under! that's ok
			case RFT_Union:
				for (u32 s=0; s<dynamic_array_len(Engine->Rtti->Records); ++s)
				{
					rtti_record *S = Engine->Rtti->Records + s;
					if (string_compare(S->Name, Field->TypeAsString))
					{
						void *SubObj = ObjPtr + Field->Offset;
						if (Field->Name)
						{
							StartPos->y -= YInc;
							StartPos->x += XInc;
						}
						editor_show_record(Engine, S, SubObj, StartPos);
						if (Field->Name)
							StartPos->x -= XInc;
						StartPos->y += YInc;
						break;
					}
				}
				break;
			default:
				Engine->Platform->Log("Runtime filed type not handled. Value %d\n", Field->Type);
				break;
		}
		StartPos->y -= YInc;
	}
}

