#ifndef GBG_IMGUI_H
#define GBG_IMGUI_H

#include "gbg_render.h"
#include "gbg_input.h"

typedef struct theme
{
	sprite Button;
	sprite Slider;
	char *OnOverSound;
	vec4 DefaultColor;
	vec4 MouseOverColor;
	vec4 PressedColor;
	vec4 SelectedColor;
	vec4 SelectedBorderColor;
	vec4 BorderColor;
} theme;

typedef enum text_align
{
	TextAlign_Left,
	TextAlign_Center,
	TextAlign_Right
} text_align;

typedef struct edit_text
{
	u32 Pos;
	u32 Selection;
	f32 CursorBlinkTime;
	b32 CursorHidden;
	char CurText[64];
} edit_text;

typedef struct focus
{
	u32 Sid;
	f32 ZLayer;
	//Button at the frame that this sid got a possible focus
	button Button;
} focus;

typedef struct imgui_container
{
	skey Name;
	rect Layout;
	vec2 ScrollPos;
	b32 IsActive;
	f32 LastScrollPos;
} imgui_container;

typedef struct container_focus
{
	skey Wip;
	skey Cur;
} container_focus;

typedef struct imgui
{
	bmp_font Font;
	vec4 FontColor;
	u32 FontSize;
	u32 FramesInFocus;
	u32 CurrentFocus;
	focus PossibleFocus;
	//TODO: unify focus with this new Panel focus
	container_focus PanelInFocus;
	imgui_container *OpenContainer;

	//Pad selected
	u32 Selected;
	u32 ObjectCount;
	u32 CaptureInput;
	b32 EnterPressed;
	b32 ShowList;
	b32 InsideList;
	b32 ReadOnlyMode;
	edit_text EditText;
	theme Theme;

	b32 CreateUniqueSid;
	f32 ZLayer;
	
	b32 NavigateWithGamepad;

	//New state handling
	u32 DownFocusId;

	//Scroll list items starting point. Only when scrolling is necessary
	s32 ScrollDelta;

	imgui_container *Containers;
	imgui_container *ScrollContainers;
	u8 ScrollContainerOpenNum;
} imgui;

void imgui_init(struct engine *Engine);
void imgui_update(struct engine *Engine);
void imgui_set_bm_font(struct engine *Engine, bmp_font *Font);
b32 imgui_button(struct engine *Engine, const char *Label, rect Rect);
void imgui_label(struct engine *Engine, const char *Label, vec2 Pos, text_align Align);
b32 imgui_slider(struct engine *Engine, const char *Label, f32 *Value, rect Rect, vec2 HandlerSize);
b32 imgui_edit_text(struct engine *Engine, char *Text, u32 TextSize, rect Rect);
b32 imgui_edit_int(struct engine *Engine, s32 *IntValue, rect Rect);
b32 imgui_edit_float(struct engine *Engine, f32 *FloatValue, rect Rect);
b32 imgui_list_box(struct engine *Engine, const char **Labels, rect Rect, u32 *SelectedValue);
b32 imgui_tick_box(struct engine *Engine, b32 Value, rect Rect);

void imgui_sprite(engine *Rngine, sprite Sprite, vec2 Pos, vec2 Size);

void imgui_begin_scroll_panel(struct engine *Engine, rect Layout, skey Key);
void imgui_end_scroll_panel(struct engine *Engine);

vec2 imgui_get_text_metrics(struct engine *Engine, struct bmp_font *Font, u32 FontSize, const char *Label, u32 LabelSize);

#endif
