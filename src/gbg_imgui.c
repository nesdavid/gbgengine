#include "gbg_imgui.h"
#include "gbg_engine.h"

vec2 imgui_get_text_metrics(engine *Engine, bmp_font *Font, u32 FontSize, const char *Label, u32 LabelSize)
{
	assert(LabelSize <= string_len(Label));

	f32 Scale = FontSize/(f32)Font->GlyphH;
	f32 Width = Font->GlyphW*Scale;

	f32 W = LabelSize*Width;
	f32 H = Font->GlyphH*Scale;

	vec2 Ret = {W, H};
	return Ret;
}

static void imgui_set_possible_focus(imgui *Gui, u32 Sid, button Button)
{
	Gui->PossibleFocus.ZLayer = Gui->ZLayer;
	Gui->PossibleFocus.Sid = Sid;
	Gui->PossibleFocus.Button = Button;
	Gui->FramesInFocus = 0;
}

static b32 was_gamepad_button_pressed(engine *Engine, GamepadButtons Button)
{
	return Engine->ImGui->NavigateWithGamepad && input_was_button_down(&Engine->Input->Gamepads[0].Buttons[Button]);
}

static void do_click(imgui *Gui)
{
	Gui->CurrentFocus = 0;
	Gui->FramesInFocus = 0;
	Gui->Selected = 0;
	Gui->PossibleFocus.Sid = 0;
	Gui->PossibleFocus.ZLayer = 0;
	Gui->CaptureInput = 0;
	if (!Gui->InsideList)
	{
		Gui->ShowList = 0;
	}
}

void imgui_init(engine *Engine)
{
	Engine->ImGui->Containers = dynamic_array_create(Engine, imgui_container);
	Engine->ImGui->ScrollContainers = dynamic_array_create(Engine, imgui_container);
}

static void imgui_begin_container(engine *Engine, rect Layout, skey Key)
{
	imgui *ImGui = Engine->ImGui;
	imgui_container Info = {Key, Layout, {0, 0}, 1};
	dynamic_array_add(&ImGui->Containers, Info);
	ImGui->ZLayer -= 0.03f;
}

static void imgui_end_container(engine *Engine)
{
	imgui *ImGui = Engine->ImGui;
	s32 Last = dynamic_array_len(ImGui->Containers);
	assert(Last > 0);
	dynamic_array_remove_at(ImGui->Containers, Last - 1);
	ImGui->ZLayer += 0.03f;
}

static vec2 imgui_container_get_world_pos(imgui *ImGui)
{
	vec2 WPos = {0};
	for (s32 i=0; i<dynamic_array_len(ImGui->Containers); ++i)
	{
		imgui_container *C = ImGui->Containers + i;
		WPos = vec2_add(WPos, vec2_add(C->Layout.Pos, C->ScrollPos));
	}

	return WPos;
}

static void imgui_record_last_scroll_pos(imgui *Gui, vec2 Pos)
{
	if (Gui->OpenContainer)
	{
		if (Pos.y < Gui->OpenContainer->LastScrollPos)
			Gui->OpenContainer->LastScrollPos = Pos.y;
	}
}

void imgui_sprite(engine *Engine, sprite Sprite, vec2 Pos, vec2 Size)
{
	imgui_record_last_scroll_pos(Engine->ImGui, Pos);
	vec2 GPos = imgui_container_get_world_pos(Engine->ImGui);
	Pos = vec2_add(Pos, GPos);
	vec3 ImagePos = vec2_to3(Pos, Engine->ImGui->ZLayer-0.03);
	ImagePos.y -= 35;
	ImagePos.x -= 25;

	render_draw_sprite(Engine, Sprite, ImagePos, Size, 0, BlendMode_Normal);
}

b32 imgui_button(engine *Engine, const char *Label, rect Rect)
{
	imgui *Gui = Engine->ImGui;
	Gui->ObjectCount++;
	u32 Sid = 0;

	imgui_container *CurContainer = 0;

	imgui_begin_container(Engine, Rect, SKey(""));

	imgui_record_last_scroll_pos(Engine->ImGui, Rect.Pos);
	vec2 GlobalContainerPos = imgui_container_get_world_pos(Gui);
	//Changing Rect pos to global pos. From here on it should be fine to do so
	Rect.Pos = GlobalContainerPos;

	//If Button rect is outside parent container, bail out.
	s32 ScrollContainerIndex = 0;
	s32 ScrollContainerLen = dynamic_array_len(Gui->ScrollContainers);

	if (ScrollContainerLen && Gui->OpenContainer)
	{
		//Scroll Containers keep all containers so they can save the scrolling info
		//Search for our container
		for (s32 i=0; i<ScrollContainerLen; ++i)
		{
			imgui_container *SC = Gui->ScrollContainers + i;
			if (skey_compare_fast(&SC->Name, &Gui->OpenContainer->Name))
			{
				ScrollContainerIndex = i;
				break;
			}
		}

		rect ScrollRect = {0};
		for (s32 i=0; i<ScrollContainerIndex + 1; ++i)
		{
			imgui_container *SC = Gui->ScrollContainers + i;
			ScrollRect.Pos = vec2_add(SC->Layout.Pos, ScrollRect.Pos);
		}

		ScrollRect.Rad = Gui->OpenContainer->Layout.Rad;
		if (!collision_test_rect_vs_rect(ScrollRect, Rect))
		{
			imgui_end_container(Engine);
			return 0;
		}
	}

	Gui->ZLayer -= 0.03f;
	//for a label with align center, a 0,0 pos is the center
	imgui_label(Engine, Label, Vec2(0, 0), TextAlign_Center);
	Gui->ZLayer += 0.03f;

	if (!Gui->CreateUniqueSid)
		Sid = string_sid(Label);

	if (Sid == 0)
		Sid = Gui->ObjectCount;

	b32 Click = 0;
	b32 MouseTest = collision_test_rect_point(Rect, Engine->Input->MouseState.MousePos);
	if ((MouseTest || Gui->ObjectCount == Gui->Selected) && !Gui->ReadOnlyMode)
	{
		render_set_default_color(Engine, Gui->Theme.DefaultColor);
		if (Gui->CurrentFocus == 0)
		{
			if (Gui->PossibleFocus.Sid == 0)
			{
				imgui_set_possible_focus(Gui, Sid, Engine->Input->MouseState.LeftButton);
			}
			else
			{
				if (Gui->PossibleFocus.ZLayer > Gui->ZLayer)
				{
					imgui_set_possible_focus(Gui, Sid, Engine->Input->MouseState.LeftButton);
				}
			}
		}
		else if (Gui->CurrentFocus == Sid)
		{
			b32 PlayMouseOverSound = 0;

			if (Gui->FramesInFocus == 0)
				PlayMouseOverSound = 1;

			Gui->FramesInFocus++;

			if (Gui->ObjectCount != Gui->Selected)
			{
				Gui->Selected = 0;
			}

			//TODO: Fix android tap :(
//			if (input_was_button_down(&Gui->PossibleFocus.Button))
			if (input_was_button_down(&Engine->Input->MouseState.LeftButton))
			{
				Gui->DownFocusId = Sid;
			}

			if (was_gamepad_button_pressed(Engine, Gamepad_A) || Gui->EnterPressed)
			{
				Click = 1;
				do_click(Gui);
			}

			if (Gui->DownFocusId == Sid)
			{
				if (Gui->Theme.Button.TextureAsset)
					Gui->Theme.Button.UVRect.Pos.y = Gui->Theme.Button.UVRect.Rad.y*2;
				else
					render_set_default_color(Engine, Gui->Theme.PressedColor);

				if (MouseTest && Gui->DownFocusId == Sid && input_was_button_clicked(&Engine->Input->MouseState.LeftButton))
				{
					Click = 1;
					do_click(Gui);
				}
			}
			else
			{
				if (Gui->Theme.Button.TextureAsset)
					Gui->Theme.Button.UVRect.Pos.y = Gui->Theme.Button.UVRect.Rad.y;
				else
				{
					render_set_default_color(Engine, Gui->Theme.MouseOverColor);
				}

				if (Gui->Theme.OnOverSound)
				{
					if (PlayMouseOverSound)
					{
						sound_play(Engine, Gui->Theme.OnOverSound);
					}
				}
			}
		}
		else
		{
			//Mouse over another button, but not the one in focus 
			if (Gui->PossibleFocus.ZLayer >= Gui->ZLayer)
			{
				imgui_set_possible_focus(Gui, Sid, Engine->Input->MouseState.LeftButton);
			}
		}
	}
	else
	{
		if (Gui->CurrentFocus == Sid)
		{
			Gui->CurrentFocus = 0;
			Gui->FramesInFocus = 0;
			Gui->PossibleFocus.Sid = 0;
		}

		if (Gui->Theme.Button.TextureAsset)
			Gui->Theme.Button.UVRect.Pos.y = 0;
		else
			render_set_default_color(Engine, Gui->Theme.DefaultColor);
	}

	if (Gui->Theme.Button.TextureAsset)
	{
		render_draw_sprite(Engine, Gui->Theme.Button, vec2_to3(Rect.Pos, Gui->ZLayer), vec2_mul(Rect.Rad, 2), 0, 0);
	}
	else
	{
		//Assume baseline in the center of the glyph...
		render_draw_rect(Engine, vec2_to3(Rect.Pos, Gui->ZLayer), vec2_mul(Rect.Rad, 2));

		render_set_default_color(Engine, Gui->Theme.BorderColor);
		f32 ZP = Engine->ImGui->ZLayer - 0.02f;
		render_draw_line(Engine, vec2_to3(vec2_sub(Rect.Pos, Rect.Rad), ZP), vec2_to3(vec2_add(Rect.Pos, Vec2(Rect.Rad.x, -Rect.Rad.y)), ZP));
		render_draw_line(Engine, vec2_to3(vec2_add(Rect.Pos, Vec2(Rect.Rad.x, -Rect.Rad.y)), ZP), vec2_to3(vec2_add(Rect.Pos, Rect.Rad), ZP));
		render_draw_line(Engine, vec2_to3(vec2_add(Rect.Pos, Rect.Rad), ZP), Vec3(Rect.Pos.x - Rect.Rad.x, Rect.Pos.y + Rect.Rad.y, ZP));
		render_draw_line(Engine, Vec3(Rect.Pos.x - Rect.Rad.x, Rect.Pos.y + Rect.Rad.y, ZP), vec2_to3(vec2_sub(Rect.Pos, Rect.Rad), ZP));
	}

	imgui_end_container(Engine);
	return Click;
}

void imgui_label(engine *Engine, const char *Label, vec2 Pos, text_align Align)
{
	imgui *Gui = Engine->ImGui;

	imgui_record_last_scroll_pos(Gui, Pos);
	vec2 GPos = imgui_container_get_world_pos(Gui);
	Pos = vec2_add(Pos, GPos);

	vec2 Extents = imgui_get_text_metrics(Engine, &Gui->Font, Gui->FontSize, Label, string_len(Label));
	f32 Scale = Gui->FontSize/(f32)Gui->Font.GlyphH;
	if (Align == TextAlign_Center)
		Pos.x -= Extents.x/2 - ((Gui->Font.GlyphW*Scale)/2);
	else if (Align == TextAlign_Right)
		Pos.x -= Extents.x;

	f32 ZInc = 0.02f;
	Gui->ZLayer -= ZInc;
	render_draw_text_bmf(Engine, &Gui->Font, Gui->FontSize, Label, Pos, Gui->FontColor);
	Gui->ZLayer += ZInc;
}

//From Rect.Pos to the right draw the slider, to left and right aligned draw the label
b32 imgui_slider(struct engine *Engine, const char *Label, f32 *Value, rect Rect, vec2 HandlerSize)
{
	f32 SliderWidth = 100.0f;
	f32 SliderHalfW = SliderWidth/2.0f;

	imgui *Gui = Engine->ImGui;
	imgui_label(Engine, Label, Rect.Pos, TextAlign_Right);
	
	vec2 SliderStartPos = Vec2(Rect.Pos.x+5, Rect.Pos.y);
	if (Gui->Theme.Slider.TextureAsset)
	{
		render_draw_sprite(Engine, Gui->Theme.Slider, vec2_to3(Vec2(SliderStartPos.x+50, SliderStartPos.y), Engine->ImGui->ZLayer), Vec2(SliderWidth, Rect.Rad.y), 0, 0);
	}
	else
	{
		render_set_default_color(Engine, Vec4(1, 1, 1, 1));
		f32 ZP = Engine->ImGui->ZLayer;
		render_draw_line(Engine, vec2_to3(SliderStartPos, ZP), vec2_to3(vec2_add(SliderStartPos, Vec2(SliderWidth, 0)), ZP));
	}

	f32 CurVal = fmaxf(0, fminf(*Value, 1));
	rect BtnRect = {Vec2(SliderStartPos.x + SliderWidth*CurVal, Rect.Pos.y), HandlerSize};
	Engine->ImGui->ZLayer += -0.02f;
	imgui_button(Engine, "", BtnRect);
	Engine->ImGui->ZLayer += 0.02f;

	rect ColRect = {Vec2(SliderStartPos.x+SliderHalfW, Rect.Pos.y), Vec2(SliderHalfW , 16)};

	b32 Ret = 0;
	u32 StringId = string_sid(Label);
	b32 AlreadyInFocus = Gui->CurrentFocus == StringId || Gui->DownFocusId == Gui->ObjectCount;
	if (AlreadyInFocus || collision_test_rect_point(ColRect, Engine->Input->MouseState.MousePos))
	{
		if (Engine->Input->MouseState.LeftButton.CurState == ButtonState_Down)
		{
			Gui->CurrentFocus = StringId;
			f32 MouseX = clamp(Engine->Input->MouseState.MousePos.x - SliderStartPos.x, 0, SliderWidth);
			CurVal = MouseX / SliderWidth;
			Ret = 1;
		}
	}

	if (Gui->Selected == Gui->ObjectCount)
	{
		if (was_gamepad_button_pressed(Engine, Gamepad_DpadLeft))
		{
			Ret = 1;
			CurVal -= 0.1f;
			if (CurVal < 0)
				CurVal = 0;
		}
		if (was_gamepad_button_pressed(Engine, Gamepad_DpadRight))
		{
			Ret = 1;
			CurVal += 0.1f;
			if (CurVal > 1)
				CurVal = 1;
		}
	}

	*Value = CurVal;

	return Ret;
}

static void imgui_move_cursor_down(engine *Engine)
{
	imgui *Gui = Engine->ImGui;
	Gui->Selected++;
	if (Gui->Selected > Gui->ObjectCount)
		Gui->Selected = Gui->ObjectCount;
}

static void imgui_move_cursor_up(engine *Engine)
{
	imgui *Gui = Engine->ImGui;
	Gui->Selected--;
	if (Gui->Selected < 1)
		Gui->Selected = 1;
}

void imgui_update(struct engine *Engine)
{
	imgui *Gui = Engine->ImGui;
	Gui->EnterPressed = 0;
	Gui->ZLayer = 0;

	Gui->ScrollDelta = -Engine->Input->MouseState.WheelDelta;

	if (Engine->Input->Gamepads[0].IsConnected)
	{
		if (was_gamepad_button_pressed(Engine, Gamepad_DpadDown))
		{
			imgui_move_cursor_down(Engine);
		}
		if (was_gamepad_button_pressed(Engine, Gamepad_DpadUp))
		{
			imgui_move_cursor_up(Engine);
		}
	}

	for (u32 k=0; k<dynamic_array_len(Engine->Input->Keys); ++k)
	{
		key *Key = Engine->Input->Keys + k;
		if (Key->State == ButtonState_Down)
		{
			if (Key->KeyCode == KeyCode_Down)
			{
				imgui_move_cursor_down(Engine);
			}
			else if (Key->KeyCode == KeyCode_Up)
			{
				imgui_move_cursor_up(Engine);
			}
			else if (Key->KeyCode == KeyCode_Enter)
			{
				Gui->EnterPressed = 1;
			}
		}
	}

	if (input_was_button_clicked(&Engine->Input->MouseState.LeftButton))
	{
		Gui->DownFocusId = 0;
		Gui->ScrollDelta = 0;
	}

	Gui->ObjectCount = 0;

	Gui->CurrentFocus = Gui->PossibleFocus.Sid;

	Gui->PanelInFocus.Cur = Gui->PanelInFocus.Wip;
	Gui->PanelInFocus.Wip = SKey("");

	for (s32 i=dynamic_array_len(Gui->ScrollContainers); i > 0; --i)
	{
		imgui_container *C = Gui->ScrollContainers + i - 1;
		if (!C->IsActive)
		{
			dynamic_array_remove_at(Gui->ScrollContainers, i - 1);
			continue;
		}
		C->IsActive = 0;
	}
}

typedef enum input_filter_type
{
	InputType_Text,
	InputType_Int,
	InputType_Float,
} input_filter_type;

static b32 imgui_edit_text_filter_input(struct engine *Engine, char *Text, u32 TextSize, rect Rect, input_filter_type InputType)
{
	imgui *Gui = Engine->ImGui;
	Gui->ObjectCount++;

	imgui_record_last_scroll_pos(Gui, Rect.Pos);
	vec2 GPos = imgui_container_get_world_pos(Gui);

	char *Name = string_new(Engine, "textedit%d", Gui->ObjectCount);
	u32 Sid = string_sid(Name);
	memory_free(&Engine->Memory, Name);

	vec4 BorderColor = Gui->Theme.BorderColor;

	b32 UserClicked = !Gui->ReadOnlyMode && (input_was_button_clicked(&Engine->Input->MouseState.LeftButton) || input_was_button_down(&Engine->Input->Gamepads[0].Buttons[Gamepad_A]));

	char *CurText = Text;
	u32 CurTextSize = string_len(Text);
	if (Gui->CaptureInput == Sid)
	{
		CurText = Gui->EditText.CurText;
		CurTextSize = string_len(CurText);
		vec2 TextRect = imgui_get_text_metrics(Engine, &Gui->Font, Gui->FontSize, CurText, CurTextSize);
		//TODO: Assuming text center align
		f32 InitialPos = -TextRect.x/2.0f;

		vec2 SelectedSize = {0};
		f32 CursorPos = 0;

		vec2 Size = imgui_get_text_metrics(Engine, &Gui->Font, Gui->FontSize, CurText, Gui->EditText.Pos);
		//TODO: Depends of the text align, assuming center
		CursorPos = InitialPos + Size.x;

		if (Gui->EditText.Selection)
		{
			//Draw selection box
			SelectedSize = TextRect;
			BorderColor = Gui->Theme.SelectedBorderColor;
			render_set_default_color(Engine, Gui->Theme.SelectedColor);
			vec3 P = {CursorPos - InitialPos + Rect.Pos.x, Rect.Pos.y, Gui->ZLayer-0.02f};
			render_draw_rect(Engine, P, SelectedSize);
		}
		else
		{
			//Draw blikning cursor
			render_set_default_color(Engine, Gui->Theme.BorderColor);
			Gui->EditText.CursorBlinkTime += 0.2f; //TODO: Pass deltatime here?. Engine should hold the dt to use
			f32 BlinkTotalTime = 2;
			if (!Gui->EditText.CursorHidden)
			{
				BlinkTotalTime = 5;
				u32 Space = 4;

				f32 StartX = Rect.Pos.x + CursorPos + (SelectedSize.x/2.0f);

				f32 ZP = Engine->ImGui->ZLayer - 0.04f;
				vec3 CursorStart = {StartX, Rect.Pos.y - Rect.Rad.y + Space, ZP};
				vec3 CursorEnd = {StartX, Rect.Pos.y + Rect.Rad.y - Space, ZP};

				render_draw_line(Engine, CursorStart, CursorEnd);
			}

			if (Gui->EditText.CursorBlinkTime > BlinkTotalTime)
			{
				Gui->EditText.CursorHidden = !Gui->EditText.CursorHidden;
				Gui->EditText.CursorBlinkTime -= BlinkTotalTime;
			}
		}

		for (u32 i=0; i<dynamic_array_len(Engine->Input->Keys); i++)
		{
			key *K = Engine->Input->Keys + i;
			if (K->State == ButtonState_Down)
			{
				if (K->KeyCode == KeyCode_BackSpace)
				{
					if (Gui->EditText.Selection > 0)
					{
						u32 Cursor = Gui->EditText.Pos;
						for (u32 i=Gui->EditText.Selection; i<CurTextSize; ++i)
						{
							CurText[Cursor++] = CurText[i];	
						}
						CurText[Cursor] = 0;

						Gui->EditText.Pos = 0;
						Gui->EditText.Selection = 0;
						string_copy(CurText, Text);
						return 1;
					}
					else if (Gui->EditText.Pos > 0)
					{
						u32 i;
						for (i=Gui->EditText.Pos; i<CurTextSize; ++i)
						{
							CurText[i-1] = CurText[i];
						}
						CurText[i-1] = 0;
						Gui->EditText.Pos -= 1;
						string_copy(CurText, Text);
						return 1;
					}
				}
				else if (K->KeyCode == KeyCode_Enter)
				{
					Gui->CaptureInput = 0;
					Gui->CurrentFocus = 0;
					Gui->Selected = 0;
					//TODO: Make string copy safe
					string_copy(CurText, Text);
					return 1;
				}
				else if (K->KeyCode == KeyCode_Right)
				{
					if (Gui->EditText.Pos + Gui->EditText.Selection < CurTextSize)
					{
						++Gui->EditText.Pos;
					}
					else
					{
						Gui->EditText.Pos = CurTextSize;
					}
					Gui->EditText.Selection = 0;
				}
				else if (K->KeyCode == KeyCode_Left)
				{
					if (Gui->EditText.Pos > 0)
					{
						--Gui->EditText.Pos;
					}
					else
					{
						Gui->EditText.Pos = 0;
					}
					Gui->EditText.Selection = 0;
				}
				else
				{
					if (K->KeyChar[0])
					{
						//TODO: Support utf8
						char AChar = K->KeyChar[0];
						//TODO: Filter chars that the font can handle. For now skip symbols
						if (AChar == KeyCode_Tab || AChar == KeyCode_BackSpace)
							return 0;

						if (InputType == InputType_Int)
						{
							if (AChar < '0' || AChar > '9')
							{
								if (AChar != '-')
									return 0;
							}
						}

						if (InputType == InputType_Float)
						{
							if (AChar < '0' || AChar > '9')
							{	
								if (AChar != '.' && AChar != '-')
									return 0;
							}
							//. is float sep? 
							//TODO: Use float separator from localization data from host
						}

						if (Gui->EditText.Selection > 0)
						{
							u32 Cursor = Gui->EditText.Pos;
							CurText[Cursor++] = AChar;
							for (u32 i=Gui->EditText.Selection; i<CurTextSize; ++i)
							{
								CurText[Cursor++] = CurText[i];	
							}
							CurText[Cursor] = 0;
							Gui->EditText.Pos = Cursor;
							Gui->EditText.Selection = 0;
							string_copy(CurText, Text);
							return 1;
						}
						else if (Gui->EditText.Pos < TextSize -1)
						{
							CurText[CurTextSize+1] = 0;
							for (u32 i=CurTextSize; i>Gui->EditText.Pos; --i)
							{
								CurText[i] = CurText[i-1];
							}
							CurText[Gui->EditText.Pos++] = AChar;
							string_copy(CurText, Text);
							return 1;
						}
					}
				}
			}
		}
	}

	rect ColRect = {vec2_add(GPos, Rect.Pos), Rect.Rad};
	if (collision_test_rect_point(ColRect, Engine->Input->MouseState.MousePos) || Gui->ObjectCount == Gui->Selected)
	{
		//TODO: Change cursor
		render_set_default_color(Engine, Gui->Theme.DefaultColor);
		Gui->CurrentFocus = Sid;
		if (UserClicked)
		{
			//TODO: Click everywhere else should reset the capture
			Gui->CaptureInput = Sid;
			Gui->EditText.Pos = 0;
			Gui->EditText.Selection = CurTextSize;
			Engine->Platform->KeyboardShow();
			//TODO: make string copy safe
			string_copy(Text, Gui->EditText.CurText);
		}
	}
	else
	{
		if (Gui->CurrentFocus == Sid)
		{
			Gui->CurrentFocus = 0;
		}

		render_set_default_color(Engine, Gui->Theme.DefaultColor);
	}

	imgui_label(Engine, CurText, Rect.Pos, TextAlign_Center);

	Rect.Pos = ColRect.Pos;
	render_draw_rect(Engine, vec2_to3(Rect.Pos, Gui->ZLayer), vec2_mul(Rect.Rad, 2));
	f32 ZP = Engine->ImGui->ZLayer - 0.02f;
	render_set_default_color(Engine, BorderColor);
	render_draw_line(Engine, vec2_to3(vec2_sub(Rect.Pos, Rect.Rad), ZP), vec2_to3(vec2_add(Rect.Pos, Vec2(Rect.Rad.x, -Rect.Rad.y)), ZP));
	render_draw_line(Engine, vec2_to3(vec2_add(Rect.Pos, Vec2(Rect.Rad.x, -Rect.Rad.y)), ZP), vec2_to3(vec2_add(Rect.Pos, Rect.Rad), ZP));
	render_draw_line(Engine, vec2_to3(vec2_add(Rect.Pos, Rect.Rad), ZP), Vec3(Rect.Pos.x - Rect.Rad.x, Rect.Pos.y + Rect.Rad.y, ZP));
	render_draw_line(Engine, Vec3(Rect.Pos.x - Rect.Rad.x, Rect.Pos.y + Rect.Rad.y, ZP), vec2_to3(vec2_sub(Rect.Pos, Rect.Rad), ZP));


	return 0;
}

b32 imgui_edit_text(struct engine *Engine, char *Text, u32 TextSize, rect Rect)
{
	return imgui_edit_text_filter_input(Engine, Text, TextSize, Rect, InputType_Text);
}

b32 imgui_edit_int(struct engine *Engine, s32 *IntValue, rect Rect)
{
	char TempField[64];
	snprintf(TempField, static_array_len(TempField), "%d", *IntValue);
	b32 Ret = imgui_edit_text_filter_input(Engine, TempField, static_array_len(TempField), Rect, InputType_Int);
	if (Ret)
	{
		//TODO: Make our own function
#ifdef PLAT_WIN64
		sscanf_s(TempField, "%d", IntValue, static_array_len(TempField));
#else
		sscanf(TempField, "%d", IntValue);
#endif
	}

	return Ret;
}

b32 imgui_edit_float(struct engine *Engine, f32 *FloatValue, rect Rect)
{
	char TempField[64];
	snprintf(TempField, static_array_len(TempField), "%.2f", *FloatValue);
	b32 Ret = imgui_edit_text_filter_input(Engine, TempField, static_array_len(TempField), Rect, InputType_Float);

	if (Ret)
	{
#ifdef PLAT_WIN64
		sscanf_s(TempField, "%f", FloatValue, static_array_len(TempField));
#else
		sscanf(TempField, "%f", FloatValue);
#endif
	}
	return Ret;
}

b32 imgui_list_box(struct engine *Engine, const char **Labels, rect Rect, u32 *SelectedValue)
{
	imgui *ImGui = Engine->ImGui;
	ImGui->ObjectCount++;

	if (Labels == 0)
		return 0;

	u32 ElementsCount = dynamic_array_len(Labels);
	if (ElementsCount == 0)
		return 0;

	if (*SelectedValue >= ElementsCount)
	{
		*SelectedValue = 0;
	}

	const char *CurLabel = Labels[*SelectedValue];
	if (imgui_button(Engine, CurLabel, Rect))
		ImGui->ShowList = ImGui->ObjectCount;

	s32 ElementSize = ImGui->FontSize + 2;
	
	if (ImGui->ShowList == ImGui->ObjectCount)
	{
		f32 ZInc = 0.04f;
		vec2 GPos = imgui_container_get_world_pos(ImGui);
		GPos = vec2_add(GPos, Rect.Pos);
		rect PanelLayout = {.Pos = Rect.Pos};
		PanelLayout.Pos.y -= ((ElementSize*ElementsCount) - ElementSize)/2.0f;

		b32 MustScroll = 0;
		if (GPos.y - (ElementSize * ElementsCount) < Engine->Render->DesignScreenOrigin.y)
		{
			MustScroll = 1;
		}

		if (GPos.y > Engine->Render->DesignSize.y - GPos.y)
		{
			if (MustScroll)
			{
				PanelLayout.Rad = Vec2(Rect.Rad.x, GPos.y/2);
			}
			else
			{
				PanelLayout.Rad = Vec2(Rect.Rad.x, (f32)(ElementSize * ElementsCount)/2.0f);
			}
		}
		else
		{
			if (MustScroll)
			{
				PanelLayout.Rad = Vec2(Rect.Rad.x, (f32)(ElementSize * ElementsCount)/2.0f);
			}
			else
			{
				PanelLayout.Rad = Vec2(Rect.Rad.x, (f32)(ElementSize * ElementsCount)/2.0f);
			}
		}
		const char *ScrollName = string_new(Engine, "ListBox%d", ImGui->ObjectCount);
		imgui_begin_scroll_panel(Engine, PanelLayout, SKey(ScrollName));
		rect BtnRect;
		BtnRect.Rad = Rect.Rad;
		BtnRect.Pos = Vec2(0, PanelLayout.Rad.y - (ElementSize/2));
		ImGui->ZLayer -= ZInc;
		ImGui->InsideList = 1;

		for (s32 i=0; i<dynamic_array_len(Labels); ++i)
		{
			const char *Label = Labels[i];
			if (imgui_button(Engine, Label, BtnRect))
			{
				ImGui->ShowList = 0;
				imgui_end_scroll_panel(Engine);
				ImGui->InsideList = 0;
				*SelectedValue = i;
				return 1;
			}
			BtnRect.Pos.y -= ElementSize;
		}

		ImGui->InsideList = 0;
		ImGui->ZLayer += ZInc;
		imgui_end_scroll_panel(Engine);
		string_delete(Engine, (char *)ScrollName);
	}

	return 0;
}

b32 imgui_tick_box(struct engine *Engine, b32 Value, rect Rect)
{
	f32 ZP = Engine->ImGui->ZLayer - 0.02f;
	if (Value)
	{
		imgui_record_last_scroll_pos(Engine->ImGui, Rect.Pos);
		render_set_default_color(Engine, Engine->ImGui->Theme.BorderColor);
		vec2 GPos = vec2_add(imgui_container_get_world_pos(Engine->ImGui), Rect.Pos);
		render_draw_rect(Engine, vec2_to3(GPos, ZP), vec2_mul(Rect.Rad, 1.2f));
	}

	b32 Ret = imgui_button(Engine, "", Rect);
	return Ret;
}

static imgui_container *imgui_scroll_container_get(imgui *Gui, skey *Key)
{
	for (s32 i=0; i<dynamic_array_len(Gui->ScrollContainers); ++i)
	{
		imgui_container *C = Gui->ScrollContainers + i;
		if (skey_compare_fast(&C->Name, Key))
		{
			return C;
		}
	}

	return 0;
}

void imgui_begin_scroll_panel(engine *Engine, rect Layout, skey Key)
{
	struct opengl *GL = &Engine->Render->OpenGL;
	imgui *Gui = Engine->ImGui; 

	imgui_begin_container(Engine, Layout, Key);

	//Getting g pos before changing the scroll
	vec2 GPos = imgui_container_get_world_pos(Gui);

	imgui_container *CurCont = imgui_scroll_container_get(Gui, &Key);
	Gui->OpenContainer = CurCont;

	if (!CurCont)
	{
		CurCont = Gui->Containers + dynamic_array_len(Gui->Containers) - 1;
		dynamic_array_add(&Gui->ScrollContainers, *CurCont);
	}

	CurCont->IsActive = 1;

	imgui_container *LastC = Gui->Containers + dynamic_array_len(Gui->Containers) - 1;
	imgui_container *LastScrollContainer = Gui->ScrollContainers + dynamic_array_len(Gui->ScrollContainers) - 1;

	if (skey_compare_fast(&Gui->PanelInFocus.Cur, &Key) && skey_compare_fast(&LastC->Name, &LastScrollContainer->Name))
	{
		CurCont->ScrollPos.y += Gui->ScrollDelta;
		//TODO: Fix clamp in scroll
		/*
		f32 MaxVal = (Gui->OpenContainer->LastScrollPos * -1) - CurCont->Layout.Rad.y;
		if (MaxVal >= 0)
			CurCont->ScrollPos.y = clamp(CurCont->ScrollPos.y, 0, MaxVal);
		else
			CurCont->ScrollPos.y = clamp(CurCont->ScrollPos.y, MaxVal, 0);
			*/
	}

	LastC->ScrollPos = CurCont->ScrollPos;

	rect TestRectLayout = Rect(GPos, Layout.Rad);
	if (collision_test_rect_point(TestRectLayout, Engine->Input->MouseState.MousePos))
	{
		Gui->PanelInFocus.Wip = Key;
	}

	Engine->ImGui->ZLayer -= 0.03f;

	render_set_default_color(Engine, Engine->ImGui->Theme.BorderColor);
	f32 ZP = Engine->ImGui->ZLayer - 0.04f;
	vec2 LeftBottomP = vec2_sub(GPos, Layout.Rad);
	vec2 RightTopP = vec2_add(GPos, Layout.Rad);
	render_draw_line(Engine, vec2_to3(LeftBottomP, ZP), Vec3(RightTopP.x, LeftBottomP.y, ZP));
	render_draw_line(Engine, vec2_to3(Vec2(RightTopP.x, LeftBottomP.y), ZP), vec2_to3(RightTopP, ZP));
	render_draw_line(Engine, vec2_to3(RightTopP, ZP), Vec3(LeftBottomP.x, RightTopP.y, ZP));
	render_draw_line(Engine, Vec3(LeftBottomP.x, RightTopP.y, ZP), vec2_to3(LeftBottomP, ZP));

	ZP = Engine->ImGui->ZLayer - 0.02f;

	u8 StencilValue = ++Engine->ImGui->ScrollContainerOpenNum;
	render_stencil_write(Engine, StencilValue);

	render_set_default_color(Engine, Engine->ImGui->Theme.DefaultColor);
	render_draw_rect(Engine, vec2_to3(GPos, ZP), vec2_mul(Layout.Rad, 2));

	render_stencil_compare(Engine, StencilValue);
}

void imgui_end_scroll_panel(engine *Engine)
{
	imgui *ImGui = Engine->ImGui;
	//TODO: this zlayer is getting added twice 
	ImGui->ZLayer += 0.03f;

	//Pop the container
	//Before deleting it, Search it in the ScrollContainers
	s32 Len = dynamic_array_len(ImGui->Containers);
	imgui_container *LastContainer = ImGui->Containers + Len - 1;
	s32 SCIndex = 0;
	for (SCIndex=0; SCIndex<dynamic_array_len(ImGui->ScrollContainers); ++SCIndex)
	{
		if (skey_compare_fast(&LastContainer->Name, &((ImGui->ScrollContainers + SCIndex)->Name)))
			break;
	}
	
	if (SCIndex > 0)
	{
		//The last one will get deleted
		ImGui->OpenContainer = ImGui->ScrollContainers + SCIndex - 1;
	}
	else
	{
		ImGui->OpenContainer = 0;
	}

	--ImGui->ScrollContainerOpenNum;

	imgui_end_container(Engine);
	render_stencil_end(Engine);
}

