#include "gbg_world.h"
#include "gbg_rtti.h"

rigid_body RigidBody_Circle(vec2 Pos, f32 Rad)
{
	rigid_body Ret = {.Type = RB_Circle, .Circle = {Pos, Rad}};
	return Ret;
}

rigid_body RigidBody_Rect(vec2 Pos, vec2 Rad)
{
	rigid_body Ret = {.Type = RB_Rect, .Rect = {Pos, Rad}};
	return Ret;
}

static entity *entity_make_valid(world *World, entity *Ent)
{
	entity New = {.Scale={1, 1}, .IsValid=1};

	*Ent = New;

	return Ent;
}

static void record_diff(engine *Engine, skey **OutNamesArray, const char *NamePath, rtti_record *Record, const u8 *Original, const u8* Compare);

static void _record_field_diff(engine *Engine, skey **OutNamesArray, const char *NamePath, rtti_field *Field, const u8 *Original, const u8* Compare)
{
	char *NewName = Field->Name;
	if (NamePath)
	{
		NewName = string_new(Engine, "%s.%s", NamePath, Field->Name);
	}
	record_diff(Engine, OutNamesArray, NewName, rtti_record_get_by_name(Engine, Field->TypeAsString), Original, Compare);
	if (NamePath)
	{
		string_delete(Engine, NewName);
	}
}

static void record_diff(engine *Engine, skey **OutNamesArray, const char *NamePath, rtti_record *Record, const u8 *Original, const u8* Compare)
{
	//Handle special case that there's no original imprint, everything is new then
	if (Original == 0)
	{
		for (s32 i=0; i<dynamic_array_len(Record->Fields); ++i)
		{
			rtti_field *Field = Record->Fields + i;
			skey NameKey = SKey(Field->Name);
			dynamic_array_add(OutNamesArray, NameKey);
		}

		return;
	}

	for (s32 i=0; i<dynamic_array_len(Record->Fields); ++i)
	{
		rtti_field *Field = Record->Fields + i;

		switch (Field->Type)
		{
			case RFT_Unknown:
				//Only case allowed for it it's to be a void* or similar
				if (Field->IsPointer)
					continue;
				assert(0);
			break;

			case RFT_String:
			{
				const char **OrigStr = (const char **)(Original + Field->Offset);
				const char **CompStr = (const char **)(Compare + Field->Offset);
				if (!string_compare(*OrigStr, *CompStr))
				{
					skey NameKey = SKey(Field->Name);
					dynamic_array_add(OutNamesArray, NameKey);
				}
			} break;

			case RFT_Struct:
			{
				const u8 *OrigSubObj = Original + Field->Offset;
				const u8 *CompSubObj = Compare + Field->Offset;
				_record_field_diff(Engine, OutNamesArray, NamePath, Field, OrigSubObj, CompSubObj);
			}	break;
			case RFT_Union:
			{
				rtti_record *UnionRecord = rtti_record_get_by_name(Engine, Field->TypeAsString);
				rtti_field *BiggestField = rtti_union_get_biggest_field(UnionRecord);
				_record_field_diff(Engine, OutNamesArray, NamePath, BiggestField, Original + Field->Offset, Compare + Field->Offset);
			}	break;
			default:
				if (!byte_compare(Field->Size, Original + Field->Offset, Compare + Field->Offset))
				{
					skey NameKey = SKey(Field->Name);
					dynamic_array_add(OutNamesArray, NameKey);
				}
				break;
		}
	}
}

void entity_diff(engine *Engine, skey **OutNamesArray, rtti_record *Record, const entity *Original, const entity* Compare)
{
	record_diff(Engine, OutNamesArray, 0, Record, (const u8*)Original, (const u8*)Compare);
}

void *_entity_get_game_data(entity *Ent, const char *TypeName)
{
	if (Ent->GameData.Ptr)
	{
		if (string_compare(Ent->GameData.RecordName, TypeName))
			return Ent->GameData.Ptr;
	}

	return 0;
}

b32 collision_vs(collision_info *ColInfo, u32 UserTag1, u32 UserTag2, entity **Ent1, entity **Ent2)
{
	b32 IsType1 = ColInfo->EntA->RigidBody.UserTag == UserTag1;
	b32 IsType2 = 0;
	if (IsType1)
	{
		*Ent1 = ColInfo->EntA;
		IsType2 = ColInfo->EntB->RigidBody.UserTag == UserTag2;
		if (IsType2)
		{
			*Ent2 = ColInfo->EntB;
			return 1;
		}
	}
	else
	{
		IsType1 = ColInfo->EntB->RigidBody.UserTag == UserTag1;
		if (IsType1)
		{
			*Ent1 = ColInfo->EntB;
			IsType2 = ColInfo->EntA->RigidBody.UserTag == UserTag2;
			if (IsType2)
			{
				*Ent2 = ColInfo->EntA;
				return 1;
			}
		}
	}

	return 0;
}

entity *_world_entity_create_in(engine *Engine, entity_cluster *EntityCluster)
{
	world *World = Engine->World;
	entity_cluster *Cluster = EntityCluster;

	for (;;)
	{
		for (u32 i=0; i<static_array_len(Cluster->E); ++i)
		{
			entity *Ent = Cluster->E + i;
			if (!Ent->IsValid)
			{
				return entity_make_valid(World, Ent);
			}
		}	

		if (Cluster->Next)
		{
			Cluster = Cluster->Next;
		}
		else
		{
			entity_cluster *NewCluster = memory_alloc_type(&Engine->Memory, entity_cluster);
			memset(NewCluster, 0, sizeof(entity_cluster));
			Cluster->Next = NewCluster;
			Cluster = NewCluster;
		}
	}

	assert(0);
	return 0;
}

entity *_world_entity_create_from_imprint_using(engine *Engine, imprint *Imprint, entity_cluster *EntityCluster)
{
	entity *Ret = _world_entity_create_in(Engine, EntityCluster);
	rtti_record_copy(Engine, Engine->Rtti->CachedEntityRecord, Imprint->Entity, Ret);
	Ret->IsValid = 1;
	return Ret;
}

entity *world_entity_create_from_imprint(engine *Engine, imprint *Imprint)
{
	return _world_entity_create_from_imprint_using(Engine, Imprint, &Engine->World->Entities);
}

entity *world_entity_create_from_imprint_in_editor(struct engine *Engine, struct imprint *Imprint)
{
	return _world_entity_create_from_imprint_using(Engine, Imprint, &Engine->World->EntitiesFromEditor);
}

entity *world_entity_create(struct engine *Engine)
{
	return _world_entity_create_in(Engine, &Engine->World->Entities);
}

entity *world_entity_create_in_editor(struct engine *Engine)
{
	return _world_entity_create_in(Engine, &Engine->World->EntitiesFromEditor);
}

void entity_destroy(struct engine *Engine, entity *Entity)
{
	Entity->IsValid = 0;
	//TODO: Think on how to notify the user that the entity is being deleted. Maybe more cleanup is needed. Mayne a deeper clean up with reflection, gamedata inside gamedata
	//Maybe it's not needed at all
	memory_free(&Engine->Memory, Entity->GameData.Ptr);
	Entity->GameData.Ptr = 0;
	if (Entity->GameData.RecordName)
	{
		string_delete(Engine, Entity->GameData.RecordName);
	}
}

void entity_move(struct engine *Engine, entity *Ent, vec2 Vel)
{
	Ent->Vel = Vel;
	Ent->MoveFrameFlag = true;
}

void entity_render(struct engine *Engine, entity *Ent)
{
	render_params *Params = &Ent->RenderParams;
	render_set_default_color(Engine, Params->Color);

	if (Params->AssetRef.FilePath)
	{
		skey NameKey = SKey(Params->AssetRef.FilePath);
		asset *Asset = asset_get(Engine, NameKey);
		vec2 *Size = &Params->Size;
		vec2 ScaledSize = Vec2(Ent->Scale.x * Size->x, Ent->Scale.y * Size->y);
		if (Asset->Type == AssetType_Texture)
		{
			sprite Sprite = {Asset, Params->UVRect, Params->Color};
			render_draw_sprite(Engine, Sprite, Ent->Pos, ScaledSize, Ent->Rot, Params->BlendMode);
		}
		else if (Asset->Type == AssetType_SpriteSheet)
		{
			sprite Sprite = {Asset, {{0, 0}, {(f32)Asset->SpriteSheetAsset.Texture.Width, (f32)Asset->SpriteSheetAsset.Texture.Height}}, Params->Color};
			sprite_sheet SpriteSheet = {Sprite, Asset->SpriteSheetAsset.SpriteW, Asset->SpriteSheetAsset.SpriteH};
			render_draw_sprite_in_sheet(Engine, SpriteSheet, Params->ColAndRow, Ent->Pos, ScaledSize, Ent->Rot, Params->BlendMode);
		}
		else
		{
			assert(0);
		}
	}
	else
	{
		if (Ent->RigidBody.Type == RB_Circle)
		{
			render_draw_circle(Engine, Ent->Pos, Ent->RigidBody.Circle.Rad);
		}
		else
		{
			vec2 *RadSize = &Ent->RigidBody.Rect.Rad;
			vec2 ScaledSize = Vec2(Ent->Scale.x * RadSize->x, Ent->Scale.y * RadSize->y);
			render_draw_rect(Engine, Ent->Pos, vec2_mul(ScaledSize, 2));
		}
	}
}

void world_init(engine *Engine)
{
	Engine->World = memory_alloc_type(&Engine->Memory, world);
	memset(Engine->World, 0, sizeof(world));
	Engine->World->FrameCollisions = dynamic_array_create(Engine, collision_info);
	Engine->World->CollisionFilters = dynamic_array_create(Engine, collision_filter);
	Engine->World->Extent = Rect(Vec2(500, 250), Vec2(600, 400));
}

static void world_clear_level_cluster(engine *Engine, entity_cluster *Cluster)
{
	entity_cluster *CurCluster = Cluster;
	while(CurCluster)
	{
		for (u32 i=0; i<static_array_len(CurCluster->E); ++i)
		{
			entity *EditorEnt = CurCluster->E + i;
			if (EditorEnt->IsValid)
			{
				entity_destroy(Engine, EditorEnt);
			}
		}

		CurCluster = CurCluster->Next;
	}

	//Delete clusters
	CurCluster = Cluster->Next;
	while (CurCluster)
	{
		if (CurCluster->Next)
		{
			Cluster->Next = CurCluster->Next;
			memory_free(&Engine->Memory, CurCluster);
		}
		else
		{
			memory_free(&Engine->Memory, CurCluster);
			Cluster->Next = 0;
		}
		CurCluster = Cluster->Next;
	}
}

//TODO: Cap DeltaTime to use a fixed time step
void world_update(struct engine *Engine, f32 DeltaTime)
{
	world *World = Engine->World;
	f32 MaxDT = clamp(DeltaTime, 0.0f, 0.16667f);
	dynamic_array_clear(World->FrameCollisions);

	//This order is important. 
	//1. Current transition to state
	if (World->SetTransition == WorldTransition_Play)
	{
		entity_cluster *Cluster = &World->EntitiesFromEditor;
		while(Cluster)
		{
			for (u32 i=0; i<static_array_len(Cluster->E); ++i)
			{
				entity *EditorEnt = Cluster->E + i;
				if (EditorEnt->IsValid)
				{
					entity *NewEnt = world_entity_create(Engine);
					rtti_record_copy(Engine, Engine->Rtti->CachedEntityRecord, EditorEnt, NewEnt);
				}
			}

			Cluster = Cluster->Next;
		}
	}
	else if (World->SetTransition == WorldTransition_Stop)
	{
		world_clear_level(Engine);
	}

	if (World->CurrentTransition == WorldTransition_Play)
	{
		World->CurrentState = WorldState_Play;
	}
	else if (World->CurrentTransition == WorldTransition_Stop)
	{
		World->CurrentState = WorldState_Stop;
	}

	//2. Clear transition
	if (World->CurrentTransition != WorldTransition_None)
	{
		World->CurrentTransition = WorldTransition_None;
	}

	//3. Set current transition
	World->CurrentTransition = World->SetTransition;

	//4. Clear set transition
	if (World->SetTransition != WorldTransition_None)
	{
		World->SetTransition = WorldTransition_None;
	}

	entity_cluster *Cluster = 0;

	if (World->CurrentState == WorldState_Stop)
	{
		Cluster = &World->EntitiesFromEditor;
		while(Cluster)
		{
			for (u32 i=0; i<static_array_len(Cluster->E); ++i)
			{
				//Only render editor, it should be in its own state
				entity *EdEnt = Cluster->E + i;
				if (EdEnt->IsValid)
				{
					entity_render(Engine, EdEnt);
				}
			}

			Cluster = Cluster->Next;
		}	
	}

	Cluster = &World->Entities;
	while(Cluster)
	{
		for (u32 i=0; i<static_array_len(Cluster->E); ++i)
		{
			entity *Ent = Cluster->E + i;
			if (Ent->IsValid)
			{
				if (!collision_test_rect_point(World->Extent, vec3_to2(Ent->Pos)))
				{
					entity_destroy(Engine, Ent);
					continue;
				}
				entity_render(Engine, Ent);

				if (World->IsPaused)
					continue;

				//physics
				{
					if (Ent->Vel.x != 0 || Ent->Vel.y != 0)
					{
						vec2 MoveDir = vec2_mul(Ent->Vel, MaxDT);
						f32 MoveBudget = vec2_length(MoveDir);
						f32 TotalMove = MoveBudget;
						while (MoveBudget > 0)
						{
							collision_info FirstHits[] = {{.Distance = FLT_MAX}, {.Distance = FLT_MAX}, {.Distance = FLT_MAX}, {.Distance = FLT_MAX}};
							u32 FirstHitIndex = 0;
							collision_info *FirstHit = FirstHits + FirstHitIndex;
							entity_cluster *HitCluster = &World->Entities;
							while (HitCluster)
							{
								for (u32 e=0; e<static_array_len(HitCluster->E); ++e)
								{
									entity *HitEnt = HitCluster->E + e;

									b32 IgnoreCollision = 0;
									//Check filters
									for (u32 FilterIx=0; FilterIx<dynamic_array_len(World->CollisionFilters); ++FilterIx)
									{
										collision_filter *ColFilter = World->CollisionFilters + FilterIx;
										if ((ColFilter->Tag1 == Ent->RigidBody.UserTag && ColFilter->Tag2 == HitEnt->RigidBody.UserTag) || (ColFilter->Tag2 == Ent->RigidBody.UserTag && ColFilter->Tag1 == HitEnt->RigidBody.UserTag))
										{
											if (ColFilter->Type == CFT_Ignore)
											{
												IgnoreCollision = 1;
												break;
											}
										}
									}

									if (IgnoreCollision)
										continue;

									if (HitEnt->IsValid && HitEnt != Ent)
									{
										if (Ent->RigidBody.Type == RB_Circle)
										{
											if (HitEnt->RigidBody.Type == RB_Rect)
											{
												rect TestRect = HitEnt->RigidBody.Rect;
												TestRect.Pos = vec2_add(TestRect.Pos, vec3_to2(HitEnt->Pos));
												TestRect.Rad = vec2_add(TestRect.Rad, Vec2(Ent->RigidBody.Circle.Rad, Ent->RigidBody.Circle.Rad));
												f32 TMin;
												vec2 HitP, HitN;
												if (collision_ray_rect(vec3_to2(Ent->Pos), MoveDir, TestRect, &TMin, &HitP, &HitN))
												{
													if (MoveBudget >= TMin)
													{
														collision_info ColInfo = {.EntA = Ent, .EntB = HitEnt, 
															.Distance = TMin, .NormalizedDistance = TMin/TotalMove,
															.P = HitP, .N = HitN, .IsInside = TMin < GBG_EPSILON ? 1: 0	
														};

														if (!HitEnt->IsSensor) 
														{
															if (FirstHit->Distance > TMin)
															{
																*FirstHit = ColInfo;
															}
															else if (FirstHit->Distance == TMin)
															{
																//Multiple collisions at the same time
																++FirstHitIndex;
																FirstHit = FirstHits + FirstHitIndex;
																*FirstHit = ColInfo;
															}
														}
														else if (HitEnt->IsSensor)
														{
															dynamic_array_add(&World->FrameCollisions, ColInfo);
														}
													}
												}
											}
										}
										else if (Ent->RigidBody.Type == RB_Rect)
										{
											if (HitEnt->RigidBody.Type == RB_Rect)
											{
												rect TestRect = HitEnt->RigidBody.Rect;
												TestRect.Pos = vec2_add(TestRect.Pos, vec3_to2(HitEnt->Pos));
												TestRect.Rad = vec2_add(TestRect.Rad, Ent->RigidBody.Rect.Rad);
												f32 TMin;
												vec2 HitP, HitN;
												if (collision_ray_rect(vec3_to2(Ent->Pos), MoveDir, TestRect, &TMin, &HitP, &HitN))
												{
													//TODO: Fix this part of the collision too
													if (MoveBudget >= TMin)
													{
														collision_info ColInfo = {.EntA = Ent, .EntB = HitEnt, 
															.Distance = TMin, .NormalizedDistance = TMin/TotalMove,
															.P = HitP, .N = HitN, .IsInside = TMin < GBG_EPSILON ? 1: 0	
														};

														if (!HitEnt->IsSensor && FirstHit->Distance > TMin)
														{
															*FirstHit = ColInfo;
														}
														else if (HitEnt->IsSensor)
														{
															dynamic_array_add(&World->FrameCollisions, ColInfo);
														}
													}
												}
											}
										}
									}
								}

								HitCluster = HitCluster->Next;
							}

							if (FirstHit->EntB)
							{
								for (u32 a=0; a<2; ++a)
								{
									if (FirstHit->N.E[a] != 0)
									{
										Ent->Vel.E[a] = Ent->Vel.E[a] * -1;
									}
								}

								Ent->Pos.x = FirstHit->P.x;
								Ent->Pos.y = FirstHit->P.y;

								//Trying to separate object so they don't penetrate among them
								Ent->Pos = vec3_add(Ent->Pos, vec2_to3(FirstHit->N, 0));
								MoveBudget -= FirstHit->Distance;
								//Velocity should have changed with collision response
								MoveDir = vec2_mul(Ent->Vel, MaxDT*(MoveBudget/TotalMove));

								for (u32 HitNum = 0; HitNum <= FirstHitIndex; ++HitNum)
								{
									dynamic_array_add(&World->FrameCollisions, FirstHits[HitNum]);
								}
							}
							else
							{
								MoveBudget = 0;
								Ent->Pos = vec3_add(Ent->Pos, vec2_to3(MoveDir, 0));
							}
						}

						if (Ent->MoveFrameFlag)
						{
							Ent->MoveFrameFlag = false;
							Ent->Vel = Vec2(0, 0);
						}
					}
				}
			}
		}

		Cluster = Cluster->Next;
	}
}

void world_clear_level_editor(engine *Engine)
{
	world_clear_level_cluster(Engine, &Engine->World->EntitiesFromEditor);
}

void world_query_entities(engine *Engine, u32 RigidBodyUserTag, query_entity_fn Func)
{
	entity_cluster *Cluster = &Engine->World->Entities;
	while(Cluster)
	{
		for (u32 i=0; i<static_array_len(Cluster->E); ++i)
		{
			entity *Ent = Cluster->E + i;
			if (Ent->IsValid)
			{
				if (Ent->RigidBody.UserTag == RigidBodyUserTag)
				{
					Func(Engine, Ent);
				}
			}
		}
		Cluster = Cluster->Next;
	}
}

typedef struct world_load_table
{
	void (*ClearFunc)(engine *Engine);
	entity *(*CreateEntityFunc)(engine *Engine);
} world_load_table;

static b32 world_load_level_table_(engine *Engine, const char *LevelName, world_load_table *Table)
{
	if (LevelName)
	{
		Table->ClearFunc(Engine);

		file_handle LvlHandle = Engine->Platform->FileOpen(LevelName);
		if (LvlHandle.Descriptor == -1)
		{
			Engine->Platform->Log("Error: Level name [%s] was not found!\n", LevelName);
			return 0;
		}

		u8 LVL[3];
		u32 ReadFrom = Engine->Platform->FileRead(LvlHandle, LVL, sizeof(LVL), 0);
		if (LVL[0] == 'L' && LVL[1] == 'V' && LVL[2] == 'L')
		{
			ReadFrom += read_record(Engine, LvlHandle, ReadFrom, rtti_record_get_by_name(Engine, "gbg_level_config"), (u8*)&Engine->World->UserConfig);
			u32 EntCount = 0;
			ReadFrom += Engine->Platform->FileRead(LvlHandle, (u8*)&EntCount, sizeof(EntCount), ReadFrom);
			for (u32 i=0; i < EntCount; ++i)
			{
				entity *NewEnt = Table->CreateEntityFunc(Engine);
				char *ImprintName;
				ReadFrom += read_string(Engine, LvlHandle, ReadFrom, &ImprintName);
				skey ImprintKey = SKey(ImprintName);
				imprint *Imprint = imprint_get(Engine, ImprintKey);
				rtti_record_copy(Engine, Engine->Rtti->CachedEntityRecord, Imprint->Entity, NewEnt);

				ReadFrom += read_record_fields(Engine, LvlHandle, ReadFrom, Engine->Rtti->CachedEntityRecord, (u8*)NewEnt);
			}
		}
		Engine->Platform->FileClose(LvlHandle);
	}
	else
	{
		Engine->Platform->Log("Error: Level name is not valid!\n");
		return 0;
	}

	return 1;
}

void world_clear_level(engine *Engine)
{
	world_clear_level_cluster(Engine, &Engine->World->Entities);
}

b32 world_load_level(engine *Engine, const char *LevelName)
{
	world_load_table Table;
	Table.ClearFunc = world_clear_level;
	Table.CreateEntityFunc = world_entity_create;
	return world_load_level_table_(Engine, LevelName, &Table);
}

b32 world_load_level_in_editor(engine *Engine, const char *LevelName)
{
	world_load_table Table;
	Table.ClearFunc = world_clear_level_editor;
	Table.CreateEntityFunc = world_entity_create_in_editor;
	return world_load_level_table_(Engine, LevelName, &Table);
}

entity *world_get_entity_under_rect(struct engine *Engine, rect TestRect)
{
	entity_cluster *Cluster = &Engine->World->EntitiesFromEditor;
	while(Cluster)
	{
		for (u32 i=0; i<static_array_len(Cluster->E); ++i)
		{
			entity *Ent = Cluster->E + i;
			if (Ent->IsValid)
			{
				rect EntRect;
				EntRect.Pos = vec3_to2(Ent->Pos);
				EntRect.Rad = vec2_div(Ent->RenderParams.Size, 2);
				if (collision_test_rect_vs_rect(TestRect, EntRect))
				{
					return Ent;
				}
			}
		}
		Cluster = Cluster->Next;
	}

	return 0;
}

entity *world_get_entity_under_pos(struct engine *Engine, vec2 Pos)
{
	entity_cluster *Cluster = &Engine->World->EntitiesFromEditor;
	while(Cluster)
	{
		for (u32 i=0; i<static_array_len(Cluster->E); ++i)
		{
			entity *Ent = Cluster->E + i;
			if (Ent->IsValid)
			{
				rect TestRect;
				TestRect.Pos = vec3_to2(Ent->Pos);
				TestRect.Rad = vec2_div(Ent->RenderParams.Size, 2);
				if (collision_test_rect_point(TestRect, Pos))
				{
					return Ent;
				}
			}
		}
		Cluster = Cluster->Next;
	}

	return 0;
}
