#ifndef GBG_SOUND_H
#define GBG_SOUND_H

#include "gbg_platform.h"

typedef struct sound_asset
{
	u8 *Data;
	u32 DataSize;
	b32 Loop;
	f32 Gain;
	b32 StreamFromDisk;
	b32 Paused;
	u8 *StreamBuffer;
	file_handle StreamFile;
	u32 StreamFileOffset;
	u32 StreamBeginFileOffset;
	b32 StreamRequest;
}sound_asset;

typedef struct stream_sound_data
{
	sound_asset *SoundAsset;
	u32 Header;
}stream_sound_data;

typedef enum sound_command_type
{
	SCT_Play,
	SCT_Stop,
} sound_command_type;

typedef struct sound_command
{
	sound_command_type Type;
	sound_asset *SoundAsset;
} sound_command;

typedef struct sound_command_circular_buffer
{
	u32 Produced;
	u32 Consumed;
	sound_command Commands[64];
} sound_command_circular_buffer;

typedef struct sound_system
{
	stream_sound_data *StreamSoundData;
	sound_command_circular_buffer SoundCommands;
	b32 StreamLock;
	f32 MasterVolume;
	u8 Channels;
	u16 SampleRate;
} sound_system;

struct engine;
void sound_init(struct engine *Engine);
void sound_play(struct engine *Engine, const char *Name);
void sound_stop(struct engine *Engine, const char *Name);
void sound_set_pause(struct engine *Engine, const char *Name, b32 Pause);
void sound_set_loop(struct engine *Engine, const char *Name);
void sound_set_master_volume(struct engine *Engine, f32 Volume);
void sound_request_buffer_mix(struct engine *Engine, s8 *StreamBuffer, u32 BufferLen);
void sound_update(struct engine *Engine);

void sound_set_gain(struct engine *Engine, const char *Name, f32 Gain);
f32 sound_get_gain(struct engine *Engine, const char *Name);


#endif
