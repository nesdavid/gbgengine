#ifndef GBG_EDITOR_H
#define GBG_EDITOR_H

#include <gbg_math.h>

//From where is this selection coming from?
//From the editor imprints or the in game entities?
typedef enum selection_mode
{
	SM_Imprint,
	SM_EntityLevel,
} selection_mode;

typedef struct selected_imprint
{
	imprint Imprint;
	selection_mode Mode;
} selected_imprint;

typedef struct editor_config
{
	b32 HideEntityCreation;
	b32 HideEntityEdit;
	b32 AllowOnlyGameDataEdit;
	b32 PaintMode;
	char **HideImprints;
} editor_config;

GOBJECT()
typedef struct sprite_sheet_config
{
	u32 SpriteW;
	u32 SpriteH;
	u32 OrderId;
} sprite_sheet_config;

typedef struct editor
{
	b32 Show;
	struct rtti_record *NewSelected;

	char NameOut[64];
	void *TempObject;
	u32 *ListBoxIndices;
	u32 ListBoxCurIndex;
	char **RecordNames;
	char **TextureNames;
	char **AvailableAssets;
	char *LevelsDir;

	selected_imprint LastEntityClicked;

	//For every entity placed in the world is needed to know from which imprint, 
	//if any is that one coming from.
	imprint *InWorldImprints;

	char *CurrentWorldName;

	b32 *CollapsedStatus;
	u32 CollapseCount;

	editor_config Config;

	vec2 TileDim;
	vec2 WorldDimInTiles;
	vec2 WorldStart;
}editor;

void editor_init(struct engine *Engine);
void editor_update(struct engine *Engine);
void editor_show_record(struct engine *Engine, struct rtti_record *Struct, void *Obj, struct vec2 *StartPos);
void editor_set_default_theme(struct engine *Engine);

#endif
