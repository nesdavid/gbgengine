#ifndef GBG_ASSETS_H
#define GBG_ASSETS_H

#include "gbg_string.h"
#include "gbg_sound.h"
#include "gbg_rtti.h"

struct texture_asset
{
	u32 TextureId;
	u32 RenderBatchId;
	u32 Height;
	u32 Width;
	u32 Format;
	u8 *Data;
};

typedef struct sprite_sheet_asset
{
	struct texture_asset Texture;
	u32 SpriteW;
	u32 SpriteH;
} sprite_sheet_asset;

GENUM()
typedef enum asset_type
{
	AssetType_None,
	AssetType_Sound,
	AssetType_Texture,
	AssetType_SpriteSheet,

	AssetType_Max
} asset_type;

//TODO: add sound hot reloading
typedef struct asset
{
	skey PathName;
	asset_type Type;
	union 
	{
		sound_asset SoundAsset;	
		struct texture_asset TextureAsset;
		struct sprite_sheet_asset SpriteSheetAsset;
	};

	struct asset *Next;
} asset;

//point to an asset on disc. 
// I'm still unsure about this struct/concept. I'd like it to be the reference to load/unload/point to
// any asset.
// For now it's only a char* that points to the file on disc without the extension
//It's also useful for the editor to show a special input widget
GOBJECT()
typedef struct asset_ref
{
	char *FilePath;
} asset_ref;

#if DEBUG
typedef struct asset_timestamp
{
	u64 Time;
	asset *Asset;
} asset_timestamp;
#endif

GOBJECT()
typedef struct asset_config
{
	asset_type Type;
	asset_ref AssetRef;
	named_pointer Config;
} asset_config;

typedef struct asset_database
{
	asset Assets[64];
	char *RootPath;

#if DEBUG
	asset_timestamp *Timestamps;
#endif

	imprint *SavedImprints;
} asset_database;

struct engine;

imprint *imprint_get(struct engine *Engine, skey Name);

//Attempts to load the sound specified with Name. It will return early if sound is already loaded
void asset_load_sound(struct engine *Engine, const char *Name);
void asset_load_sound_streamed(struct engine *Engine, const char *Name);

//Return valid asset or null if not found. 
asset *asset_get(struct engine *Engine, skey Name);

u8 *asset_load_file(struct engine *Engine, const char *Name, u32 *Size);

void asset_load_texture(struct engine *Engine, const char *Name);

void asset_init(struct engine *Engine);
void asset_update(struct engine *Engine);

typedef void (*asset_query_callback)(struct engine *Engine, asset *Asset);

void asset_get_all_of_type(struct engine *Engine, asset_type Type, asset_query_callback Callback);

#endif
