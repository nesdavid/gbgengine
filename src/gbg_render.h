#ifndef GBG_RENDER_H
#define GBG_RENDER_H

#ifdef RENDER_GLES
#include <GLES3/gl31.h>
 #ifdef PLAT_ANDROID
 #include <GLES3/gl3ext.h>
 #elif defined(PLAT_HTML)
 #include <GLES3/gl2ext.h>
 #endif
#else
#include "../third_party/glcorearb.h"
#include "../third_party/glext.h"
#endif

#include "gbg_math.h"
#include "gbg_assets.h"

//TODO: Render is tied to opengl, it may change in the future if a new render platform is added

struct opengl
{
	PFNGLATTACHSHADERPROC AttachShader;
	PFNGLACTIVETEXTUREPROC ActiveTexture;
	PFNGLBEGINTRANSFORMFEEDBACKPROC BeginTransformFeedback;
	PFNGLBINDBUFFERPROC BindBuffer;
	PFNGLBINDVERTEXARRAYPROC BindVertexArray;
	PFNGLBINDTEXTUREPROC BindTexture;
	PFNGLBINDBUFFERBASEPROC BindBufferBase;
	PFNGLBLENDFUNCPROC BlendFunc;
	PFNGLBUFFERDATAPROC BufferData;
	PFNGLBUFFERSUBDATAPROC BufferSubData;
	PFNGLCLEARPROC Clear;
	PFNGLCLEARCOLORPROC ClearColor;
	PFNGLCLEARDEPTHFPROC ClearDepthf;
	PFNGLCREATESHADERPROC CreateShader;
	PFNGLCREATEPROGRAMPROC CreateProgram;
	PFNGLCOMPILESHADERPROC CompileShader;
	PFNGLCULLFACEPROC CullFace;
	PFNGLDELETEBUFFERSPROC DeleteBuffers;
	PFNGLDELETEPROGRAMPROC DeleteProgram;
	PFNGLDELETESHADERPROC DeleteShader;
	PFNGLDELETEVERTEXARRAYSPROC DeleteVertexArrays;
	PFNGLDELETETEXTURESPROC DeleteTextures;
	PFNGLDETACHSHADERPROC DetachShader;
	PFNGLDEPTHFUNCPROC DepthFunc;
	PFNGLDEPTHMASKPROC DepthMask;
	PFNGLDEPTHRANGEFPROC DepthRangef;
	PFNGLDISABLEPROC Disable;
	PFNGLDISABLEVERTEXATTRIBARRAYPROC DisableVertexAttribArray;
	PFNGLDRAWARRAYSPROC DrawArrays;
	PFNGLDRAWELEMENTSPROC DrawElements;
	PFNGLDRAWELEMENTSINSTANCEDPROC DrawElementsInstanced;
	PFNGLDRAWRANGEELEMENTSPROC DrawRangeElements;
	PFNGLENABLEPROC Enable;
	PFNGLENABLEVERTEXATTRIBARRAYPROC EnableVertexAttribArray;
	PFNGLFRONTFACEPROC FrontFace;
	PFNGLGENBUFFERSPROC GenBuffers;
	PFNGLGENVERTEXARRAYSPROC GenVertexArrays;
	PFNGLGENTEXTURESPROC GenTextures;
	PFNGLGETERRORPROC GetError;
	PFNGLGETSHADERIVPROC GetShaderiv;
	PFNGLGETSTRINGPROC GetString;
	PFNGLGETSTRINGIPROC GetStringi;
	PFNGLGETINTEGERVPROC GetIntegerv;
	PFNGLGETSHADERINFOLOGPROC GetShaderInfoLog;
	PFNGLGETUNIFORMLOCATIONPROC GetUniformLocation;
	PFNGLGETUNIFORMBLOCKINDEXPROC GetUniformBlockIndex;
	PFNGLGETUNIFORMINDICESPROC GetUniformIndices;
	PFNGLGETPROGRAMIVPROC GetProgramiv;
	PFNGLGETPROGRAMINFOLOGPROC GetProgramInfoLog;
	PFNGLLINKPROGRAMPROC LinkProgram;
	PFNGLMAPBUFFERRANGEPROC MapBufferRange;
	PFNGLUNMAPBUFFERPROC UnmapBuffer;
	PFNGLTEXIMAGE2DPROC TexImage2D;
	PFNGLTEXPARAMETERIPROC TexParameteri;
	PFNGLCOMPRESSEDTEXIMAGE2DPROC CompressedTexImage2D;
	PFNGLUSEPROGRAMPROC UseProgram;
	PFNGLVIEWPORTPROC Viewport;
	PFNGLVERTEXATTRIBPOINTERPROC VertexAttribPointer;
	PFNGLSHADERSOURCEPROC ShaderSource;
	PFNGLUNIFORM4FPROC Uniform4f;
	PFNGLUNIFORM4FVPROC Uniform4fv;
	PFNGLUNIFORMBLOCKBINDINGPROC UniformBlockBinding;
	PFNGLUNIFORMMATRIX4FVPROC UniformMatrix4fv;
	PFNGLGETACTIVEUNIFORMPROC GetActiveUniform;
	PFNGLTRANSFORMFEEDBACKVARYINGSPROC TransformFeedbackVaryings;
	PFNGLENDTRANSFORMFEEDBACKPROC EndTransformFeedback;
	PFNGLFLUSHPROC Flush;
	PFNGLSTENCILMASKPROC StencilMask;
	PFNGLSTENCILFUNCPROC StencilFunc;
	PFNGLSTENCILOPPROC StencilOp;
#ifndef RENDER_GLES
	PFNGLGETBUFFERSUBDATAPROC GetBufferSubData;
	PFNGLGETCOMPRESSEDTEXIMAGEPROC GetCompressedTexImage;
#endif
};

typedef struct
{
	const char *Name;
	GLuint Type;
} shader_def;

typedef struct shader_program
{
	u32 Id;
	struct shader_program *Next;
}shader_program;

typedef enum 
{
	ShaderProgramType_Error,
	ShaderProgramType_Line,
	ShaderProgramType_Mesh,
	ShaderProgramType_Circle,
	ShaderProgramType_Sprite,

	ShaderProgramType_Max
} shader_program_type;

typedef struct vertex_pc
{
	vec3 Pos;
	vec4 Color;
} vertex_pc;

typedef struct vertex_pcuv
{
	vec3 Pos;
	vec4 Color;
	vec2 UV;
} vertex_pcuv;

typedef struct mesh
{
	u32 ElementsStart;
	u32 ElementsLen;
}mesh;

GENUM()
typedef enum blend_mode
{
	BlendMode_Normal,
	BlendMode_Additive
}blend_mode;

typedef struct sprite
{
	asset *TextureAsset;
	rect UVRect;
	vec4 Color;
} sprite;

typedef struct sprite_sheet
{
	sprite Sprite;
	u32 SpriteW;
	u32 SpriteH;
} sprite_sheet;

GENUM()
typedef enum render_type
{
	RenderType_Normal,
	RenderType_SpriteSheet,
} render_type;

GOBJECT()
typedef struct render_params
{
	render_type Type;
	asset_ref AssetRef;
	union 
	{
		rect UVRect;
		vec2 ColAndRow;
	};
	vec2 Size;
	vec4 Color;
	blend_mode BlendMode;
} render_params;

//Particle system wip -->
typedef struct particle_info 
{
	sprite Sprite; 
	f32 LifeTime; 
	f32 Size;
	f32 Speed;
	vec3 Pos; 
	vec2 Dir; 
	vec4 OrigColor;
	vec4 EndColor;
	blend_mode BlendMode;
	void *UserData;
	int UserTag;
} particle_info;
//<-- wip

struct engine;

typedef struct buffer_object
{
	u32 Vao;
	u32 Vertices;
	u32 VerticesLen;
	u32 Elements;
	u32 ElementsLen;
} buffer_object;

typedef struct sprite_batch
{
	u32 Id;
	u32 TextureId;
	u32 RenderOrder;
	blend_mode BlendMode;
	struct sprite_info *SpriteInfo;
}sprite_batch;

typedef struct render_command_mesh
{
	mesh *Mesh;
	vec3 Pos;
	vec2 Scale;
	vec4 Color;
} render_command_mesh;

typedef struct render_command_line
{
	vec3 From;
	vec3 To;
	vec4 Color;
} render_command_line;

typedef struct render_command_sprite
{
	sprite Sprite;
	vec3 Pos;
	vec2 Size;
	f32 ZRot;
	blend_mode BlendMode;
} render_command_sprite;

typedef enum stencil_command_type
{
	StencilType_Write,
	StencilType_Compare,
	StencilType_End,
} stencil_command_type;

typedef struct render_command_stencil
{
	stencil_command_type Type;
	u8 Flag;
} render_command_stencil;

typedef enum render_command_type
{
	RCT_Mesh,
	RCT_Line,
	RCT_Stencil,
	RCT_Sprite,
} render_command_type;

typedef struct render_command
{
	render_command_type Type;
	union
	{
		render_command_mesh MeshCommand;
		render_command_line LineCommand;
		render_command_stencil StencilCommand;
		render_command_sprite SpriteCommand;
	};
} render_command;

typedef struct render
{
	b32 Initialized;

	f64 CPUFrameTime;

	shader_program *ShaderList;
	struct opengl OpenGL;

	u32 Shaders[ShaderProgramType_Max];
	f32 ProjectionMatrix[16];

	u32 LineVao;
	u32 LineBufferObject;
	//TODO: Make a dynamic array for this vertices buffers
	vertex_pc LineVertices[4096];
	u32 LineIndex;

	buffer_object MeshVbo;
	buffer_object SpriteVbo;

	vec2 DesignSize;
	vec2 ScreenOrigin;
	vec2 DesignScreenOrigin;
	vec4 DefaultColor;

	sprite_batch *SpriteBatches;
	u32 SpriteBatchIdGen;

	u32 TransformFeedbackObject;

	mesh CircleMesh;
	mesh QuadMesh;

	render_command *Commands;
	s32 *StencilEndIndexes;
	s32 CommandEnd;
	s32 CommandToReturn;
} render;

void render_init(struct engine *Engine);
void render_update(struct engine *Engine);
void render_shutdown(struct engine *Engine);

void render_gpu_driver_init(struct engine *Engine);
void render_gpu_driver_shutdown(struct engine *Engine);

void render_set_ortographic_projection(struct engine *Engine, f32 Left, f32 Right, f32 Bottom, f32 Top);
void render_set_clear_color(struct engine *Engine, vec4);

u32 render_create_program(struct engine *Engine, shader_def *ShaderDefs, u32 ShaderDefsLen);
mesh render_create_mesh(struct engine *Engine, vertex_pc *Vertices, u32 VerticesLen, u16 *Elements, u32 ElementsLen);

struct asset;
void render_draw_line(struct engine *Engine, vec3 From, vec3 To);
void render_draw_circle(struct engine *Engine, vec3 Pos, f32 Rad);
void render_draw_mesh(struct engine *Engine, mesh *Mesh, vec3 Pos, vec2 Scale);
void render_draw_quad(struct engine *Engine, vec2 Pos, f32 Scale);
void render_draw_rect(struct engine *Engine, vec3 Pos, vec2 Scale);
void render_draw_sprite(struct engine *Engine, sprite Sprite, vec3 Pos, vec2 Scale, f32 ZRot, blend_mode BlendMode);
void render_draw_sprite_in_sheet(struct engine *Engine, sprite_sheet SpriteSheet, vec2 ColAndRow, vec3 Pos, vec2 Size, f32 Rot, blend_mode BlendMode);
void render_create_texture(struct engine *Engine, struct asset *Asset, u32 PixelSize);

void render_set_default_color(struct engine *Engine, vec4 Color);

typedef struct bmp_font
{
	char *AssetName;
	u16 Rows;
	u16 Cols;
	u16 GlyphW;
	u16 GlyphH;
} bmp_font;

#define render_draw_text_bmf(Engine, Font, FontSize, Text, Pos, Color) render_draw_text_bmf_(Engine, Font, FontSize, Text, string_len(Text), Pos, Color);
void render_draw_text_bmf_(struct engine *Engine, bmp_font *Font, u32 FontSize, const char *Text, u32 TextLen, vec2 Pos, vec4 Color);

u32 render_sprite_batch_add(struct engine *Engine, u32 TextureId, u32 Order, blend_mode BlendMode);

//TODO: Where is this function getting called from, maybe Android?
void render_sprite_batch_reset(struct engine *Engine);

#if DEBUG
void render_reload_asset(struct engine *Engine, struct asset *Asset);
#endif

void render_stencil_write(struct engine *Engine, u8 Value);
void render_stencil_compare(struct engine *Engine, u8 Value);
void render_stencil_end(struct engine *Engine);

#endif
