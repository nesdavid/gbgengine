#ifndef GBG_STRING_H
#define GBG_STRING_H

#include "gbg_memory.h"
#include <stdarg.h>

/**
 * Every char * means utf8.
 *
 * Win platform uses wchar (utf16), the engine will transform between those standards,
 * the platform will receive utf8 and transform it for internal use and return utf8.
 *
 * Currently investigating how to use utf8 in win32, it can be done since 2019 with an xaml configuration: https://learn.microsoft.com/en-us/windows/apps/design/globalizing/use-utf8-code-page
 *
 * The problem seems to be with IShellItem which is only utf16. So, altough everything is changed to use the Ascii functions, where possible, that Shell API don't have an utf8 counterpart so were are still in an hybrid state.
 *
 * Linux should use utf8 as the set locale
 *
 * Android?
 */

u32 string_sid(const char *Name);
u32 string_len(const char *Name);
void string_copy(const char *Source, char *Dest);
void string_strip(char *Str, u32 Len);
char *string_dup(struct engine *Engine, const char *Name);
char *string_new(struct engine *Engine, const char *Fmt, ...);
char *string_new_v(struct engine *Engine, const char *Fmt, va_list vargs);

void string_delete(struct engine *engine, char *Str);

char *string_substr(struct engine *Engine, const char *Source, u32 From, u32 To);

b32 string_compare(const char *Str1, const char *Str2);

b32 string_is_alpha(char C);

char *string_from_int(struct engine *Engine, s32 Number);
char *string_from_float(struct engine *Engine, f32 Number);

s32 string_index_of(const char *Str, char C);
s32 string_reverse_index_of(const char *Str, char C);

s32 string_find_str(const char *Source, const char *Test);

void string_replace(char *Source, char Find, char Replace);

//Helper to write/red strings to/from file
u32 write_string(struct engine *Engine, file_handle File, const char *Str, u32 Len, u32 From);
u32 read_string(struct engine *Engine, file_handle File, u32 ReadFrom, char **OutString);

u32 write_string_json(struct engine *Engine, file_handle File, char *Str, u32 Len, u32 From);

//string key
//Careful with char* S, it won't be duplicated
GOBJECT()
typedef struct skey
{
	const char *S;
	u32 K;
	b32 IsSet;
} skey;

skey SKey(const char *Name);
b32 skey_compare_fast(skey *A, skey *B);

b32 byte_compare(u32 Len, const u8* A, const u8* B);

#endif
