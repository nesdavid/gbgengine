#ifndef _GBG_H_
#define _GBG_H_

#include <gbg_assets.h>
#include <gbg_editor.h>
#include <gbg_engine.h>
#include <gbg_imgui.h>
#include <gbg_input.h>
#include <gbg_math.h>
#include <gbg_memory.h>
#include <gbg_net.h>
#include <gbg_platform.h>
#include <gbg_render.h>
#include <gbg_rtti.h>
#include <gbg_sound.h>
#include <gbg_string.h>
#include <gbg_types.h>
#include <gbg_utils.h>
#include <gbg_world.h>

#endif
