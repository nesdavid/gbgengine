#include "gbg_assets.h"
#include "gbg_engine.h"
#include "gbg_string.h"
#include "gbg_utils.h"
#include "gbg_world.h"

#include <string.h> //for memset

#define CheckCC(Ptr, A, B, C, D) (Ptr[0] == A && Ptr[1] == B && Ptr[2] == C && Ptr[3] == D)

static asset *asset_get_raw(struct engine *Engine, skey Key)
{
	u32 Index = Key.K % static_array_len(Engine->AssetDatabase.Assets);
	asset *Asset = Engine->AssetDatabase.Assets + Index;
	return Asset;
}

static b32 asset_find_in_list(asset **Asset, u32 Sid)
{
	asset *ItAsset = *Asset;
	// Lookup
	while(1)
	{
		//TODO: What happens if the Sid collides? how often may that happen?
		//Should I also check for the pathname to be sure? But I don't what to check it every frame
		//Is it safe to keep the Asset pointer? The asset database is stable, but what can happen with an invalid asset?
		if (ItAsset->PathName.K == Sid)
		{
			*Asset = ItAsset;
			return 1;
		}

		if (ItAsset->Next == 0)
			break;

		ItAsset = ItAsset->Next;
	}

	*Asset = ItAsset;
	return 0;
}

asset *asset_get(struct engine *Engine, skey Name)
{
	asset *Asset = asset_get_raw(Engine, Name);

	if (Asset->PathName.K == 0)
	{
		return 0;
	}

	asset_find_in_list(&Asset, Name.K);
	return Asset;
}

static asset *asset_get_or_create(engine *Engine, skey Name)
{
	asset *Asset = asset_get_raw(Engine, Name);

	//1. Asset is empty, it can be used
	if (Asset->PathName.K == 0)
	{
		Asset->PathName = Name;
		return Asset;
	}

	if (asset_find_in_list(&Asset, Name.K))
		return Asset;

	//2. Didn't find it, create a new one
	asset *NewAsset = memory_alloc_type(&Engine->Memory, asset);
	memset(NewAsset, 0, sizeof(asset));
	NewAsset->PathName = Name;
	Asset->Next = NewAsset;

	return NewAsset;
}

static char *asset_get_full_path(engine *Engine, const char *Name)
{
	char *Ret = 0;
#if PLAT_ANDROID
	Ret = string_dup(Engine, Name);
#else
	Ret = string_new(Engine, "%s%s", Engine->AssetDatabase.RootPath, Name);
#endif

	return Ret;
}

u8 *asset_load_file(struct engine *Engine, const char *Name, u32 *Size)
{
	char *FullPath = asset_get_full_path(Engine, Name);
	u8 *Ret = Engine->Platform->FileReadFully(FullPath, Size);
	memory_free(&Engine->Memory, FullPath);
	
	return Ret;
}

static u8 *prepare_asset_and_get_file(struct engine *Engine, const char *Name, const char *Extension, asset **OutAsset)
{
	skey KeyName = SKey(Name);
	*OutAsset = asset_get_or_create(Engine, KeyName); 

	char *FullName = string_new(Engine, "%s%s", Name, Extension);
	u32 Size;
	u8 *File = asset_load_file(Engine, FullName, &Size);

	string_delete(Engine, FullName);
	return File;
}

b32 asset_initialize_sound(engine *Engine, asset *Asset, u8 *Data)
{
	if (CheckCC(Data, 'R', 'I', 'F', 'F'))
	{
		u32 *DataSize = (u32*)(Data + 4);
		u8 *FileType = Data + 8;
		if (CheckCC(FileType, 'W', 'A', 'V', 'E'))
		{
			FileType += 4;
			if (CheckCC(FileType, 'J', 'U', 'N', 'K'))
			{
				u32 *JunkSize = (u32*)(Data + 16);
				//Skip Data
				Data += *JunkSize + 8;
				FileType += *JunkSize + 8;
			}

			if (CheckCC(FileType, 'f', 'm', 't', ' '))
			{
//				u32 *FormatLength = (u32*)(Data + 16);
//				u16 *Format = (u16*)(Data + 20);
				u16 *Channels = (u16*)(Data + 22);
				if (*Channels != Engine->SoundSystem->Channels)
				{
					Engine->Platform->Log("Sound file has incorrect channels info %d\n", *Channels);
					return 0;
				}
				u32 *SampleRate = (u32*)(Data + 24);
				if (*SampleRate != Engine->SoundSystem->SampleRate)
				{
					Engine->Platform->Log("Sound file has incorrect samplerate info %d\n", *SampleRate);
					return 0;
				}

		//		u32 *AvgSampleRate = (u32*)(Data + 28);
		//		u16 *BlockAlign = (u16*)(Data + 32);
		//		u16 *BitsPerSample = (u16*)(Data + 34);
				u8 *DataChunk = Data + 36;
				while (!CheckCC(DataChunk, 'd', 'a', 't', 'a'))
				{
					DataSize = (u32*)(DataChunk+4);
					DataChunk += *DataSize + 8; //+8: 4 of the header + 4 of the chunksize
				}

				Asset->SoundAsset.DataSize = *(u32*)(DataChunk+4);
				Asset->SoundAsset.Data = DataChunk+8;
				Asset->SoundAsset.Gain = 1;
			}
			else
			{
				return 0;
			}
		}
		else
		{
			return 0;
		}
	}
	else
	{
		return 0;
	}

	return 1;
}

static void asset_clean_asset(engine *Engine, asset *Asset)
{
	Asset->PathName.K = 0;
	memory_free(&Engine->Memory, (void *)Asset->PathName.S);
}

void asset_load_sound(struct engine *Engine, const char *Name)
{
	asset *Asset = 0;
	u8 *File = prepare_asset_and_get_file(Engine, Name, ".wav", &Asset);
	if (File == 0)
		return;

	if (asset_initialize_sound(Engine, Asset, File) == 0)
	{
		Engine->Platform->Log("Error loading sound file %s\n", Name);
		memory_free(&Engine->Memory, File);
		asset_clean_asset(Engine, Asset);
	}
}

void asset_load_sound_streamed(struct engine *Engine, const char *Name)
{
	skey KeyName = SKey(Name);
	asset *Asset = asset_get_or_create(Engine, KeyName);

	char *FullPath = asset_get_full_path(Engine, Name);
	file_handle FileH = Engine->Platform->FileOpen(FullPath);
	u8 Buffer[256];
	u32 Read = Engine->Platform->FileRead(FileH, Buffer, sizeof(Buffer), 0);
	if (Read != sizeof(Buffer))
	{
		Engine->Platform->FileClose(FileH);
		asset_clean_asset(Engine, Asset);
		return;
	}

	if (asset_initialize_sound(Engine, Asset, Buffer) == 0)
	{
		Engine->Platform->Log("Error loading sound file %s\n", Name);
		Engine->Platform->FileClose(FileH);
		asset_clean_asset(Engine, Asset);
		return;
	}

	u32 BufferSize = 60000;
	Asset->SoundAsset.StreamBuffer = memory_alloc(&Engine->Memory, BufferSize);
	u32 Offset = (u32)(Asset->SoundAsset.Data - Buffer);
	Asset->SoundAsset.StreamBeginFileOffset = Offset;
	Asset->SoundAsset.StreamFileOffset = Offset;
	Asset->SoundAsset.DataSize = BufferSize/2;
	Asset->SoundAsset.StreamFromDisk = 1;
	Asset->SoundAsset.StreamFile = FileH;
	Asset->SoundAsset.StreamFileOffset += Engine->Platform->FileRead(FileH, Asset->SoundAsset.StreamBuffer, BufferSize/2, Offset);
	Asset->SoundAsset.Data = Asset->SoundAsset.StreamBuffer;
	Asset->SoundAsset.StreamRequest = 1;
}

void asset_load_texture(struct engine *Engine, const char *Name)
{
#ifdef PLAT_ANDROID
	const char *Extension = ".pkm";
#else
	const char *Extension = ".dds";
#endif

	asset *Asset = 0;
	u8 *File = prepare_asset_and_get_file(Engine, Name, Extension, &Asset);

	u32 PixelSize = 16;
#ifdef PLAT_ANDROID
	if (CheckCC(File, 'P', 'K', 'M', ' '))
	{
	    u16 Type = *(u16*)(File+6);
		if (Type == 256) //ETC_RGB8 No alpha channel
		{
			Asset->TextureAsset.Format = GL_COMPRESSED_RGB8_ETC2;
			PixelSize = 8;
		}
		else //768
		{
			Asset->TextureAsset.Format = GL_COMPRESSED_RGBA8_ETC2_EAC;
		}

		Asset->TextureAsset.Height = *(u16*)(File + 9);
		Asset->TextureAsset.Width = *(u16*)(File + 11);
		Asset->TextureAsset.Data = File + 16;
	}
#else

	//DDS Format
	if (CheckCC(File, 'D', 'D', 'S', ' '))
	{
		Asset->TextureAsset.Height = *(u32*)(File + 12);
		Asset->TextureAsset.Width = *(u32*)(File + 16);
		u8 *FourCC = (u8*)(File + 84);
		if (CheckCC(FourCC, 'D', 'X', 'T', '1'))
		{
			Asset->TextureAsset.Format = GL_COMPRESSED_RGB_S3TC_DXT1_EXT;
			PixelSize = 8;
		}
		else if (CheckCC(FourCC, 'D', 'X', 'T', '5'))
		{
			Asset->TextureAsset.Format = GL_COMPRESSED_RGBA_S3TC_DXT5_EXT;
		}
		Asset->TextureAsset.Data = File + 128;
	}
#endif

	Asset->Type = AssetType_Texture;
	render_create_texture(Engine, Asset, PixelSize);
	//Done with the file I can delete it from memory
	memory_free(&Engine->Memory, File);

#if DEBUG
	asset_timestamp AT;
	char *FullPath = string_new(Engine, "%s/%s", Engine->AssetDatabase.RootPath, Asset->PathName);
	AT.Time = Engine->Platform->GetFileModifiedDate(FullPath);
	memory_free(&Engine->Memory, FullPath);
	AT.Asset = Asset;
	dynamic_array_add(&Engine->AssetDatabase.Timestamps, AT);
#endif
}

static void asset_read_record(engine *Engine, char *FileName, rtti_record **Record, void **Obj)
{
	file_handle Handle = Engine->Platform->FileOpen(FileName);
	//TODO: add file handle check for validity
	u8 Version;
	u8 GEA[3];

	u32 ReadFrom = Engine->Platform->FileRead(Handle, &Version, 1, 0);
	if (Version == 1)
	{
		ReadFrom += Engine->Platform->FileRead(Handle, GEA, 3, ReadFrom);
		if (GEA[0] == 'G' && GEA[1] == 'E' && GEA[2] == 'A')
		{
			char *RecordName = 0;
			ReadFrom += read_string(Engine, Handle, ReadFrom, &RecordName);
			*Record = rtti_record_get_by_name(Engine, RecordName);
			string_delete(Engine, RecordName);
			u32 RecSize = rtti_record_get_size(Engine, *Record);
			*Obj = memory_alloc(&Engine->Memory, RecSize);
			u32 Read = read_record(Engine, Handle, ReadFrom, *Record, (u8*)*Obj);
			if (Read == 0)
			{
				Engine->Platform->Log("Error loading file %s. Unable to load %s\n", FileName, RecordName);
				memory_free(&Engine->Memory, *Obj);
			}
		}
	}

	Engine->Platform->FileClose(Handle);
}	


void asset_init(engine *Engine)
{
	asset_database *AssetDb = &Engine->AssetDatabase;
#if DEBUG
	AssetDb->Timestamps = dynamic_array_create(Engine, asset_timestamp);
#endif

	AssetDb->SavedImprints = dynamic_array_create(Engine, imprint);

	//TODO: Parse all assets, not only imprints, now we have textures, and sounds will come soon
	char *TemplateDir = string_new(Engine, "%simprints/", AssetDb->RootPath);
	Engine->Platform->ListFileStart(TemplateDir, "gea");
	for (;;) 
	{
		char *FileName = Engine->Platform->ListFileNext();
		if (FileName == 0)
			break;

		char *FullPath = string_new(Engine, "%simprints/%s", AssetDb->RootPath, FileName);

		rtti_record *Record = 0;
		entity *Ent = 0;
		asset_read_record(Engine, FullPath, &Record, (void*)&Ent);
		char *ImprintName = string_substr(Engine, FileName, 0, string_index_of(FileName, '.'));
		skey SName = SKey(ImprintName);
		Ent->ImprintName = SName;
		imprint EdObj = {SName, Ent};
		dynamic_array_add(&AssetDb->SavedImprints, EdObj);
		string_delete(Engine, FullPath);
	} 
	string_delete(Engine, TemplateDir);
}

void asset_reload(struct engine *Engine)
{
#if DEBUG
	for (u32 i=0; i<dynamic_array_len(Engine->AssetDatabase.Timestamps); ++i)
	{
		asset_timestamp *AT = Engine->AssetDatabase.Timestamps + i;
		char *FullPath = string_new(Engine, "%s/%s", Engine->AssetDatabase.RootPath, AT->Asset->PathName);
		if (AT->Time < Engine->Platform->GetFileModifiedDate(FullPath))
		{
			if (AT->Asset->Type == AssetType_Texture)
			{
				render_reload_asset(Engine, AT->Asset);
				dynamic_array_remove_at(Engine->AssetDatabase.Timestamps, i--);
			}
		}
		memory_free(&Engine->Memory, FullPath);
	}
#endif
}

void asset_update(struct engine *Engine)
{
	asset_reload(Engine);
}

void asset_get_all_of_type(struct engine *Engine, asset_type Type, asset_query_callback Callback)
{
	for (u32 i=0; i<static_array_len(Engine->AssetDatabase.Assets); ++i)
	{
		asset *Asset = Engine->AssetDatabase.Assets + i;
		if (Asset->PathName.K == 0)
			continue;

		if (Asset->Type == Type)
			Callback(Engine, Asset);
	}
}

imprint *imprint_get(struct engine *Engine, skey Name)
{
	for (s32 i=0; i<dynamic_array_len(Engine->AssetDatabase.SavedImprints); ++i)
	{
		imprint *Imprint = Engine->AssetDatabase.SavedImprints + i;
		if (skey_compare_fast(&Imprint->Name, &Name))
		{
			return Imprint;
		}
	}

	return 0;
}
