
#ifndef NE_UTILS_H
#define NE_UTILS_H

#define static_array_len(Array) (sizeof(Array)/sizeof(Array[0]))

#define dynamic_array_create(Engine, Type) dynamic_array_create_(Engine, sizeof(Type))
void *dynamic_array_create_(struct engine *Engine, u32 ElementSize);

//Invalidates the array.
//Returns the memory used by this array to the empty pool
void dynamic_array_destroy(void *Array);

//dynamic_array_add may require to modify the array pointer if memory needs to be moved to another block
//That's why it needs a pointer to a pointer
//2nd param, sadly we cant use the return value of a function cause it needs an l-value to pass the reference :(
//TODO: It's the second time I've been hit by passing a different size value for the 3rd hidden param. I'm very inclined to use C++ only for define a typed array :(
#define dynamic_array_add(Array, Element) dynamic_array_add_((void**)(Array), (void*)&(Element), sizeof(Element))
void dynamic_array_add_(void **Array, void *Data, u32 ElementSize);

#define dynamic_array_add_at(Array, Element, Index) dynamic_array_add_at_((void**)(Array), (void*)&(Element), sizeof(Element), Index)
void dynamic_array_add_at_(void **Array, void *Data, u32 ElementSize, u32 Index);

//Returns the size of the array (not the Capacity)
u32 dynamic_array_len(void *Array);

//Remove element and swap the last element to the empty space
#define dynamic_array_remove_at(Array, Index) dynamic_array_remove_at_(Array, Index, sizeof(*Array))
void dynamic_array_remove_at_(void *Array, u32 Index, u32 ElementSize);

//Remove the element and  move all the elements one place less, so the elements will preserve the place they have
#define dynamic_array_remove_at_preserve(Array, Index) dynamic_array_remove_at_preserve_(Array, Index, sizeof(*Array))
void dynamic_array_remove_at_preserve_(void *Array, u32 Index, u32 ElementSize);

//Clears the array, but keeps the Capacity
void dynamic_array_clear(void *Array);

//Enlarges internally the array capacity, if the array already has this capacity nothing happens.
//It won't change the size of it. Use dynamic_array_grow for that.
#define dynamic_array_reserve(Array, Capacity) dynamic_array_reserve_(Array, Capacity, sizeof(**Array))
void dynamic_array_reserve_(void **Array, u32 Capacity, u32 ElementSize);

//Grows the array so it can hold the new capacity, if the array already has this capacity nothing happens.
//This will change the size.
#define dynamic_array_grow(Array, NewSize) dynamic_array_grow_((void**)Array, NewSize, sizeof(**Array))
void dynamic_array_grow_(void **Array, u32 Capacity, u32 ElementSize);

void mem_copy(void *Source, void *Dest, u32 Len);

#endif
