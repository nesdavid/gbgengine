#ifndef GBG_RTTI_PARSER_H
#define GBG_RTTI_PARSER_H

typedef struct rtti_enum_field
{
	char *Name;
	u8 Value;
} rtti_enum_field;

typedef struct rtti_enum
{
	char *Name;
	rtti_enum_field *Fields;
} rtti_enum;

s32 rtti_enum_get_value(struct engine *Engine, const char *EnumTypeName, const char *EnumValue);

typedef enum rtti_field_type
{
	RFT_Unknown, 
	RFT_Boolean,
	RFT_Integer,
	RFT_Float,
	RFT_Char,
	RFT_Enum,
	RFT_Vec,
	RFT_String,
	RFT_Struct,
	RFT_Union,

	RFT_Max,
} rtti_field_type;

typedef struct rtti_field
{
	char *Name;
	char *TypeAsString;
	rtti_field_type Type;
	u32 Count;
	b32 IsPointer;
	u32 Offset;
	u32 Size;
} rtti_field;

typedef struct rtti_record
{
	char *Name;
	rtti_field *Fields;
	b32 Editor;
	b32 IsUnion;
} rtti_record;

rtti_field *rtti_union_get_biggest_field(rtti_record *UnionRecord);
rtti_field *rtti_record_get_field_by_name(rtti_record *Record, const char *FieldName);

//All imprint are coming from an entity
//Here is the relationship from entity to file
//TODO: This relationship is now inside the entity, maybe this struct should go away?
typedef struct imprint
{
	skey Name;
	struct entity *Entity;
}imprint;

GOBJECT()
typedef struct named_pointer
{
	char *RecordName;
	void *Ptr;
} named_pointer;

u32 rtti_record_get_size(struct engine *Engine, rtti_record *Record);

typedef struct rtti
{
	rtti_record *Records;	
	rtti_enum *Enums;	

	//This rtti is used all the time, let's cache it
	rtti_record *CachedEntityRecord;
} rtti;

void rtti_init(struct engine *Engine);

rtti_record *rtti_record_get_by_name(struct engine *Engine, const char *RecordName);
#define rtti_record_get_by_type(Engine, RecordType) rtti_record_get_by_name(Engine, #RecordType)

void *rtti_duplicate(struct engine *Engine, rtti_record *Record, void *DefaultData);
void rtti_record_copy(struct engine *Engine, rtti_record *Record, void *Source, void *Dest);
u32 read_record(struct engine *Engine, file_handle File, u32 ReadFrom, rtti_record *Record, u8 *Obj);
u32 read_record_header(struct engine *Engine, file_handle File, u32 ReadFrom, rtti_record **OutRecord);

#endif
