#include "gbg_string.h"
#include "gbg_engine.h"

#include <stdio.h>
#include <stdarg.h>

u32 string_sid(const char *Name)
{
	u32 Count = 1;
	u32 Sid = 0;
	if (Name)
	{
		for (const char *c=Name; *c!=0; ++c)
		{
			Sid += *c * Count++;
		}
	}

	return Sid;
}

u32 string_len(const char *Name)
{
	u32 Count = 0;
	if (Name)
	{
		for (const char *c = Name; *c!=0; ++c)
		{
			Count++;
		}
	}

	return Count;
}

b32 string_is_alpha(char C)
{
	return !(C < 33 || C > 126);
}

void string_strip(char *Str, u32 Len)
{
	if (Len == 0)
		return;

	u32 Start = 0;
	u32 End = Len;

	for (u32 i=0; i<Len; ++i)
	{
		if (!string_is_alpha(Str[i]))
		{
			++Start;
		}
		else
		{
			break;
		}
	}

	for (u32 i = End - 1; i >= Start; --i)
	{
		if (!string_is_alpha(Str[i]))
		{
			--End;
		}
		else
		{
			break;
		}
	}

	if (Start == End)
	{
		//All string is invalid
		Str[0] = 0;
	}

	if (End - Start == Len)
	{
		Str[End] = 0;
		return;
	}

	u32 StrIndex = 0;
	for (u32 i=Start; i<End; ++i)
	{
		Str[StrIndex++] = Str[i];
	}

	Str[StrIndex] = 0;
}

char *string_dup(engine *Engine, const char *Name)
{
	u32 Len = string_len(Name)+1;
	u8 *NewString = memory_alloc(&Engine->Memory, Len);
	for (u32 i = 0; i<Len; ++i)
	{
		NewString[i] = Name[i];
	}
	NewString[Len] = 0;

	return (char*)NewString;
}

void string_copy(const char *Source, char *Dest)
{
	u32 Size = string_len(Source);
	for (u32 i=0; i<Size ; ++i)
	{
		Dest[i] = Source[i];
	}

	Dest[Size] = 0;
}

char *string_new_v(engine *Engine, const char *Fmt, va_list Vargs)
{
	va_list Args;
	va_copy(Args, Vargs);

	char *Ret = 0;
	s32 Size = vsnprintf(0, 0, Fmt, Args);
	if (Size > 0)
	{
		Ret = (char*)memory_alloc(&Engine->Memory, Size+1);
		s32 SizeCheck = vsnprintf(Ret, Size+1, Fmt, Vargs);
		if (SizeCheck != Size)
		{
			memory_free(&Engine->Memory, Ret);
			return 0;
		}
	}

	return Ret;
}

void string_delete(engine *Engine, char *Str)
{
	memory_free(&Engine->Memory, Str);
}

char *string_new(engine *Engine, const char *Fmt, ...)
{
	va_list Args;
	va_start(Args, Fmt);

	return string_new_v(Engine, Fmt, Args);
}

b32 byte_compare(u32 Len, const u8* A, const u8* B)
{
	for (u32 i=0; i<Len; ++i)
	{
		if (A[i] != B[i])
			return 0;
	}

	return 1;
}

b32 string_compare(const char *Str1, const char *Str2)
{
	if (Str1 == 0 || Str2 == 0)
	{
		return 0;
	}

	u32 Len1 = string_len(Str1);
	if (Len1 == string_len(Str2))
	{
		return byte_compare(Len1, (const u8*)Str1, (const u8*)Str2);
	}

	return 0;
}

void string_replace(char *Source, char Find, char Replace)
{
	u32 Len = string_len(Source);
	for (u32 i=0; i<Len; ++i)
	{
		if (Source[i] == Find)
			Source[i] = Replace;
	}
}

char *string_substr(engine *Engine, const char *Source, u32 From, u32 To)
{
	if (To <= From)
		return 0;

	u32 Len = string_len(Source);
	if (From >= Len)
		return 0;

	if (To >= Len)
		To = Len;

	u32 SubStrLen = To - From;
	char *Ret = memory_alloc(&Engine->Memory, SubStrLen);

	u32 RetIndex = 0;
	for (u32 i=From; i<To; ++i)
	{
		Ret[RetIndex++] = Source[i];
	}
	Ret[RetIndex] = 0;

	return Ret;
}

s32 string_index_of(const char *Str, char C)
{
	u32 Len = string_len(Str);
	for (u32 i=0; i<Len; ++i)
	{
		if (Str[i] == C)
			return i;
	}

	return -1;
}

s32 string_find_str(const char *Source, const char *Test)
{
	u32 SourceLen = string_len(Source);
	u32 TestLen = string_len(Test);
	s32 Found = -1;

	for (u32 LookUpIndex = 0; LookUpIndex<SourceLen; ++LookUpIndex)
	{
		u32 SourceIndex = LookUpIndex;
		Found = LookUpIndex;
		for (u32 TestIndex = 0; TestIndex<TestLen; ++TestIndex)
		{
			if (Source[SourceIndex] == Test[TestIndex])
			{
				++SourceIndex;
			}
			else
			{
				Found = -1;
				break;
			}
		}

		if (Found > 0)
			break;
	}

	return Found;
}

s32 string_reverse_index_of(const char *Str, char C)
{
	s32 Len = string_len(Str);
	for (s32 i=Len-1; i>=0; --i)
	{
		if (Str[i] == C)
			return i;
	}

	return -1;
}

u32 read_string(engine *Engine, file_handle File, u32 ReadFrom, char **OutString)
{
	u32 InitialReadFrom = ReadFrom;
	u32 StringSize;

	ReadFrom += Engine->Platform->FileRead(File, (u8*)&StringSize, sizeof(StringSize), ReadFrom);
	char *String = memory_alloc(&Engine->Memory, StringSize + 1);
	ReadFrom += Engine->Platform->FileRead(File, (u8*)String, StringSize, ReadFrom);
	String[StringSize] = 0;
	*OutString = String;

	return ReadFrom - InitialReadFrom;
}

u32 write_string_json(engine *Engine, file_handle File, char *Str, u32 Len, u32 From)
{
	char *JsonStr = string_new(Engine, "\"%s\"", Str);
	u32 Written = Engine->Platform->FileWrite(File, JsonStr, Len+2, From);

	string_delete(Engine, JsonStr);
	return Written;
}

u32 write_string(engine *Engine, file_handle File, const char *Str, u32 Len, u32 From)
{
	u32 Written = Engine->Platform->FileWrite(File, (u8*)&Len, sizeof(Len), From);
	Written += Engine->Platform->FileWrite(File, (u8*)Str, Len, From + Written);

	return Written;
}

//
// String Key
//
skey SKey(const char *Name)
{
	skey SK = {Name, string_sid(Name), 1};
	return SK;
}

b32 skey_compare_fast(skey *A, skey *B)
{
	return A->K == B->K;
}

#define _STRING_FROM_FMT(Engine, Fmt, Number) \
	s32 Count = snprintf(0, 0, Fmt, Number); \
	if (Count > 0) \
	{ \
		char *Ret = memory_alloc(&Engine->Memory, Count + 1); \
		snprintf(Ret, Count+1, Fmt, Number); \
		return Ret; \
	} \
	return 0;\

char *string_from_int(engine *Engine, s32 Number)
{
	_STRING_FROM_FMT(Engine, "%d", Number);
}

char *string_from_float(engine *Engine, f32 Number)
{
	_STRING_FROM_FMT(Engine, "%f", Number);
}
