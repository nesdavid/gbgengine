#version 300 es

in lowp vec4 theColor;
out lowp vec4 outputColor;

void main()
{
   outputColor = theColor;
}
