#version 330

layout(location = 0) in vec3 position;
layout(location = 1) in vec4 color;
layout(location = 2) in vec2 UV;
uniform mat4 ProjectionMatrix;

out vec2 OutUV;
out vec4 OutColor;

void main()
{
    gl_Position = ProjectionMatrix * vec4(position.xyz, 1);
	OutColor = color;
	OutUV = UV;
}
