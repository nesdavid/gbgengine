
#define WIN32_LEAN_AND_MEAN

#ifndef UNICODE
#define UNICODE
#endif

#include <windows.h>
#include <windowsx.h>
#include <winnt.h>
#include <dwmapi.h>
#include <initguid.h>
#include <usbiodef.h>
#include <dbt.h>
#include <xinput.h>
#include <combaseapi.h>
#include <xaudio2.h>
#define DIRECTINPUT_VERSION 0x0800
#include <dinput.h>
#define COBJMACROS 
#include <Shlobj.h>
#include <Shobjidl.h>
#undef COBJMACROS 
#include <shellapi.h>
#include <Knownfolders.h>
#include <winsock2.h>
#include <iphlpapi.h>
#include <ipmib.h>
#include <ws2tcpip.h>
#include <wchar.h>
#include <stdio.h>
#include <string.h>

#include "gbg_types.h"
#include "gbg_engine.h"
#include "gbg_platform.h"
#include "gbg_render.h"
#include "gbg_input.h"
#include "gbg_memory.h"

#include "../third_party/glcorearb.h"
#include "../third_party/glext.h"
#include "../third_party/wglext.h"

#include "gbg_rtti.c"
#include "gbg_string.c"
#include "gbg_engine.c"
#include "gbg_assets.c"
#include "gbg_render.c"
#include "gbg_input.c"
#include "gbg_memory.c"
#include "gbg_math.c"
#include "gbg_sound.c"
#include "gbg_utils.c"
#include "gbg_imgui.c"
#include "gbg_net.c"
#include "gbg_world.c"
#include "gbg_editor.c"

LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

HGLRC OpenGLRenderingContext;
HDC WindowHandleToDeviceContext;
HWND WindowHandle;
static input Input = {0};

static int WindowHeight;
static int WindowWidth;

static engine Engine = {0};

typedef void (*game_update_func)(engine*, f32);
game_update_func game_update = 0;

//XINPUT functions
typedef DWORD (*XInputGetState_Func)(DWORD dwUserIndex, XINPUT_STATE *pState);
XInputGetState_Func XInput_GetState = 0;

FILETIME DllModifiedTime;
#define GAME_DLL L"game.dll"
#define GAME_TEMP_DLL L"game_temp.dll"

HMODULE hGameDll = 0;

static void _GetFileTime(const WCHAR *path, LPFILETIME date) {
	HANDLE file = CreateFile(path, GENERIC_READ, FILE_SHARE_READ, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);
	GetFileTime(file, 0, 0, date);
	CloseHandle(file);
}

static void LoadGameFunc()
{
#ifdef RELEASE
	hGameDll = LoadLibrary(GAME_DLL);
	game_update = (game_update_func)GetProcAddress(hGameDll, "game_update");
#else
	FreeLibrary(hGameDll);
	CopyFile(GAME_DLL, GAME_TEMP_DLL, 0);
	hGameDll = LoadLibrary(GAME_TEMP_DLL);
	game_update = (game_update_func)GetProcAddress(hGameDll, "game_update");
	_GetFileTime(GAME_DLL, &DllModifiedTime);
#endif
}

static void DllSwap()
{
#ifndef RELEASE
	FILETIME CurModifiedTime;
	_GetFileTime(GAME_DLL, &CurModifiedTime);
	HANDLE lockFile = CreateFile(L"lock", GENERIC_READ, FILE_SHARE_READ, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);
	if ((CurModifiedTime.dwLowDateTime > DllModifiedTime.dwLowDateTime || CurModifiedTime.dwHighDateTime > DllModifiedTime.dwHighDateTime) && lockFile == INVALID_HANDLE_VALUE) {
		LoadGameFunc();
		Engine.Reloaded = 1;
	}
	CloseHandle(lockFile);
#endif
}

static void platform_log2(const char *Fmt, ...)
{
	va_list args;
	va_start(args, Fmt);

	char S[512];
	vsnprintf(S, 512, Fmt, args);
	WCHAR WS[512];
	MultiByteToWideChar(CP_UTF8, 0, S, 512, WS, 512);
	OutputDebugString(WS);
}

static WCHAR *platform_string_conversion_ut8_to_wchar(const char *Utf8String)
{
	int Size = MultiByteToWideChar(CP_UTF8, 0, Utf8String, -1, 0, 0);
	
	WCHAR *Ret = memory_alloc(&Engine.Memory, sizeof(WCHAR) * Size);

	MultiByteToWideChar(CP_UTF8, 0, Utf8String, -1, Ret, Size);

	return Ret;
}

static void platform_file_copy(const char *Source, const char *Dest)
{
	WCHAR *WSource = platform_string_conversion_ut8_to_wchar(Source);
	WCHAR *WDest = platform_string_conversion_ut8_to_wchar(Dest);
	CopyFile(WSource, WDest, 0);
	memory_free(&Engine.Memory, WSource);
	memory_free(&Engine.Memory, WDest);
}

static void platform_log(const char *Fmt, ...)
{
	va_list args;
	va_start(args, Fmt);

	char *Out = string_new_v(&Engine, Fmt, args);
	WCHAR *WideOut = platform_string_conversion_ut8_to_wchar(Out);
	OutputDebugString(WideOut);

	memory_free(&Engine.Memory, WideOut);
	memory_free(&Engine.Memory, Out);
}

static void platform_keyboard_show()
{
}

static void platform_keyboard_hide()
{
}

static b32 create_socket_and_bind(const char *Host, const char *Port)
{
	WSADATA WsaData;
	int Res = WSAStartup(MAKEWORD(2, 2), &WsaData);
	if (Res != 0)
	{
		platform_log("Error starting WinSock");
		WSACleanup();
		return 0;
	}

	SOCKET Sock = socket(AF_INET, SOCK_DGRAM, 0);
	if (Sock < 0)
	{
		platform_log("Error creating socket %d\n", Sock);
		WSACleanup();
		return 0;
	}

	struct addrinfo *AddrInfo;
	struct addrinfo Hints = {0};
	Hints.ai_family = AF_INET;
	Hints.ai_socktype = SOCK_DGRAM;

	int Err = getaddrinfo(Host, Port, &Hints, &AddrInfo);
	if (Err != 0)
	{
		printf("error on getaddrinfo %d\n", Err);
		closesocket(Sock);
		WSACleanup();
		return 0;
	}

	Engine.Net->Socket.Descriptor = (s32)Sock;

	char LHost[NI_MAXHOST], Service[NI_MAXSERV];
	getnameinfo(AddrInfo->ai_addr, (socklen_t)AddrInfo->ai_addrlen, LHost, NI_MAXHOST, Service, NI_MAXSERV, NI_NUMERICSERV);

    Err = bind(Sock, AddrInfo->ai_addr, (int)AddrInfo->ai_addrlen);

	freeaddrinfo(AddrInfo);
	return 1;
}

static b32 platform_server_start(const char *Host, const char *Port)
{
	return create_socket_and_bind(Host, Port);
}

static void platform_server_stop()
{
	int fd = Engine.Net->Socket.Descriptor;
//	shutdown(fd, SD_BOTH);
	closesocket(fd);
	Engine.Net->Socket.Descriptor = 0;
	WSACleanup();
}

struct thread_pool_info
{
	void *(*Func)(void *Param);
	LPVOID Param;
	HANDLE Id;
};

struct thread_pool_info ThreadPool[16] = {0};

static DWORD WINAPI thread_wrapper_func(_In_ LPVOID lpParameter)
{
	struct thread_pool_info *ThreadInfo = (struct thread_pool_info*)lpParameter;
	ThreadInfo->Func(ThreadInfo->Param);
	return 0;
}

static void platform_create_thread(void *(*Func)(void *Param), void *Param)
{
	struct thread_pool_info *Info = 0;

	for (u32 i=0; i<static_array_len(ThreadPool); i++)
	{
		Info = ThreadPool + i;
		if (Info->Id == 0)
			break;
	}

	Info->Param = Param;
	Info->Func = Func;
	Info->Id = CreateThread(0, 0, thread_wrapper_func, Info, 0, 0);
}

static void platform_exit_thread()
{
	ExitThread(0);
}

static void platform_client_exit()
{
	memory_free(&Engine.Memory, Engine.Net->Host.Address);	
	memory_free(&Engine.Memory, Engine.Net->Host.Port);	
	//shutdown(Engine.Net->Socket.Descriptor, SD_RECEIVE);
	closesocket(Engine.Net->Socket.Descriptor);
	Engine.Net->Socket.Descriptor = 0;
	WSACleanup();
}

static void platform_get_ipv4_address(char *Buffer, u32 BufferLen)
{
	MIB_IPADDRTABLE IpAddrTable[4];
	ULONG Size = sizeof(IpAddrTable);

	DWORD Ret = GetIpAddrTable(
			IpAddrTable,
			&Size,
			0);

	struct in_addr InAddr;
	InAddr.S_un.S_addr = IpAddrTable->table[IpAddrTable->dwNumEntries-1].dwAddr;
	const char *Addr = inet_ntop(AF_INET, &InAddr, Buffer, BufferLen);
	if (Addr == 0)
		Buffer = 0;
}

static b32 platform_client_join(const char *Host, const char *Port)
{
	//TODO: Check all paths will free this memory up
	Engine.Net->Host.Address = string_dup(&Engine, Host);
	Engine.Net->Host.Port = string_dup(&Engine, Port);

	char LocalHostIp[64];
	platform_get_ipv4_address(LocalHostIp, static_array_len(LocalHostIp));
	b32 Ret = create_socket_and_bind(LocalHostIp, Port);

	return Ret;
}

static s32 platform_socket_write(file_handle Handle, u8 *Data, u32 DataLen, const char *Host, const char *Port)
{
	struct addrinfo *AddrInfo;
	struct addrinfo Hints = {0};
	Hints.ai_family = AF_INET;
	Hints.ai_socktype = SOCK_DGRAM;

	int Err = getaddrinfo(Host, Port, &Hints, &AddrInfo);
	if (Err != 0)
	{
		printf("error on getaddrinfo %d. Can't send message\n", Err);
		return 0;
	}

	int Bytes = sendto((SOCKET)Handle.Descriptor, Data, DataLen, 0, AddrInfo->ai_addr, (int)AddrInfo->ai_addrlen);
	freeaddrinfo(AddrInfo);

	return Bytes;
}

static s32 platform_socket_read(file_handle Handle, u8 *Data, u32 DataLen, char *OutHost, char *OutPort)
{
	struct sockaddr SockAddr;
	int SockAddrLen = sizeof(SockAddr);	

	int Bytes = recvfrom(Handle.Descriptor, Data, DataLen, 0, &SockAddr, &SockAddrLen);

	if (OutHost != 0 && OutPort != 0)
	{
		getnameinfo(&SockAddr, SockAddrLen, OutHost, NI_MAXHOST, OutPort, NI_MAXSERV, NI_NUMERICSERV | NI_NUMERICHOST);
	}

	if (Bytes < 0)
	{
		//shutdown(Handle.Descriptor, SD_RECEIVE);
		closesocket(Handle.Descriptor);
	}

	return Bytes;
}

static u16 platform_net_to_host_16(u16 NetShort)
{
	return ntohs(NetShort);
}

static u32 platform_net_to_host_32(u32 NetInt)
{
	return ntohl(NetInt);
}

static u16 platform_host_to_net_16(u16 HostShort)
{
	return htons(HostShort);
}

static u32 platform_host_to_net_32(u32 HostInt)
{
	return htonl(HostInt);
}

static void platform_get_user_name(char *UserName, u32 Len)
{
	PWCHAR WideUserName = memory_alloc(&Engine.Memory, sizeof(WCHAR)*Len);
	DWORD OutSize = Len;
	GetUserNameW(WideUserName, &OutSize);

	WideCharToMultiByte(CP_UTF8, 0, WideUserName, OutSize, UserName, Len, 0, 0);
}

static void platform_get_config_path(char *Path, u32 Size)
{
	PWSTR Folder;
	KNOWNFOLDERID FolderId = FOLDERID_LocalAppData;
	HRESULT Res = SHGetKnownFolderPath(&FolderId, 0, 0, &Folder);

	if (Res == S_OK)
	{
		WideCharToMultiByte(CP_UTF8, 0, Folder, -1, Path, Size, 0, 0);
	}
	else
	{
		Path[0] = 0;
	}

	CoTaskMemFree(Folder);
}

static void platform_mutex(b32 *Lock)
{
	while (InterlockedCompareExchange(Lock, 1, 0))
		;
}

static u64 platform_get_file_modified_date(const char *Name)
{
	FILETIME FileTime = {0};
	ULARGE_INTEGER Ret;

	WCHAR *WName = platform_string_conversion_ut8_to_wchar(Name);

	_GetFileTime(WName, &FileTime);

	memory_free(&Engine.Memory, WName);

	Ret.HighPart = FileTime.dwHighDateTime;
	Ret.LowPart = FileTime.dwLowDateTime;

	return Ret.QuadPart;
}

static void platform_execute(const char *AppName, const char *Params, const char *WorkingDir)
{
	PWCHAR WAppName = platform_string_conversion_ut8_to_wchar(AppName);
	PWCHAR WParams = platform_string_conversion_ut8_to_wchar(Params);
	PWCHAR WWorkingDir = 0;
	if (WorkingDir)
	{
		WWorkingDir = platform_string_conversion_ut8_to_wchar(WorkingDir);
	}

	SHELLEXECUTEINFO ShExecInfo;
	ShExecInfo.cbSize = sizeof(SHELLEXECUTEINFO);
	ShExecInfo.fMask = 0;
	ShExecInfo.hwnd = NULL;
	ShExecInfo.lpVerb = NULL;
	ShExecInfo.lpFile = WAppName;
	ShExecInfo.lpParameters = WParams;
	ShExecInfo.lpDirectory = WWorkingDir;
	ShExecInfo.nShow = SW_SHOWNORMAL;
	ShExecInfo.hInstApp = NULL;

	ShellExecuteEx(&ShExecInfo);

	memory_free(&Engine.Memory, WAppName);
	memory_free(&Engine.Memory, WParams);
	memory_free(&Engine.Memory, WWorkingDir);
}

static void platform_exit()
{
	PostMessage(0, WM_QUIT, 0, 0);
}

static f64 platform_get_local_time()
{
	SYSTEMTIME Time;
	FILETIME FileTime;
	GetLocalTime(&Time);

	SystemTimeToFileTime(&Time, &FileTime);

	ULARGE_INTEGER Large; 
	Large.LowPart = FileTime.dwLowDateTime;
	Large.HighPart = FileTime.dwHighDateTime;

	return Large.QuadPart/10000000.0;
}

static u8* platform_alloc(u32 Size)
{
	void *Ret = VirtualAlloc((void*)0x12340000, Size, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);
	return Ret;
}

static void print_error(const char *Msg)
{	
	LPVOID lpMsgBuf;
	DWORD dw = GetLastError();
	FormatMessage(
			FORMAT_MESSAGE_ALLOCATE_BUFFER | 
			FORMAT_MESSAGE_FROM_SYSTEM |
			FORMAT_MESSAGE_IGNORE_INSERTS,
			NULL,
			dw,
			MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
			(LPTSTR) &lpMsgBuf,
			0, NULL );

	platform_log("[%d] %s: %s\n", dw, Msg, lpMsgBuf);
	LocalFree(lpMsgBuf);
}

static file_handle platform_file_create(const char *FileName)
{
	file_handle Ret;

	PWCHAR WideFileName = platform_string_conversion_ut8_to_wchar(FileName);
	Ret.Handle = CreateFile(WideFileName, GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ, 0, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);
	memory_free(&Engine.Memory, WideFileName);

	if (Ret.Handle == INVALID_HANDLE_VALUE)
	{
		print_error("Error creating file");
		return Ret;
	}

	return Ret;
}

static file_handle platform_file_open(const char *FileName)
{
	file_handle Ret;

	PWCHAR WideFileName = platform_string_conversion_ut8_to_wchar(FileName);
	Ret.Handle = CreateFile(WideFileName, GENERIC_READ, FILE_SHARE_READ, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_OVERLAPPED, 0);
	memory_free(&Engine.Memory, WideFileName);
	if (Ret.Handle == INVALID_HANDLE_VALUE)
	{
		print_error("Error opening file");
	}

	return Ret;
}

static u32 platform_file_read(file_handle FileHandle, u8 *Buffer, u32 BufferLen, u32 From)
{
	u32 Read = 0;
	OVERLAPPED Overlapped = {0};
	Overlapped.Offset = From;
	//TODO: Support files larger than 32 bits. The read parameter is 32, It seems reading in parts is the solution
	ReadFile(FileHandle.Handle, Buffer, BufferLen, &Read, &Overlapped);
	if (GetLastError() == ERROR_IO_PENDING)
	{
		GetOverlappedResult(FileHandle.Handle, &Overlapped, &Read, TRUE);
	}

	return Read;
}

static b32 platform_directory_create(const char *Path)
{
	WCHAR *WidePath = platform_string_conversion_ut8_to_wchar(Path);
	b32 Ret = CreateDirectory(WidePath, 0);
	memory_free(&Engine.Memory, WidePath);	

	return Ret;
}

static void platform_file_close(file_handle FileHandle)
{
	CloseHandle(FileHandle.Handle);
}

static u32 platform_file_write(file_handle FileHandle, u8 *Content, u32 Size, u32 From)
{
	OVERLAPPED Overlapped = {0};
	Overlapped.Offset = From;
	u32 Written;
	BOOL Ret = WriteFile(FileHandle.Handle, Content, Size, &Written, &Overlapped);
	if (!Ret)
	{
		print_error("Error writing file");
		return 0;
	}

	return Written;
}

static void platform_file_write_fully(const char *FullPath, u8 *Content, u32 Size)
{
	WCHAR *WideFullPath = platform_string_conversion_ut8_to_wchar(FullPath);

	HANDLE Handle = CreateFile(WideFullPath, GENERIC_WRITE, 0, 0, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);

	memory_free(&Engine.Memory, WideFullPath);

	if (Handle != INVALID_HANDLE_VALUE)
	{
		WriteFile(Handle, Content, Size, 0, 0);
		CloseHandle(Handle);
	}
	else
	{
		print_error("Error writing file");
	}
}

static u8* platform_file_read_fully(const char *FileName, u32* SizeRead)
{
	WCHAR *WideFileName = platform_string_conversion_ut8_to_wchar(FileName);
	HANDLE FileHandle = CreateFile(WideFileName, GENERIC_READ, FILE_SHARE_READ, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);
	memory_free(&Engine.Memory, WideFileName);

	if (FileHandle == INVALID_HANDLE_VALUE)
	{
		platform_log("Error opening file %s\n", FileName);
		print_error("system error");
		return 0;
	}

	u8 *Buffer = 0;
	LARGE_INTEGER FileSize;
	if (GetFileSizeEx(FileHandle, &FileSize))
	{
		Buffer = memory_alloc(&Engine.Memory, FileSize.LowPart);

		if (ReadFile(FileHandle, Buffer, FileSize.LowPart, SizeRead, 0) == FALSE)
		{
			memory_free(&Engine.Memory, Buffer);
			return 0;
		}

		if (FileSize.LowPart != *SizeRead)
		{
			memory_free(&Engine.Memory, Buffer);
			return 0;
		}
	}
	else
	{
		return 0;
	}

	CloseHandle(FileHandle);
	return Buffer;
}

static void *platform_load_gl_function(const char *Name)
{
	void* FnPtr = wglGetProcAddress(Name);
	if (!FnPtr)
	{
		HMODULE GlDll = LoadLibrary(L"opengl32.dll");
		FnPtr = GetProcAddress(GlDll, Name);
	}

	return FnPtr;
}

static void platform_hide_cursor()
{
	ShowCursor(0);
}

//Stolen from Raymond chen:
//https://devblogs.microsoft.com/oldnewthing/20100412-00/?p=14353
WINDOWPLACEMENT g_wpPrev = { sizeof(g_wpPrev) };
static void platform_switch_to_fullscreen()
{
  DWORD dwStyle = GetWindowLong(WindowHandle, GWL_STYLE);
  if (dwStyle & WS_OVERLAPPEDWINDOW) {
    MONITORINFO mi = { sizeof(mi) };
    if (GetWindowPlacement(WindowHandle, &g_wpPrev) &&
        GetMonitorInfo(MonitorFromWindow(WindowHandle,
                       MONITOR_DEFAULTTOPRIMARY), &mi)) {
      SetWindowLong(WindowHandle, GWL_STYLE,
                    dwStyle & ~WS_OVERLAPPEDWINDOW);
      SetWindowPos(WindowHandle, HWND_TOP,
                   mi.rcMonitor.left, mi.rcMonitor.top,
                   mi.rcMonitor.right - mi.rcMonitor.left,
                   mi.rcMonitor.bottom - mi.rcMonitor.top,
                   SWP_NOOWNERZORDER | SWP_FRAMECHANGED);
    }
  } else {
    SetWindowLong(WindowHandle, GWL_STYLE,
                  dwStyle | WS_OVERLAPPEDWINDOW);
    SetWindowPlacement(WindowHandle, &g_wpPrev);
    SetWindowPos(WindowHandle, NULL, 0, 0, 0, 0,
                 SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER |
                 SWP_NOOWNERZORDER | SWP_FRAMECHANGED);
  }
}

static float UserAspectRatio = 0.66f;

static void platform_set_window_title(const char *Name)
{
	WCHAR *WideName = platform_string_conversion_ut8_to_wchar(Name);
	SetWindowText(WindowHandle, WideName);
	memory_free(&Engine.Memory, WideName);
}

static void set_window_size(u32 Width, u32 Height)
{
	RECT Rect, Client;

	UserAspectRatio = Height/(float)Width;

	GetWindowRect(WindowHandle, &Rect);
	GetClientRect(WindowHandle, &Client);

	Width += (Rect.right-Rect.left) - Client.right;
	Height += (Rect.bottom-Rect.top) - Client.bottom;
	SetWindowPos(WindowHandle, HWND_TOP, 0, 0, Width, Height, SWP_NOMOVE);
}

#define HAS_MASK(Value, Mask) ((Value & Mask) == Mask)

static void update_xinput_gamepad_buttons(XINPUT_STATE *GamepadState, gamepad *Gamepad)
{
	u32 GamepadEnum[] = {
		0,
		XINPUT_GAMEPAD_DPAD_UP,
		XINPUT_GAMEPAD_DPAD_DOWN,
		XINPUT_GAMEPAD_DPAD_LEFT,
		XINPUT_GAMEPAD_DPAD_RIGHT,
		XINPUT_GAMEPAD_START,
		XINPUT_GAMEPAD_BACK,
		XINPUT_GAMEPAD_LEFT_THUMB,
		XINPUT_GAMEPAD_RIGHT_THUMB,
		XINPUT_GAMEPAD_LEFT_SHOULDER,
		XINPUT_GAMEPAD_RIGHT_SHOULDER,
		XINPUT_GAMEPAD_A,
		XINPUT_GAMEPAD_B,
		XINPUT_GAMEPAD_X,
		XINPUT_GAMEPAD_Y,
	};

	WORD Buttons = GamepadState->Gamepad.wButtons;
	for (u32 i=1; i<Gamepad_Max; ++i)
	{
		Gamepad->Buttons[i].CurState = HAS_MASK(Buttons, GamepadEnum[i]) ? ButtonState_Down : ButtonState_Up;
	}
}

static vec2 input_process_thumb(LONG X, LONG Y, LONG DeadZone)
{
	if ((DeadZone*DeadZone) < (X*X + Y*Y))
	{
		return Vec2(X/32768.0f, Y/32768.0f);
	}
	else
	{
		return Vec2(0, 0);
	}
}

static void HandleXInput()
{
	if (XInput_GetState)
	{
		XINPUT_STATE GamepadState;

		for (u32 g=0; g<GG_GAMEPAD_DEVICES; ++g)
		{
			gamepad *Gamepad = Input.Gamepads + g;
			DWORD Res = XInput_GetState(g, &GamepadState);
			if (Res == ERROR_SUCCESS)
			{
				Gamepad->IsConnected = 1;
				Gamepad->LeftThumb = input_process_thumb(GamepadState.Gamepad.sThumbLX, GamepadState.Gamepad.sThumbLY, XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE);

				update_xinput_gamepad_buttons(&GamepadState, Gamepad);
			}
			else
			{
				Gamepad->IsConnected = 0;
			}
		}
	}
}

//DInput (other gamepads than xbox)
//

BOOL is_xinput_device(long Data)
{
	int NumDev;
	int Ret = GetRawInputDeviceList(
								0,
								&NumDev,
								sizeof(RAWINPUTDEVICELIST));

	if (NumDev == 0)
		return 0;

	RAWINPUTDEVICELIST *RawInputDeviceList = malloc(sizeof(RAWINPUTDEVICELIST) * NumDev);
	Ret = GetRawInputDeviceList(
								RawInputDeviceList,
								&NumDev,
								sizeof(RAWINPUTDEVICELIST));

	for (int i=0; i<NumDev; ++i)
	{
		WCHAR DevName[128];
		int DevNameSize = 128;
		RID_DEVICE_INFO Info;
		int RidSize = sizeof(Info);
		u32 Count = GetRawInputDeviceInfoW(RawInputDeviceList[i].hDevice, RIDI_DEVICEINFO, &Info, &RidSize);
		Count = GetRawInputDeviceInfoW(RawInputDeviceList[i].hDevice, RIDI_DEVICENAME, &DevName, &DevNameSize);

		long Id = MAKELONG(Info.hid.dwVendorId, Info.hid.dwProductId);

		if (Data == Id)
		{
			if (wcsstr(DevName, L"&IG_") != 0)
			{
				free(RawInputDeviceList);
				return 1;
			}
		}
	}

	free(RawInputDeviceList);
	return 0;
}

static LPDIRECTINPUT8 di = 0;
static LPDIRECTINPUTDEVICE8 DInputJoysticks[GG_GAMEPAD_DEVICES] = {0};

BOOL is_joy_already_handled(LONG Data)
{
	DIPROPDWORD dipdw;  // DIPROPDWORD contains a DIPROPHEADER structure. 
	dipdw.diph.dwSize = sizeof(DIPROPDWORD);
	dipdw.diph.dwHeaderSize = sizeof(DIPROPHEADER);
	dipdw.diph.dwObj = 0; // device property 
	dipdw.diph.dwHow = DIPH_DEVICE;

	for (int g=0; g<GG_GAMEPAD_DEVICES; ++g)
	{
		LPDIRECTINPUTDEVICE8 Joy = DInputJoysticks[g];
		if (Joy)
		{
			HRESULT hr;
			hr = IDirectInputDevice8_GetProperty(Joy, DIPROP_VIDPID, &dipdw.diph);
			if (SUCCEEDED(hr)) {
				if (Data == dipdw.dwData)
					return 1;
			}
		}
	}
	
	return 0;
}

BOOL CALLBACK dinput_enum_callback(const DIDEVICEINSTANCE* Instance, VOID* Context)
{
	if (is_xinput_device(Instance->guidProduct.Data1))
        return DIENUM_CONTINUE;

	if (is_joy_already_handled(Instance->guidProduct.Data1))
        return DIENUM_CONTINUE;

	int FreeIndex = -1;
	for (int i=0; i<GG_GAMEPAD_DEVICES; ++i)
	{
		if (DInputJoysticks[i] == 0)
		{
			FreeIndex = i;
			break;
		}
	}

	//No more free gamepad slots, stop enumarating
	if (FreeIndex == -1)
		return DIENUM_STOP;

    // Obtain an interface to the enumerated joystick.
    IDirectInput8_CreateDevice(di, &Instance->guidInstance, &DInputJoysticks[FreeIndex], 0);

    return DIENUM_CONTINUE;
}

void dinput_discover_devices()
{
	HRESULT hr;
	// Look for the first simple joystick we can find.
	if (FAILED(hr = IDirectInput8_EnumDevices(di, DI8DEVCLASS_GAMECTRL, dinput_enum_callback,
					0, DIEDFL_ATTACHEDONLY))) 
	{
		return;
	}

	// Make sure we got a joystick
	int GamepadPresent = 0;
	for (int i=0; i<GG_GAMEPAD_DEVICES; ++i)
	{
		if (DInputJoysticks[i])
		{
			++GamepadPresent;
			break;
		}
	}

	if (GamepadPresent == 0) 
	{
		return;
	}

	DIDEVICEINSTANCE pdidi;
	pdidi.dwSize = sizeof(pdidi);
	for (int i=0; i<GamepadPresent; ++i)
	{
		LPDIRECTINPUTDEVICE8 Joystick = DInputJoysticks[i];
		IDirectInputDevice8_GetDeviceInfo(Joystick, &pdidi);

		// Set the data format to "simple joystick" - a predefined data format 
		//
		// A data format specifies which controls on a device we are interested in,
		// and how they should be reported. This tells DInput that we will be
		// passing a DIJOYSTATE2 structure to IDirectInputDevice::GetDeviceState().
		if (FAILED(hr =  IDirectInputDevice8_SetDataFormat(Joystick, &c_dfDIJoystick2))) 
		{
			continue;
		}

		// Set the cooperative level to let DInput know how this device should
		// interact with the system and with other DInput applications.
		if (FAILED(hr = IDirectInputDevice8_SetCooperativeLevel(Joystick, WindowHandle, DISCL_EXCLUSIVE |
													  DISCL_FOREGROUND))) 
		{
			continue;
		}
	}
}

void dinput_init()
{
	//NOTE: Got this from here: https://www.cs.cmu.edu/~jparise/directx/joystick/
	//TODO: Try loading the dll instead of static linking
	//	HMODULE DInputDll = LoadLibraryA("dinput8.dll");
	HRESULT hr;

	IID Iid = IID_IDirectInput8;

	// Create a DirectInput device
	if (FAILED(hr = DirectInput8Create(GetModuleHandle(0), 
					DIRECTINPUT_VERSION, 
					&Iid, 
					(VOID**)&di, 
					0))) 
	{
		di = 0;
		return;
	}

	dinput_discover_devices();
}

HRESULT dinput_poll()
{
	DIJOYSTATE2 js;
    HRESULT     hr;

	if (di == 0)
	{
		//Error initializing DirectInput, skipping
		return 0;
	}

	//how many free joystick slot do we have?
	int Busy = 0;
	for (u32 g=0; g<GG_GAMEPAD_DEVICES; ++g)
	{
		gamepad *Gamepad = Input.Gamepads + g;
		if (Gamepad->IsConnected)
			++Busy;
	}

	//All gamepads are managed by xinput
	if (Busy == GG_GAMEPAD_DEVICES)
		return S_OK;

	gamepad *Gamepad = 0;
	for (u32 i=0; i<GG_GAMEPAD_DEVICES; ++i)
	{
		Gamepad = Input.Gamepads + i;
		if (!Gamepad->IsConnected)
			break;
	}

	for (u32 g=0; g<GG_GAMEPAD_DEVICES; ++g)
	{
		LPDIRECTINPUTDEVICE8 Joystick = DInputJoysticks[g];

		//Try next, we can have holes in the joy array
		if (Joystick == 0)
			continue;

		// Poll the device to read the current state
		hr = IDirectInputDevice8_Poll(Joystick);
		if (FAILED(hr)) 
		{
			hr = IDirectInputDevice8_Acquire(Joystick);

			// If we encounter a fatal error, return failure.
			if ((hr == DIERR_INVALIDPARAM) || (hr == DIERR_NOTINITIALIZED)) {
				platform_log("fatal error invalid param or not initialized failed %lld\n", hr);
				continue;
			}

			// If another application has control of this device, return successfully.
			// We'll just have to wait our turn to use the joystick.
			//Also when jozstick is unplugged
			if (hr == DIERR_OTHERAPPHASPRIO) {
				continue;
			}

			if (FAILED(hr))
			{
				//Probably controller was unplugged, nulling object
				DInputJoysticks[g] = 0;
				continue;
			}
		}

		// Get the input's device state
		if (FAILED(hr = IDirectInputDevice8_GetDeviceState(Joystick, sizeof(DIJOYSTATE2), &js))) 
		{
			continue; // The device should have been acquired during the Poll()
		}

		//Try to mimic a gamepad structure
		LONG ThumbX = js.lX - 32768;
		LONG ThumbY = 32768 - js.lY; //Y axis is inverted, hope is DI design and not ps4 pad
		Gamepad->LeftThumb = input_process_thumb(ThumbX, ThumbY, XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE);

		ThumbX = js.lZ - 32768;
		ThumbY = 32768 - js.lRz; 
		Gamepad->RightThumb = input_process_thumb(ThumbX, ThumbY, XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE);

		Gamepad->LeftTrigger = js.lRx/65535.0f;
		Gamepad->RightTrigger = js.lRy/65535.0f;

		Gamepad->Buttons[Gamepad_DpadUp].CurState = ButtonState_Up;
		Gamepad->Buttons[Gamepad_DpadDown].CurState = ButtonState_Up;
		Gamepad->Buttons[Gamepad_DpadRight].CurState = ButtonState_Up;
		Gamepad->Buttons[Gamepad_DpadLeft].CurState = ButtonState_Up;

		if (js.rgdwPOV[0] != -1)
		{
			int Div = js.rgdwPOV[0]/4500;
			switch(Div)
			{
				case 0:
					Gamepad->Buttons[Gamepad_DpadUp].CurState = ButtonState_Down;
					break;
				case 1:
					Gamepad->Buttons[Gamepad_DpadUp].CurState = ButtonState_Down;
					Gamepad->Buttons[Gamepad_DpadRight].CurState = ButtonState_Down;
					break;
				case 2:
					Gamepad->Buttons[Gamepad_DpadRight].CurState = ButtonState_Down;
					break;
				case 3:
					Gamepad->Buttons[Gamepad_DpadRight].CurState = ButtonState_Down;
					Gamepad->Buttons[Gamepad_DpadDown].CurState = ButtonState_Down;
					break;
				case 4:
					Gamepad->Buttons[Gamepad_DpadDown].CurState = ButtonState_Down;
					break;
				case 5:
					Gamepad->Buttons[Gamepad_DpadDown].CurState = ButtonState_Down;
					Gamepad->Buttons[Gamepad_DpadLeft].CurState = ButtonState_Down;
					break;
				case 6:
					Gamepad->Buttons[Gamepad_DpadLeft].CurState = ButtonState_Down;
					break;
				case 7:
					Gamepad->Buttons[Gamepad_DpadLeft].CurState = ButtonState_Down;
					Gamepad->Buttons[Gamepad_DpadUp].CurState = ButtonState_Down;
					break;
			}
		}

		Gamepad->Buttons[Gamepad_A].CurState = js.rgbButtons[1] == 0 ? ButtonState_Up : ButtonState_Down;
		Gamepad->Buttons[Gamepad_B].CurState = js.rgbButtons[2] == 0 ? ButtonState_Up : ButtonState_Down;
		Gamepad->Buttons[Gamepad_X].CurState = js.rgbButtons[0] == 0 ? ButtonState_Up : ButtonState_Down;
		Gamepad->Buttons[Gamepad_Y].CurState = js.rgbButtons[3] == 0 ? ButtonState_Up : ButtonState_Down;
		Gamepad->Buttons[Gamepad_LeftShoulder].CurState = js.rgbButtons[4] == 0 ? ButtonState_Up : ButtonState_Down;
		Gamepad->Buttons[Gamepad_RightShoulder].CurState = js.rgbButtons[5] == 0 ? ButtonState_Up : ButtonState_Down;
		Gamepad->Buttons[Gamepad_Back].CurState = js.rgbButtons[8] == 0 ? ButtonState_Up : ButtonState_Down;
		Gamepad->Buttons[Gamepad_Start].CurState = js.rgbButtons[9] == 0 ? ButtonState_Up : ButtonState_Down;
		Gamepad->Buttons[Gamepad_LeftThumb].CurState = js.rgbButtons[10] == 0 ? ButtonState_Up : ButtonState_Down;
		Gamepad->Buttons[Gamepad_RightThumb].CurState = js.rgbButtons[11] == 0 ? ButtonState_Up : ButtonState_Down;

		Gamepad->IsConnected = 1;
		Gamepad++;
	}

	return S_OK;
}

///END Dinput


//XAudio2
//
//XAudio will be set to play wav files with the following format:
// Channels 2
// SampleRate 44100
// AvgSampleRate 176400
// BlockAlign 4
// BitsPerSample 16
//
static IXAudio2 *XAudio2 = 0;
static IXAudio2MasteringVoice *MasterVoice = 0;
static IXAudio2SourceVoice *SourceVoice = 0;

static IXAudio2VoiceCallbackVtbl XAudioCallbacksTable = {0};
static IXAudio2VoiceCallback XAudioCallbacks = {0};

static s8 AudioBuffer1[4096*2];
static s8 AudioBuffer2[4096*2];
static s8 *CurAudioBuffer = AudioBuffer1;

static void xaudio_buffer_end(IXAudio2VoiceCallback *VoiceCb, void *BufferContext)
{
}

static void xaudio_stream_end(IXAudio2VoiceCallback *VoiceCb)
{
}

static void xaudio_buffer_start(IXAudio2VoiceCallback *VoiceCb, void *Context)
{
}

static void xaudio_loop_end(IXAudio2VoiceCallback *VoiceCb, void *Context)
{
}

static void xaudio_voice_error(IXAudio2VoiceCallback *VoiceCb, void *Context, HRESULT Res)
{
}

static void xaudio_processing_pass_end(IXAudio2VoiceCallback *VoiceCb)
{
}

static void xaudio_processing_pass_start(IXAudio2VoiceCallback *VoiceCb, UINT32 Len)
{
}

static void audio_send_data(engine *Engine)
{
	sound_request_buffer_mix(Engine, CurAudioBuffer, sizeof(AudioBuffer1));
	XAUDIO2_BUFFER Audio2Buffer = {0};
	Audio2Buffer.AudioBytes = sizeof(AudioBuffer1);
	Audio2Buffer.pAudioData = CurAudioBuffer;
	IXAudio2SourceVoice_SubmitSourceBuffer(SourceVoice, &Audio2Buffer, 0);

	if (CurAudioBuffer == AudioBuffer1)
		CurAudioBuffer = AudioBuffer2;
	else
		CurAudioBuffer = AudioBuffer1;
}

void init_sound()
{
	XAudioCallbacksTable.OnStreamEnd = xaudio_stream_end;
    XAudioCallbacksTable.OnBufferEnd = xaudio_buffer_end;
	XAudioCallbacksTable.OnBufferStart = xaudio_buffer_start;
	XAudioCallbacksTable.OnLoopEnd = xaudio_loop_end;
	XAudioCallbacksTable.OnVoiceError = xaudio_voice_error;
	XAudioCallbacksTable.OnVoiceProcessingPassEnd = xaudio_processing_pass_end;
	XAudioCallbacksTable.OnVoiceProcessingPassStart = xaudio_processing_pass_start;

    XAudioCallbacks.lpVtbl = &XAudioCallbacksTable;

	HRESULT Res = CoInitializeEx(0, COINIT_MULTITHREADED);
	if (Res == S_OK)
	{
		Res = XAudio2Create(&XAudio2, 0, XAUDIO2_DEFAULT_PROCESSOR);
		if (Res == S_OK)
		{
			Res = IXAudio2_CreateMasteringVoice(XAudio2, &MasterVoice, XAUDIO2_DEFAULT_CHANNELS, XAUDIO2_DEFAULT_SAMPLERATE, 0, 0, 0, AudioCategory_GameEffects);
			if (Res == S_OK)
			{
				WAVEFORMATEX Fmt = {1, 2, 44100, 176400, 4, 16, 0};
				Res = IXAudio2_CreateSourceVoice(XAudio2, &SourceVoice, &Fmt, 0, XAUDIO2_DEFAULT_FREQ_RATIO, &XAudioCallbacks, 0, 0);
				if (Res != S_OK)
				{
					platform_log("ERROR creating xaudio2 voice index \n");
					return;
				}
			}
			else
			{
				platform_log("XAudio 2 failed to create a master voice\n");
				return;
			}
		}
		else
		{
			platform_log("XAudio 2 failed to initialize\n");
			return;
		}
	}
	else
	{
		platform_log("CoInitializeEx failed can't have sound\n");
		return;
	}

	audio_send_data(&Engine);
	IXAudio2SourceVoice_Start(SourceVoice, 0, XAUDIO2_COMMIT_NOW);
}

DWORD WINAPI sound_thread(_In_ LPVOID lpParameter)
{
	while(1)
	{
		engine *LocalEngine = (engine*)lpParameter;
		XAUDIO2_VOICE_STATE VoiceState = { 0 };
		IXAudio2SourceVoice_GetState(SourceVoice, &VoiceState, 0);

		if (VoiceState.BuffersQueued < 2)
		{
			audio_send_data(LocalEngine);
		}

		sound_update(LocalEngine);
	}

	return 0;
}

typedef struct file_op
{
	HANDLE Handle;
	WIN32_FIND_DATA Data;
	WCHAR *CurPath;
} file_op;

char *FileOperationExtension = 0;
WCHAR *FileOperationBasePath = 0;
char FindFileNameU8[MAX_PATH*2];
file_op *FileOpDirectoryList = 0;

static b32 platform_list_file_operation_start(char *Path, char *Extension)
{
	if (FileOpDirectoryList)
	{
		platform_log("Error on file operation start. Close the old one before starting a new one!\n");
		platform_log("Previous path is %s\n", FileOpDirectoryList->CurPath);
		return 0;
	}

	file_op FileOp = {INVALID_HANDLE_VALUE};
	FileOperationBasePath = platform_string_conversion_ut8_to_wchar((const char*)Path);
	FileOperationExtension = string_dup(&Engine, Extension);
	FileOpDirectoryList = dynamic_array_create(&Engine, file_op);
	dynamic_array_add(&FileOpDirectoryList, FileOp);

	return 1;
}

static char *platform_list_file_operation_next();

static char* platform_list_file_operation_end()
{
	u32 Len = dynamic_array_len(FileOpDirectoryList);
	file_op *FileOp = FileOpDirectoryList + Len - 1;
	memory_free(&Engine.Memory, FileOp->CurPath);

	if (Len > 1)
	{
		dynamic_array_remove_at(FileOpDirectoryList, Len - 1);
		return platform_list_file_operation_next();
	}

	memory_free(&Engine.Memory, FileOperationExtension);
	memory_free(&Engine.Memory, FileOperationBasePath);
	dynamic_array_destroy(FileOpDirectoryList);
	FileOpDirectoryList = 0;
	FileOperationExtension = 0;
	FileOperationBasePath = 0;

	return 0;
}

static void platform_get_current_dir(char *OutputDir, u32 BufferSize)
{
	LPWSTR WDir = memory_alloc(&Engine.Memory, BufferSize * sizeof(TCHAR));
	GetCurrentDirectoryW(BufferSize, WDir);

	WideCharToMultiByte(CP_UTF8, 0, WDir, BufferSize, OutputDir, BufferSize, 0, 0);

	memory_free(&Engine.Memory, WDir);
}

static char *file_operation_next_or_return(file_op *FileOp)
{
	if (FileOp->Data.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN)
		return platform_list_file_operation_next();
	   
	//Skip if it starts with a .
	if (FileOp->Data.cFileName[0] == '.')
	{
		return platform_list_file_operation_next();
	}

	WideCharToMultiByte(CP_UTF8, 0, FileOp->Data.cFileName, -1, FindFileNameU8, MAX_PATH*2, 0, 0);

	if (FileOp->Data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
	{
		if (string_compare(FindFileNameU8, ".."))
			return platform_list_file_operation_next();
		else
		{
			file_op NextOp = {INVALID_HANDLE_VALUE};

			if (FileOp->CurPath)
			{
				u32 Len = wcslen(FileOp->Data.cFileName) + wcslen(FileOp->CurPath) + 1;
				NextOp.CurPath = memory_alloc(&Engine.Memory, (Len*2));
				wsprintf(NextOp.CurPath, L"%s/%s", FileOp->CurPath, FileOp->Data.cFileName);
			}
			else
			{
				u32 Len = wcslen(FileOp->Data.cFileName);
				NextOp.CurPath = memory_alloc(&Engine.Memory, (Len*2)+1);
				wmemcpy(NextOp.CurPath, FileOp->Data.cFileName, Len+1);
			}
			
			dynamic_array_add(&FileOpDirectoryList, NextOp);
			return platform_list_file_operation_next();
		}
	}

	if (string_compare(FindFileNameU8 + string_reverse_index_of(FindFileNameU8, '.')+1, FileOperationExtension))
	{
		if (FileOp->CurPath)
		{
			char *DupFileName = string_dup(&Engine, FindFileNameU8);
			char CurPath8[MAX_PATH];
			WideCharToMultiByte(CP_UTF8, 0, FileOp->CurPath, -1, CurPath8, MAX_PATH*2, 0, 0);
			sprintf(FindFileNameU8, "%s/%s", CurPath8, DupFileName);
			string_delete(&Engine, DupFileName);	
		}
		return FindFileNameU8;
	}
	else
		return platform_list_file_operation_next();
}

static char *platform_list_file_operation_next()
{
	s32 Index = dynamic_array_len(FileOpDirectoryList);
	assert(Index > 0);
	file_op *FileOp = FileOpDirectoryList + Index - 1;
	if (FileOp->Handle == INVALID_HANDLE_VALUE)
	{
		WCHAR FindPath[MAX_PATH];
		if (FileOp->CurPath)
			wsprintf(FindPath, L"%s/%s/*", FileOperationBasePath, FileOp->CurPath);
		else
			wsprintf(FindPath, L"%s/*", FileOperationBasePath);

		FileOp->Handle = FindFirstFile(FindPath, &FileOp->Data);
		//No file found
		if (FileOp->Handle == INVALID_HANDLE_VALUE)
		{
			return platform_list_file_operation_end();
		}
		
		return file_operation_next_or_return(FileOp);
	}

	if (FindNextFile(FileOp->Handle, &FileOp->Data))
	{
		return file_operation_next_or_return(FileOp);
	}
	else
	{
		return platform_list_file_operation_end();
	}
}

static char *platform_file_dialog_picker(char *Path, file_picker_mode Mode, char *TypeDescription, const char *TypeExtension)
{
	char *Ret = 0;

	COMDLG_FILTERSPEC rgSpec[] = {0, 0};
	rgSpec[0].pszName = platform_string_conversion_ut8_to_wchar(TypeDescription);
	rgSpec[0].pszSpec = platform_string_conversion_ut8_to_wchar(TypeExtension);
	LPCWSTR DirPath = platform_string_conversion_ut8_to_wchar(Path);

	WCHAR WorkingDir[MAX_PATH];
	GetFullPathName(DirPath, MAX_PATH, WorkingDir, 0);

	memory_free(&Engine.Memory, (void*)DirPath);

	HRESULT hr = 0;
	IFileDialog *pfd = NULL;
	if (Mode == FP_Open)
	{
		// CoCreate the File Open Dialog object.
		hr = CoCreateInstance(&CLSID_FileOpenDialog, 
						  NULL, 
						  CLSCTX_INPROC_SERVER, 
						  &IID_IFileDialog,
						  &pfd);
	}
	else
	{
		hr = CoCreateInstance(&CLSID_FileSaveDialog, 
						  NULL, 
						  CLSCTX_INPROC_SERVER, 
						  &IID_IFileDialog,
						  &pfd);
	}

    if (SUCCEEDED(hr))
	{
		// Set the options on the dialog.
		DWORD dwFlags;

		// Before setting, always get the options first in order 
		// not to override existing options.
		hr = IFileDialog_GetOptions(pfd, &dwFlags);
		if (SUCCEEDED(hr))
		{
			// In this case, get shell items only for file system items.
			hr = IFileDialog_SetOptions(pfd, dwFlags | FOS_FORCEFILESYSTEM);
			if (SUCCEEDED(hr))
			{
				// Set the file types to display only. 
				// Notice that this is a 1-based array.
				hr = IFileDialog_SetFileTypes(pfd, 1, rgSpec);
				IFileDialog_SetDefaultExtension(pfd, rgSpec[0].pszSpec + 2);
				if (SUCCEEDED(hr))
				{
					IShellItem *FolderItem;
					hr = SHCreateItemFromParsingName(WorkingDir, 0, &IID_IShellItem, &FolderItem);
					if (SUCCEEDED(hr)) 
					{
						hr = IFileDialog_SetDefaultFolder(pfd, FolderItem);
						IShellItem_Release(FolderItem);

						if (SUCCEEDED(hr))
						{
							// Show the dialog
							hr = IFileDialog_Show(pfd, NULL);
							if (SUCCEEDED(hr))
							{
								// Obtain the result once the user clicks 
								// the 'Open' button.
								// The result is an IShellItem object.
								IShellItem *psiResult;
								hr = IFileDialog_GetResult(pfd, &psiResult);
								if (SUCCEEDED(hr))
								{
									// We are just going to print out the 
									// name of the file for sample sake.
									PWSTR pszFilePath = NULL;
									hr = IShellItem_GetDisplayName(psiResult, SIGDN_FILESYSPATH, 
											&pszFilePath);
									if (SUCCEEDED(hr))
									{

										int StrSize = WideCharToMultiByte(CP_UTF8, 0, pszFilePath, -1, 0, 0, 0, 0);
										Ret = memory_alloc(&Engine.Memory, StrSize);
										WideCharToMultiByte(CP_UTF8, 0, pszFilePath, -1, Ret, StrSize, 0, 0);

										CoTaskMemFree(pszFilePath);
									}
									IShellItem_Release(psiResult);
								}
							}
						}
					}
				}
			}
		}
		IFileOpenDialog_Release(pfd);
	}

	memory_free(&Engine.Memory, (void*)rgSpec[0].pszName);
	memory_free(&Engine.Memory, (void*)rgSpec[0].pszSpec);

    return Ret;
}

int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PWSTR pCmdLine, int nCmdShow)
{
	ULONGLONG PrevTime;

	//TODO: Check for usb name for different game pads: WinUsb_QueryDeviceInformation
	HMODULE XInputDll = LoadLibrary(L"Xinput1_4.dll");
	if (XInputDll == 0)
	{
		//Loading old xinput version
		XInputDll = LoadLibrary(L"Xinput9_1_0.dll");
	}

	//If xinput is not present then there's no gamepad support
	if (XInputDll)
	{
		XInput_GetState = (XInputGetState_Func)GetProcAddress(XInputDll, "XInputGetState");	
	}

	platform Platform = {0};
	Platform.FileReadFully = platform_file_read_fully;
	Platform.FileRead = platform_file_read;
	Platform.FileOpen = platform_file_open;
	Platform.FileCreate = platform_file_create;
	Platform.FileClose = platform_file_close;
	Platform.FileWriteFully = platform_file_write_fully;
	Platform.FileWrite = platform_file_write;
	Platform.FilePicker = platform_file_dialog_picker;
	Platform.FileCopy = platform_file_copy;
	Platform.DirectoryCreate = platform_directory_create;
	Platform.LoadGLFunction = platform_load_gl_function;
	Platform.Log = platform_log;
	Platform.SetWindowSize = set_window_size;
	Platform.HideCursor = platform_hide_cursor;
	Platform.GetLocalTime = platform_get_local_time;
	Platform.SetWindowTitle = platform_set_window_title;
	Platform.GetFileModifiedDate = platform_get_file_modified_date;
	Platform.Exit = platform_exit;
	Platform.Execute = platform_execute;
	Platform.SwitchFullScreen = platform_switch_to_fullscreen;
	Platform.Mutex = platform_mutex;
	Platform.ServerStart = platform_server_start;
	Platform.ServerStop = platform_server_stop;
	Platform.ClientJoin = platform_client_join;
	Platform.ClientExit = platform_client_exit;
	Platform.SocketRead = platform_socket_read;
	Platform.SocketWrite = platform_socket_write;
	Platform.ThreadCreate = platform_create_thread;
	Platform.ThreadExit = platform_exit_thread;
	Platform.NetToHost16 = platform_net_to_host_16;
	Platform.NetToHost32 = platform_net_to_host_32;
	Platform.HostToNet16 = platform_host_to_net_16;
	Platform.HostToNet32 = platform_host_to_net_32;
	Platform.GetIp4Address = platform_get_ipv4_address;
	Platform.GetUserName = platform_get_user_name;
	Platform.GetConfigPath = platform_get_config_path;
	Platform.KeyboardShow = platform_keyboard_show;
	Platform.KeyboardHide = platform_keyboard_hide;
	Platform.ListFileStart = platform_list_file_operation_start;
	Platform.ListFileNext = platform_list_file_operation_next;
	Platform.GetCurrentDir = platform_get_current_dir;
	
	render Render = {0};

#if DEBUG
	Engine.AssetDatabase.RootPath = "../../assets/";
#else
	Engine.AssetDatabase.RootPath = "assets/";
#endif


	Engine.Platform = &Platform;
	Engine.Render = &Render;
	Engine.Input = &Input;
	Engine.Memory = memory_init(platform_alloc(MB(10)), MB(10), platform_mutex);
#if DEBUG_MEMORY
	Engine.Memory.DebugLog = platform_log2;
#endif

	imgui ImGui = {0};
	Engine.ImGui = &ImGui;

	net Net = {0};
	Engine.Net = &Net;

    // Register the window class.
    const wchar_t CLASS_NAME[]  = L"GBG Engine app";
    
    WNDCLASSEX wc = {0};

	wc.cbSize = sizeof(WNDCLASSEX);
    wc.lpfnWndProc   = WindowProc;
    wc.hInstance     = hInstance;
    wc.lpszClassName = CLASS_NAME;
	wc.style = CS_OWNDC;
	wc.hCursor = LoadImage(0, L"assets/res/cursor.ico", IMAGE_ICON, 32, 32, LR_LOADFROMFILE);
	if (wc.hCursor == 0)
		wc.hCursor = LoadCursor(0, IDC_ARROW);
#ifdef DEBUG
	wc.hIcon = LoadImage(0, L"../../assets/res/appicon.ico", IMAGE_ICON, 256, 256, LR_LOADFROMFILE);
#else
	wc.hIcon = LoadImage(0, L"assets/res/appicon.ico", IMAGE_ICON, 256, 256, LR_LOADFROMFILE);
#endif
	wc.hIconSm = wc.hIcon;

    RegisterClassEx(&wc);

    // Create the window.

	//TODO: Change the title bar text should be part of the game and not the platform
    WindowHandle = CreateWindowEx(
        0,   // Optional window styles.
        CLASS_NAME,                     // Window class
        0, 	//WINDOW TEXT
		WS_OVERLAPPEDWINDOW ,

        // Size and position
        CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT,

        NULL,       // Parent window    
        NULL,       // Menu
        hInstance,  // Instance handle
        NULL        // Additional application data
        );

    if (WindowHandle == NULL)
    {
        return 0;
    }

	//NOTE: Dinput needs the window Handle
	dinput_init();

	render_init(&Engine);
	engine_init(&Engine);
	init_sound();
	CreateThread(0, 0, sound_thread, &Engine, 0, 0);

    ShowWindow(WindowHandle, nCmdShow);

	LoadGameFunc();

	PrevTime = GetTickCount64();
    MSG msg = {0};
    while (1)
    {
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				break;
			}

			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else
		{
			//NOTE: The stencil buffer will not clear properly if the mask is not set to all 1
			Engine.Render->OpenGL.StencilMask(0xFF);
			Engine.Render->OpenGL.Clear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT|GL_STENCIL_BUFFER_BIT);
			DllSwap();
			
			HandleXInput();
			dinput_poll();

			ULONGLONG DT = GetTickCount64()-PrevTime;
			PrevTime = GetTickCount64();
			f32 DeltaTime = DT/1000.0f;
			game_update(&Engine, DeltaTime);
			engine_update(&Engine, DeltaTime);

			SwapBuffers(WindowHandleToDeviceContext);

			Engine.Reloaded = 0;
		}
    }

    return 0;
}

LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    switch (uMsg)
    {
	case WM_CREATE:
		{
			PIXELFORMATDESCRIPTOR pfd = {
				sizeof(PIXELFORMATDESCRIPTOR),
				1,
				PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER, //Flags
				PFD_TYPE_RGBA,	//Framebuffer kind
				32,
				0, 0, 0, 0, 0, 0,
				0,
				0,
				0, 0, 0, 0,
				24,	//depthbuffer bits
				8,  //stencil buffer
				0, //aux buffers
				PFD_MAIN_PLANE,
				0,
				0, 0, 0
			};

			WindowHandleToDeviceContext = GetDC(hwnd);
			int LetWindowChoosteThisPixelFormat = ChoosePixelFormat(WindowHandleToDeviceContext, &pfd);
			SetPixelFormat(WindowHandleToDeviceContext, LetWindowChoosteThisPixelFormat, &pfd);

			OpenGLRenderingContext = wglCreateContext(WindowHandleToDeviceContext);
			wglMakeCurrent(WindowHandleToDeviceContext, OpenGLRenderingContext);

			PFNWGLGETEXTENSIONSSTRINGARBPROC wglGetExtensionsStringARB = (PFNWGLGETEXTENSIONSSTRINGARBPROC)wglGetProcAddress("wglGetExtensionsStringARB");

			printf("%s\n", wglGetExtensionsStringARB(WindowHandleToDeviceContext));
			const char *Extensions = wglGetExtensionsStringARB(WindowHandleToDeviceContext);

			char *Context = 0;
			char *Tok = strtok_s((char*)Extensions, " ", &Context);
			while (Tok != 0)
			{
				if (strcmp(Tok, "WGL_EXT_swap_control") == 0)
				{
					PFNWGLSWAPINTERVALEXTPROC wglSwapIntervalEXT = (PFNWGLSWAPINTERVALEXTPROC)wglGetProcAddress("wglSwapIntervalEXT");
					wglSwapIntervalEXT(1);
					break;
				}	
				Tok = strtok_s(0, " ", &Context);
			}

			DEV_BROADCAST_DEVICEINTERFACE NotificationFilter = {0};

			NotificationFilter.dbcc_size = sizeof(NotificationFilter);
			NotificationFilter.dbcc_devicetype = DBT_DEVTYP_DEVICEINTERFACE;
			NotificationFilter.dbcc_reserved = 0;

			NotificationFilter.dbcc_classguid = GUID_DEVINTERFACE_USB_DEVICE;

			HDEVNOTIFY hDevNotify = RegisterDeviceNotification(hwnd, &NotificationFilter, DEVICE_NOTIFY_SERVICE_HANDLE);

			return 0;
		}

	case WM_DEVICECHANGE:
		dinput_discover_devices();
		return 0;

    case WM_DESTROY:
		wglDeleteContext(OpenGLRenderingContext);
        PostQuitMessage(0);
        return 0;

	case WM_MOUSEWHEEL:
	{
		short WheelDelta = GET_WHEEL_DELTA_WPARAM(wParam);
		Engine.Input->MouseState.WheelDelta = WheelDelta/4;
		return 0;
	}
	case WM_MOUSEMOVE:
		{
			f32 xPos = (f32)GET_X_LPARAM(lParam);
			f32 yPos = (f32)WindowHeight - GET_Y_LPARAM(lParam);
			if (Engine.Render)
			{
				vec2 Origin = Engine.Render->ScreenOrigin;
				vec2 Scale = Vec2(Engine.Render->DesignSize.x / (WindowWidth + Origin.x*2), Engine.Render->DesignSize.y / (WindowHeight + Origin.y*2));
				xPos = xPos * Scale.x;
				xPos += Origin.x * Scale.x;
				yPos = yPos * Scale.y;
				yPos += Origin.y * Scale.y;
			}
			Input.MouseState.MousePos = Vec2(xPos, yPos);
		}
		return 0;

	case WM_RBUTTONDOWN:
		{
			Input.MouseState.RightButton.CurState = ButtonState_Down;
		}
		return 0;

	case WM_RBUTTONUP:
		{
			Input.MouseState.RightButton.CurState = ButtonState_Up;
		}
		return 0;

	case WM_LBUTTONDOWN:
		{
			Input.MouseState.LeftButton.CurState = ButtonState_Down;
		}
		return 0;

	case WM_LBUTTONUP:
		{
			Input.MouseState.LeftButton.CurState = ButtonState_Up;
		}
		return 0;

	case WM_CHAR:
		{
			key Key = {.State = ButtonState_Down};
			WideCharToMultiByte(CP_UTF8, 0, (WCHAR*)&wParam, 1, Key.KeyChar, sizeof(Key.KeyChar), 0, 0);
			process_key(&Engine, Key);
		}
		return 0;
	case WM_KEYDOWN:
		{
			key Key = KeyFromCode(wParam, ButtonState_Down);
			process_key(&Engine, Key);
		}
		return 0;
	case WM_KEYUP:
		{
			key Key = KeyFromCode(wParam, ButtonState_Up);
			process_key(&Engine, Key);
		}
		return 0;

	case WM_SIZE:
		{
			//TODO: Maybe all this belongs to the game on how it wants to handle the viewport change?
			GLsizei w = LOWORD(lParam);
			GLsizei h = HIWORD(lParam);
			WindowHeight = h;
			WindowWidth = w;

			GLsizei newh = (GLsizei)(w*UserAspectRatio);
			GLsizei neww = (GLsizei)(h/UserAspectRatio);

			Engine.Render->ScreenOrigin = Vec2(0, 0);
			Engine.Render->DesignScreenOrigin = Vec2(0, 0);

			if (newh > h)
			{
				s32 DiffH = newh - h;
				Engine.Render->ScreenOrigin.y = (f32)(DiffH/2);
				Engine.Render->DesignScreenOrigin.y = (f32)(DiffH/2)*(Engine.Render->DesignSize.y/newh);

				Engine.Render->OpenGL.Viewport(0, -DiffH/2, w, h+DiffH);
			}
			else if (neww > w)
			{
				s32 DiffW = neww - w;
				Engine.Render->ScreenOrigin.x = (f32)(DiffW/2);
				Engine.Render->DesignScreenOrigin.x = (DiffW/2)*(Engine.Render->DesignSize.x/neww);
				Engine.Render->OpenGL.Viewport(-DiffW/2, 0, w+DiffW, h);
			}
			else
			{
				Engine.Render->OpenGL.Viewport(0, 0, w, h);
			}
		}
		return 0;

	case WM_INPUT:
		platform_log("device input\n");
		break;
	}
    return DefWindowProc(hwnd, uMsg, wParam, lParam);
}

