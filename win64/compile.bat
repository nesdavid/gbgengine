@echo off
set OutputDir=

if "%1"=="" set OutputDir=debug
if "%1"=="debug" set OutputDir=debug
if "%1"=="development" set OutputDir=development
if "%1"=="release" set OutputDir=release

set LinkerFlags= /link /SUBSYSTEM:WINDOWS
set CommonFlags=/I ..\..\gbgengine /I ..\..\gbgengine /I ..\..\gbgengine\third_party /nologo /DPLAT_WIN64 /Zi /W2

if "%OutputDir%"=="" (
	echo Incorrect command. Expected 'debug' or 'release'
	goto end
) else if "%OutputDir%"=="debug" (
	set CompilerFlags= %CommonFlags%  /DDEBUG /MDd /Od 
	set LinkerFlags= /link /SUBSYSTEM:WINDOWS 
) else if "%OutputDir%"=="development" (
	set CompilerFlags= %CommonFlags% /DDEVELOPMENT /MD /O2 
) else if "%OutputDir%"=="release" (
	set CompilerFlags= %CommonFlags% /DRELEASE /MD /O2 
)


mkdir %OutputDir%
pushd %OutputDir%

del *.pdb

RC /nologo /r ../icon.rc
move ..\icon.res ..\%OutputDir%\

cl.exe ..\..\gbgengine\win64\win64.c /Fe"GbgTest.exe" icon.res %CompilerFlags% %LinkerFlags% /libpath User32.lib /libpath opengl32.lib /libpath Gdi32.lib Dwmapi.lib Ole32.lib Xaudio2.lib dinput8.lib dxguid.lib Kernel32.lib Shell32.lib Ws2_32.lib Advapi32.lib Iphlpapi.lib Comctl32.lib ShLwApi.Lib

echo "pdb lock" > lock
cl.exe ..\..\source\game.c /LDd /EHsc %CompilerFlags% %LinkerFlags% -PDB:game%random%.pdb -EXPORT:game_update
del lock

if not "%OutputDir%"=="debug" (
	del ..\*.pdb
	xcopy ..\..\assets\ assets\ /e /y
	del *.obj
	del *.exp
	del *.lib
	del *.res
	del *.ilk
	move *.pdb ..\
)

popd

:end
