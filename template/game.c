#include <gbg.h>

//Unity build
#include <gbg.build>

typedef enum game_state
{
	GameState_Play
} game_state;

//Game placeholder. Use it to keep your game state alive
struct game
{
	game_state GameState;
};

struct game *Game;

//Window width and height
#define W 400
#define H 400

//Entry point from engine to your game
void game_update(engine *Engine, float DeltaTime)
{
	//First time this function gets called. Use it to initialize the game
	if (Engine->Initialized == 0)
	{
		Game = memory_alloc_type(&Engine->Memory, struct game);
		Engine->GamePtr = Game;
		Game->GameState = GameState_Play;

		Engine->Platform->SetWindowTitle("GAME TEST");

		render_set_ortographic_projection(Engine, 0.0f, (f32)W, 0.0f, (f32)H);
		Engine->Platform->SetWindowSize(W, H);
		render_set_clear_color(Engine, Vec4(0, 0, 0, 1));

		Engine->Initialized = 1;
	}
}
