#include <stdio.h>
#include <emscripten/emscripten.h>
#include <emscripten/html5.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <net/if.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <pwd.h>
#include <dirent.h>
#include <string.h>

#include "keycodes.h"

#include <gbg.h>

#include "gbg_engine.c"

//TODO:
//[] Fullscreen
//[] webgl callbacks context lost
//[] gamepad, key, mouse events
//[] sound

struct engine Engine;

u8* platform_file_read_fully(const char *FullPath, u32 *Size)
{
	u8 *Buffer = 0;
	*Size = 0;
	int fd = open(FullPath, O_RDONLY);
	if (fd >= 0)
	{
		struct stat FileStat;
		fstat(fd, &FileStat);
		*Size = FileStat.st_size;
		Buffer = memory_alloc(&Engine.Memory, FileStat.st_size);
		u32 ReadSize = read(fd, Buffer, FileStat.st_size);
		if (ReadSize != *Size)
		{
			close(fd);
			printf("Error reading. expect %d, but read %d\n", *Size, ReadSize);
			return 0;
		}
		close(fd);
	}
	else
	{
		perror("Read fully failed");
	}

	return Buffer;
}

void platform_file_write_fully(const char *FullPath, u8 *Content, u32 Size)
{
	int fd = open(FullPath, O_CREAT | O_TRUNC | O_WRONLY, S_IRUSR | S_IWUSR);
	if (fd >= 0)
	{
		write(fd, Content, Size);
		close(fd);
	}
	else
	{
		perror("Write fully failed");
	}
}

static file_handle platform_file_open(const char *FileName)
{
	file_handle Ret;
	Ret.Descriptor = open(FileName, O_RDWR);
	return Ret;
}

static u32 platform_file_read(file_handle FileHandle, u8 *Buffer, u32 BufferLen, u32 From)
{
	u32 Ret = pread(FileHandle.Descriptor, Buffer, BufferLen, From);
	return Ret;
}

static void platform_file_close(file_handle FileHandle)
{
	close(FileHandle.Descriptor);
}

static file_handle platform_file_create(const char *FileName)
{
	file_handle Ret;
	Ret.Descriptor = open(FileName, O_CREAT | O_TRUNC | O_WRONLY, S_IRUSR | S_IWUSR);
	return Ret;
}

static u32 platform_file_write(file_handle FileHandle, u8 *Content, u32 Size, u32 From)
{
	return pwrite(FileHandle.Descriptor, Content, Size, From);
}

static b32 platform_directory_create(const char *Path)
{
	int ret = mkdir(Path, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
	return ret >= 0;
}

static void platform_log(const char *Fmt, ...)
{
	va_list args;
	va_start(args, Fmt);

	vdprintf(STDOUT_FILENO, Fmt, args);
}

static char *platform_file_dialog_picker(char *Path, file_picker_mode Mode, char *TypeDescription, char *TypeExtension)
{
	platform_log("Dialog picker is not handled by html platform\n");
	return 0;
}

void *platform_load_gl_function(const char *Name)
{
	platform_log("load gl function is disabled in html [%s]\n");
//	void *FnPtr = glXGetProcAddress((const GLubyte*)Name);
	return 0;
}

static void platform_keyboard_show()
{
}

static void platform_keyboard_hide()
{
}

static float UserAspectRatio = 0.75f;

void platform_set_window_size(u32 Width, u32 Height)
{
	printf("Set window size %dx%d\n", Width, Height);
	UserAspectRatio = Height/(float)Width;
	emscripten_set_canvas_element_size("#canvas", Width, Height);
	glViewport(0, 0, Width, Height);
}

void platform_hide_cursor()
{
	emscripten_hide_mouse();
}

f64 platform_get_local_time()
{
	struct timeval Time;
	gettimeofday(&Time, 0);

	return ((Time.tv_sec*1000.0) + (Time.tv_usec/1000.0))/1000.0f;
}

static void platform_set_window_title(const char *Title)
{
	emscripten_set_window_title(Title);
}

static u64 platform_get_file_modified_date(const char *Name)
{
	struct stat FileStat;
	if (stat(Name, &FileStat) == 0)
		return FileStat.st_mtim.tv_sec;

	return 0;
}

static void platform_exit()
{
	platform_log("Platform exit. Does nothing in html\n");
}

EM_BOOL on_canvassize_changed(int eventType, const void *reserved, void *userData)
{
  int w, h;
  emscripten_get_canvas_element_size("#canvas", &w, &h);
  double cssW, cssH;
  emscripten_get_element_css_size("#canvas", &cssW, &cssH);
  printf("Canvas resized: WebGL RTT size: %dx%d, canvas CSS size: %02gx%02g\n", w, h, cssW, cssH);
  return 0;
}

static void platform_switch_to_fullscreen()
{
	EmscriptenFullscreenStrategy s;
	memset(&s, 0, sizeof(s));
	s.scaleMode = EMSCRIPTEN_FULLSCREEN_SCALE_DEFAULT;
	s.canvasResolutionScaleMode = EMSCRIPTEN_FULLSCREEN_CANVAS_SCALE_NONE;
	s.filteringMode = EMSCRIPTEN_FULLSCREEN_FILTERING_DEFAULT;
	s.canvasResizedCallback = on_canvassize_changed;
	EMSCRIPTEN_RESULT ret = emscripten_request_fullscreen_strategy(0, 1, &s);
}

static void platform_mutex(b32 *Lock)
{
	while (__sync_val_compare_and_swap(Lock, 0, 1))
		;
}

static b32 create_socket_and_bind(const char *Host, const char *Port)
{
	int Sock = socket(AF_INET, SOCK_DGRAM, 0);
	if (Sock < 0)
	{
		printf("Error creating socket %d\n", Sock);
		return 0;
	}

	struct addrinfo *AddrInfo;
	struct addrinfo Hints = {0};
	Hints.ai_family = AF_INET;
	Hints.ai_socktype = SOCK_DGRAM;

	int Err = getaddrinfo(Host, Port, &Hints, &AddrInfo);
	if (Err != 0)
	{
		printf("error on getaddrinfo %d\n", Err);
		close(Sock);
		return 0;
	}

	Engine.Net->Socket.Descriptor = Sock;

	char LHost[NI_MAXHOST], Service[NI_MAXSERV];
	getnameinfo(AddrInfo->ai_addr, AddrInfo->ai_addrlen, LHost, NI_MAXHOST, Service, NI_MAXSERV, NI_NUMERICSERV);

    Err = bind(Sock, AddrInfo->ai_addr, AddrInfo->ai_addrlen);

	freeaddrinfo(AddrInfo);
	return 1;
}

static b32 platform_server_start(const char *Host, const char *Port)
{
	return create_socket_and_bind(Host, Port);
}

static void platform_server_stop()
{
	int fd = Engine.Net->Socket.Descriptor;
	shutdown(fd, SHUT_RDWR);
	close(fd);
	Engine.Net->Socket.Descriptor = 0;
}

static void platform_get_ipv4_address(char *Buffer, u32 BufferLen)
{
	int Sock = socket(AF_INET, SOCK_DGRAM, 0);
	if (Sock < 0)
	{
		printf("Error creating socket %d\n", Sock);
		return;
	}

	struct ifreq IfReq;
	IfReq.ifr_addr.sa_family = AF_INET;

	//Iface 1 is loopback, take the second one
	IfReq.ifr_ifindex = 2;
	ioctl(Sock, SIOCGIFNAME, &IfReq);

	ioctl(Sock, SIOCGIFADDR, &IfReq);

	const char *Addr = inet_ntop(AF_INET, &((struct sockaddr_in *)&IfReq.ifr_addr)->sin_addr, Buffer, BufferLen);
	if (Addr == 0)
		Buffer = 0;

	close(Sock);
}

static b32 platform_client_join(const char *Host, const char *Port)
{
	//TODO: Check all paths will free this memory up
	Engine.Net->Host.Address = string_dup(&Engine, Host);
	Engine.Net->Host.Port = string_dup(&Engine, Port);

	char LocalHostIp[64];
	platform_get_ipv4_address(LocalHostIp, static_array_len(LocalHostIp));
	b32 Ret = create_socket_and_bind(LocalHostIp, Port);

	return Ret;
}

static void platform_client_exit()
{
	memory_free(&Engine.Memory, Engine.Net->Host.Address);	
	memory_free(&Engine.Memory, Engine.Net->Host.Port);	
	shutdown(Engine.Net->Socket.Descriptor, SHUT_RD);
	close(Engine.Net->Socket.Descriptor);
	Engine.Net->Socket.Descriptor = 0;
}

static s32 platform_socket_read(file_handle Handle, u8 *Data, u32 DataLen, char *OutHost, char *OutPort)
{
	struct sockaddr SockAddr;
	socklen_t SockAddrLen = sizeof(SockAddr);	

	int Bytes = recvfrom(Handle.Descriptor, Data, DataLen, 0, &SockAddr, &SockAddrLen);

	if (OutHost != 0 && OutPort != 0)
	{
		getnameinfo(&SockAddr, SockAddrLen, OutHost, NI_MAXHOST, OutPort, NI_MAXSERV, NI_NUMERICSERV | NI_NUMERICHOST);
	}

	if (Bytes < 0)
	{
		shutdown(Handle.Descriptor, SHUT_RD);
		close(Handle.Descriptor);
	}

	return Bytes;
}

static s32 platform_socket_write(file_handle Handle, u8 *Data, u32 DataLen, const char *Host, const char *Port)
{
	struct addrinfo *AddrInfo;
	struct addrinfo Hints = {0};
	Hints.ai_family = AF_INET;
	Hints.ai_socktype = SOCK_DGRAM;

	int Err = getaddrinfo(Host, Port, &Hints, &AddrInfo);
	if (Err != 0)
	{
		printf("error on getaddrinfo %d. Can't send message\n", Err);
		return 0;
	}

	int Bytes = sendto(Handle.Descriptor, Data, DataLen, 0, AddrInfo->ai_addr, AddrInfo->ai_addrlen);
	freeaddrinfo(AddrInfo);

	return Bytes;
}

static void platform_create_thread(void *(*Func)(void *Param), void *Param)
{
	pthread_t ServerThreadId;
	pthread_create(&ServerThreadId, 0, Func, Param);
}

static void platform_exit_thread()
{
	pthread_exit(0);
}

static u16 platform_net_to_host_16(u16 NetShort)
{
	return ntohs(NetShort);
}

static u32 platform_net_to_host_32(u32 NetInt)
{
	return ntohl(NetInt);
}

static u16 platform_host_to_net_16(u16 HostShort)
{
	return htons(HostShort);
}

static u32 platform_host_to_net_32(u32 HostInt)
{
	return htonl(HostInt);
}

static void platform_get_user_name(char *UserName, u32 Len)
{
	getlogin_r(UserName, Len);
}

void platform_get_config_path(char *Path, u32 Size)
{
	struct passwd *Pwd = getpwuid(getuid());
	snprintf(Path, Size, "%s/.config", Pwd->pw_dir);
}

typedef struct file_operation_node
{
	DIR *Dir;
	char *Path;
} file_operation_node;

char *FileOperationExt = 0;
file_operation_node *FileOperationStack = 0;

static b32 platform_list_file_operation_start(char *Path, char *Extension)
{
	if (FileOperationStack)
	{
		platform_log("Error on file operation start. Close the old one before starting a new one!\n");
		return 0;
	}

	FileOperationExt = string_new(&Engine, ".%s", Extension);
	
	FileOperationStack = dynamic_array_create(&Engine, file_operation_node);

	file_operation_node FileOp;
	FileOp.Dir = opendir(Path);
	FileOp.Path = string_dup(&Engine, Path);

	dynamic_array_add(&FileOperationStack, FileOp);

	return 1;
}

char FileOperationFileName[1024];
static char *platform_list_file_operation_next()
{
	if (FileOperationStack == 0)
	{
		return 0;
	}
	u32 Len = dynamic_array_len(FileOperationStack);
	if (Len == 0)
	{
		//Stack is empty, finish operation
		dynamic_array_destroy(FileOperationStack);
		FileOperationStack = 0;
		string_delete(&Engine, FileOperationExt);
		FileOperationExt = 0;
		return 0;
	}

	file_operation_node *CurNode = FileOperationStack + (Len - 1);
	struct dirent *DirEntry = readdir(CurNode->Dir);
	if (DirEntry)
	{
		if (DirEntry->d_name[0] == '.')
			return platform_list_file_operation_next();

		if (DirEntry->d_type == DT_DIR)
		{
			file_operation_node NewOp;
			//recurse
			NewOp.Path = string_new(&Engine, "%s/%s", CurNode->Path, DirEntry->d_name);
			NewOp.Dir = opendir(NewOp.Path);
			dynamic_array_add(&FileOperationStack, NewOp);
			return platform_list_file_operation_next();
		}
		else
		{
			if (FileOperationExt)
			{
				u32 ELen = string_len(DirEntry->d_name);
				u32 ExtLen = string_len(FileOperationExt);
				for (u32 i=0; i<ExtLen; ++i)
				{
					if (DirEntry->d_name[ELen-ExtLen+i] != FileOperationExt[i])
					{
						return platform_list_file_operation_next();
					}
				}
			}

			//For the first element, only return the name
			if (Len == 1)
			{
				return DirEntry->d_name;
			}

			//Inside another directory, create full path
			FileOperationFileName[0] = 0;
			u32 FirstPathLen = string_len(FileOperationStack->Path) + 1;
			sprintf(FileOperationFileName, "%s/%s", CurNode->Path + FirstPathLen, DirEntry->d_name);

			return FileOperationFileName;
		}
	}
	else
	{
		closedir(CurNode->Dir);
		string_delete(&Engine, CurNode->Path);
		dynamic_array_remove_at(FileOperationStack, Len-1);
		return platform_list_file_operation_next();
	}

	return 0;
}

EM_BOOL context_lost(int eventType, const void *reserved, void *userData)
{
  printf("C code received a signal for WebGL context lost! This should not happen!\n");
  return 0;
}

EM_BOOL context_restored(int eventType, const void *reserved, void *userData)
{
  printf("C code received a signal for WebGL context restored! This should not happen!\n");
  return 0;
}

static void init_opengl()
{
	EmscriptenWebGLContextAttributes attr;
	emscripten_webgl_init_context_attributes(&attr);

	platform_log("init attr returned alpha %d, depth %d, stencil %d, antialias %d, preAlpha %d, preserve %d, powerPref %d, FailIf %d, Major %d, Minpr %d, enableExtensions %d, swapControl %d, proxyMode %d, render %d\n", attr.alpha, attr.depth, attr.stencil, attr.antialias, attr.premultipliedAlpha, attr.preserveDrawingBuffer, attr.powerPreference, attr.failIfMajorPerformanceCaveat, attr.majorVersion, attr.minorVersion, attr.enableExtensionsByDefault, attr.explicitSwapControl, attr.proxyContextToMainThread, attr.renderViaOffscreenBackBuffer);

#if 0
	attr.alpha = attr.depth = attr.stencil = attr.antialias = attr.preserveDrawingBuffer = attr.failIfMajorPerformanceCaveat = 0;
	attr.enableExtensionsByDefault = 1;
	attr.premultipliedAlpha = 0;
	attr.majorVersion = 1;
	attr.minorVersion = 0;
#endif
	EMSCRIPTEN_WEBGL_CONTEXT_HANDLE ctx = emscripten_webgl_create_context("#canvas", &attr);
	emscripten_webgl_make_context_current(ctx);
	platform_log("Make context current\n");
	emscripten_set_webglcontextlost_callback("#canvas", 0, 0, context_lost);
	emscripten_set_webglcontextrestored_callback("#canvas", 0, 0, context_restored);
}

static void gamepad_init()
{
}

extern void game_update(engine*, float);

double Time0;
void main_loop()
{
	float DeltaTime = (emscripten_get_now()-Time0)/1000.f;
	Time0 = emscripten_get_now();

	game_update(&Engine, DeltaTime);
	engine_update(&Engine, DeltaTime);
}

static EM_BOOL fullscreenchange_callback(int eventType, const EmscriptenFullscreenChangeEvent *e, void *userData)
{
	printf("fullscreen callback %d, %d, %s, %s, %d, %d, %d, %d, \n",  
	e->isFullscreen, e->fullscreenEnabled, e->nodeName, e->id, e->elementWidth, e->elementHeight, e->screenWidth, e->screenHeight);

	if (e->isFullscreen)
		platform_set_window_size(e->screenWidth, e->screenHeight);
	return 0;
}

EM_BOOL keyevent_callback(int EventType, const EmscriptenKeyboardEvent *E, void *UserData)
{
	printf("key Event type %d. Char key %ld, %c. Keycode %ld\n", EventType, E->charCode, (char)E->charCode, E->keyCode);

	key Key;
	Key.KeyCode = E->keyCode;

	if (EventType == EMSCRIPTEN_EVENT_KEYDOWN)
	{
		Key.State = ButtonState_Down;
	}
	else if (EventType == EMSCRIPTEN_EVENT_KEYUP)
	{
		Key.State = ButtonState_Up;
	}

	process_key(&Engine, Key);
	return 0;
}

EM_BOOL mouse_callback(int EventType, const EmscriptenMouseEvent *e, void *userData)
{
	if (EventType == EMSCRIPTEN_EVENT_MOUSEMOVE)
	{
		int w, h;
		emscripten_get_canvas_element_size("#canvas", &w, &h);
		Engine.Input->MouseState.MousePos.x = e->targetX;
		Engine.Input->MouseState.MousePos.y = h - e->targetY;
	}
	else if (EventType == EMSCRIPTEN_EVENT_MOUSEDOWN)
	{
		if (e->button == 0)
			Engine.Input->MouseState.LeftButton.CurState = ButtonState_Down;
		else if (e->button == 2)
			Engine.Input->MouseState.RightButton.CurState = ButtonState_Down;
	}
	else if (EventType == EMSCRIPTEN_EVENT_MOUSEUP)
	{
		if (e->button == 0)
			Engine.Input->MouseState.LeftButton.CurState = ButtonState_Up;
		else if (e->button == 2)
			Engine.Input->MouseState.RightButton.CurState = ButtonState_Up;
	}

	return 0;
}

int main()
{
	Time0 = emscripten_get_now();
	platform_log("trying platform log\n");

	init_opengl();

	u32 MemSize = MB(10);
	void *LocalMem = malloc(MemSize);
	Engine.Memory = memory_init(LocalMem, MemSize, platform_mutex);

	platform *Platform = malloc(sizeof(platform));
	Platform->FileReadFully = platform_file_read_fully;
	Platform->FileWriteFully = platform_file_write_fully;
	Platform->FileOpen = platform_file_open;
	Platform->FileRead = platform_file_read;
	Platform->FileClose = platform_file_close;
	Platform->FileCreate = platform_file_create;
	Platform->FileWrite = platform_file_write;
	Platform->DirectoryCreate = platform_directory_create;
	Platform->FilePicker = platform_file_dialog_picker;
	Platform->LoadGLFunction = platform_load_gl_function;
	Platform->Log = platform_log;
	Platform->SetWindowSize = platform_set_window_size;
	Platform->HideCursor = platform_hide_cursor;
	Platform->GetLocalTime = platform_get_local_time;
	Platform->SetWindowTitle = platform_set_window_title;
	Platform->GetFileModifiedDate = platform_get_file_modified_date;
	Platform->Exit = platform_exit;
	Platform->SwitchFullScreen = platform_switch_to_fullscreen;
	Platform->Mutex = platform_mutex;
	Platform->ServerStart = platform_server_start;
	Platform->ServerStop = platform_server_stop;
	Platform->ClientJoin = platform_client_join;
	Platform->ClientExit = platform_client_exit;
	Platform->SocketRead = platform_socket_read;
	Platform->SocketWrite = platform_socket_write;
	Platform->ThreadCreate = platform_create_thread;
	Platform->ThreadExit = platform_exit_thread;
	Platform->NetToHost16 = platform_net_to_host_16;
	Platform->NetToHost32 = platform_net_to_host_32;
	Platform->HostToNet16 = platform_host_to_net_16;
	Platform->HostToNet32 = platform_host_to_net_32;
	Platform->GetIp4Address = platform_get_ipv4_address;
	Platform->GetUserName = platform_get_user_name;
	Platform->GetConfigPath = platform_get_config_path;
	Platform->KeyboardShow = platform_keyboard_show;
	Platform->KeyboardHide = platform_keyboard_hide;
	Platform->ListFileStart = platform_list_file_operation_start;
	Platform->ListFileNext = platform_list_file_operation_next;

	Engine.Platform = Platform;

	Engine.AssetDatabase.RootPath = "assets/";

	render *Render = malloc(sizeof(render));
	memset(Render, 0, sizeof(render));
	Engine.Render = Render;

	Engine.Input = malloc(sizeof(input));
	memset(Engine.Input, 0, sizeof(input));

	Engine.ImGui = malloc(sizeof(imgui));
	memset(Engine.ImGui, 0, sizeof(imgui));

	Engine.Net = malloc(sizeof(net));
	memset(Engine.Net, 0, sizeof(net));

	platform_log("render init\n");
	render_init(&Engine);
	platform_log("engine init\n");
	engine_init(&Engine);

	gamepad_init();

//	emscripten_set_fullscreenchange_callback(EMSCRIPTEN_EVENT_TARGET_DOCUMENT, 0, 1, fullscreenchange_callback);
	emscripten_set_mousemove_callback("#canvas", 0, 1, mouse_callback);
	emscripten_set_mousedown_callback("#canvas", 0, 1, mouse_callback);
	emscripten_set_mouseup_callback("#canvas", 0, 1, mouse_callback);

	emscripten_set_keydown_callback("#canvas", 0, 1, keyevent_callback);
	emscripten_set_keyup_callback("#canvas", 0, 1, keyevent_callback);

	emscripten_set_main_loop(main_loop, 0, 0);
	platform_log("End main\n");
	return 0;
}

