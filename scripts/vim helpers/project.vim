set makeprg=compile.bat
set tags=tags

noremap <F12> :!ctags ..\gbgengine\* ..\source\* -R <CR>

set omnifunc=syntaxcomplete#Complete

"let OmniCpp_NamespaceSearch = 1
"let OmniCpp_GlobalScopeSearch = 1
"let OmniCpp_ShowAccess = 1
"let OmniCpp_MayCompleteDot = 1
"let OmniCpp_MayCompleteArrow = 1
" automatically open and close the popup menu / preview window
"au CursorMovedI,InsertLeave * if pumvisible() == 0|silent! pclose|endif
set completeopt=longest,menuone,preview

function! Build()
	mak

	let l:list = getqflist()
	let newquickfix = []
	for i in l:list
		if i.bufnr > 0
			let name = bufname(i.bufnr)
			if filereadable(name)
				if stridx(name, ".") != 0
					let fi = stridx(name, "gbgengine")
					let s = "..\\" . name[fi:]
					let i["filename"] = s
					call remove(i, "bufnr")
					call add(newquickfix, i)
				endif
				call add(newquickfix, i)
			else 
				if stridx(name, "..") == 0
					let i["filename"] = name[3:]
					call remove(i, "bufnr")
					call add(newquickfix, i)
				else
					call add(newquickfix, i)
				endif
			endif
		endif
	endfor
	if len(newquickfix) > 0
		call setqflist(newquickfix)
		:cw
		:cc
	endif
endfunction

function! SwitchToHeaderOrSource()
	"update!
	if (expand ("%:e") == "c")
		find%:p:r.h
	else 
		find%:p:r.c
	endif
endfunction

noremap <C-K><C-O> :call SwitchToHeaderOrSource() <CR>
noremap <F7> :call Build() <CR>
noremap <C-F> :execute "vimgrep /\\<" . expand("<cword>") . "\\>/ ../source/** ../gbgengine/*" <cr>
